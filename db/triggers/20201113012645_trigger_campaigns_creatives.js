const Triggers = require('../triggers');

exports.up = async function (knex) {
    const triggers = new Triggers('campaigns_creatives');

    /** Function to trigger */
    const update_creatives_numberOfCampaigns = (obj) => {
        return `
            UPDATE "creatives" SET "numberOfCampaigns" = (
                SELECT COUNT(id)
                FROM "campaigns_creatives"
                WHERE "campaigns_creatives"."creativeId" = ${obj.toUpperCase()}."creativeId"
            )
            WHERE "creatives"."id" = ${obj.toUpperCase()}."creativeId"
        `
    }

    /** Condition to trigger */
    const afterInsert = `
        ${update_creatives_numberOfCampaigns('new')};
    `;
    await knex.raw(triggers.create('after', 'insert', afterInsert));

    const afterDelete = `
        ${update_creatives_numberOfCampaigns('old')};
    `;
    await knex.raw(triggers.create('after', 'delete', afterDelete));

    const afterUpdate = `
    IF OLD."creativeId" <> NEW."creativeId" THEN
        ${update_creatives_numberOfCampaigns('old')};
        ${update_creatives_numberOfCampaigns('new')};
    END IF;
    `
    await knex.raw(triggers.create('after', 'update', afterUpdate));
    return
};

exports.down = async function (knex) {
    const triggers = new Triggers('campaigns_creatives');
    await knex.raw(triggers.remove('after', 'insert'))
    await knex.raw(triggers.remove('after', 'delete'))
    await knex.raw(triggers.remove('after', 'update'))
    return
};
