
class Triggers {
    constructor(tableName) {
        this.triggers = ['insert', 'update', 'delete', 'truncate'];
        this.states = ['before', 'after', 'instead of']
        this.table = tableName
    }

    /**
     * @param {*} state ['before', 'after', 'instead of'] -- as When in Pg
     * @param {*} trigger ['insert', 'update', 'delete', 'truncate'] -- as Event in Pg
     * @param {*} rawQuery SQL raw query -- as Statement in Pg
     */
    create = (state, trigger, rawQuery) => {
        state = this.validateState(state);
        trigger = this.validateTrigger(trigger);
        rawQuery = this.validateQuery(rawQuery);
        let triggerName = `${state}_${this.table}_${trigger}`
        return `
            ${this.createFunction(triggerName, rawQuery)}
            ${this.createTrigger(triggerName, state, trigger)}
        `
    }

    /**
     * @param {*} state ['before', 'after'] -- as Timing in Pg
     * @param {*} trigger ['insert', 'update', 'delete'] -- as Event in Pg
     */
    remove = (state, trigger) => {
        this.validateState(state);
        this.validateTrigger(trigger);
        let triggerName = `${state}_${this.table}_${trigger}`
        return `
        DROP FUNCTION IF EXISTS ${triggerName}_function() CASCADE
        `
    }

    /**
     * Function must be created before create trigger
     * @param {*} name 
     * @param {*} raw 
     */
    createFunction(name, raw) {
        return `
        CREATE OR REPLACE FUNCTION ${name}_function()
            RETURNS TRIGGER 
            LANGUAGE PLPGSQL
            AS
        $${this.table}$
        BEGIN
            ${raw}
            RETURN NEW;
        END;
        $${this.table}$;
        `
    }

    /**
     * Bind trigger procedure to an existed function
     * @param {*} triggerName 
     * @param {*} state 
     * @param {*} trigger 
     */
    createTrigger(triggerName, state, trigger) {
        return `
        CREATE TRIGGER ${triggerName}
            ${state.toUpperCase()} ${trigger.toUpperCase()} 
            ON ${this.table} FOR EACH ROW
            EXECUTE PROCEDURE ${triggerName}_function();
        `
    }

    /** validate and format data here */
    validateTrigger = (trigger) => {
        if (!this.triggers.includes(trigger)) throw new Error('trigger not allow');
        return trigger;
    }

    validateState = (state) => {
        if (!this.states.includes(state)) throw new Error('state not allow');
        return state;
    }

    validateQuery = (rawQuery) => {
        rawQuery = rawQuery.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
        if (rawQuery.slice(rawQuery.length - 1) != ';') rawQuery += ';'
        return rawQuery;
    }
}

module.exports = Triggers