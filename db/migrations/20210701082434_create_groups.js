
exports.up = function (knex) {
  return knex.schema.createTable('groups', function (table) {
    table.increments();
    table.string('name').notNullable();
    table.string('description').nullable();
    table.string('address').nullable();
    table.integer('province_id').nullable().index().references('id').inTable('provinces')
      .onUpdate('CASCADE').onDelete('SET NULL');
    table.integer('district_id').nullable().index().references('id').inTable('districts')
      .onUpdate('CASCADE').onDelete('SET NULL');
    table.integer('ward_id').nullable().index().references('id').inTable('wards')
      .onUpdate('CASCADE').onDelete('SET NULL');
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('groups');
};
