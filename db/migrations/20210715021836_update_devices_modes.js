exports.up = function (knex) {
  return knex.schema.alterTable("devices", function (table) {
    table.jsonb("modes").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.alterTable("devices", function (table) {
    table.dropColumn("modes");
  });
};
