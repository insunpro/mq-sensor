
exports.up = function (knex) {
    return knex.schema.alterTable('snapshots', function (table) {
      table.integer('size').nullable();
    });
  };
  
exports.down = function (knex) {
return knex.schema.alterTable('snapshots', function (table) {
    table.dropColumn('size');
});
};
