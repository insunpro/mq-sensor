
exports.up = function (knex) {
    return knex.schema.createTable('snapshots', function (table) {
    table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()'));
    table.integer('camera_id').notNullable().index().references('id').inTable('cameras')
      .onUpdate('CASCADE').onDelete('CASCADE');
    table.integer('sensor_id').nullable().index().references('id').inTable('sensors')
      .onUpdate('CASCADE').onDelete('SET NULL');
    table.string('src').nullable();
    table.integer('dectecion').defaultTo(0);
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.dropTable('snapshots');
  };
  