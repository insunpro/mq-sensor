
exports.up = function (knex) {
    return knex.schema.alterTable('cameras', function (table) {
      table.integer('numberOfSensors').defaultTo(0);
    });
  };
  
exports.down = function (knex) {
return knex.schema.alterTable('cameras', function (table) {
    table.dropColumn('numberOfSensors');
});
};

