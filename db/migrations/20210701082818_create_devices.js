
exports.up = function (knex) {
  return knex.schema.createTable('devices', function (table) {
    table.increments();
    table.string('serialNumber').notNullable();
    table.string('name').nullable();
    table.string('lat').nullable();
    table.string('lng').nullable();
    table.integer('status').defaultTo(0);
    table.integer('group_id').nullable().index().references('id').inTable('groups')
      .onUpdate('CASCADE').onDelete('SET NULL');
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('devices');
};
