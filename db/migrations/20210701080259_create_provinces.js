
exports.up = function (knex) {
  return knex.schema.createTable('provinces', function (table) {
    table.increments();
    table.string('name').notNullable();
    table.string('type').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('provinces');
};
