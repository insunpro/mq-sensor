
exports.up = function (knex) {
  return knex.schema.createTable('message_logs', function (table) {
    table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()'));
    table.string('message_key').notNullable().index().references('key').inTable('messages')
      .onUpdate('CASCADE').onDelete('CASCADE');
    table.jsonb('data').nullable();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('message_logs');
};
