
exports.up = function (knex) {
  return knex.schema.createTable('roles', function (table) {
    table.increments();
    table.string('name').notNullable();
    table.string('description').nullable();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('roles');
};
