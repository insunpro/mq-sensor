
exports.up = function (knex) {
    return knex.schema.alterTable('messages', function (table) {
      table.integer('alertLevel').defaultTo(1);
    });
  };
  
exports.down = function (knex) {
return knex.schema.alterTable('messages', function (table) {
    table.dropColumn('alertLevel');
});
};
