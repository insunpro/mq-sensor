
exports.up = function (knex) {
    return knex.schema.alterTable('cameras', function (table) {
      table.string('host').nullable();
      table.integer('port').nullable();
      table.string('username').nullable();
      table.string('password').nullable();
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.alterTable('cameras', function (table) {
        table.dropColumn('host');
        table.dropColumn('port');
        table.dropColumn('username');
        table.dropColumn('password');
    });
  };
  