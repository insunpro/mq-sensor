
exports.up = function (knex) {
  return knex.schema.createTable('device_logs', function (table) {
    table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()'));
    table.integer('device_id').notNullable().index().references('id').inTable('devices')
      .onUpdate('CASCADE').onDelete('CASCADE');
    table.jsonb('topic').nullable();
    table.jsonb('payload').nullable();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('device_logs');
};
