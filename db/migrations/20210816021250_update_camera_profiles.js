
exports.up = function (knex) {
    return knex.schema.alterTable('cameras', function (table) {
      table.jsonb('profiles').nullable();
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.alterTable('cameras', function (table) {
        table.dropColumn('profiles');
    });
  };
  