const Triggers = require('../triggers/pg');

exports.up = async function (knex) {
    const triggers = new Triggers('sensors_cameras');

    /** Function to trigger */
    const update_cameras_numberOfSensors = (obj) => {
        return `
            UPDATE "cameras" SET "numberOfSensors" = (
                SELECT COUNT(id)
                FROM "sensors_cameras"
                WHERE "sensors_cameras"."camera_id" = ${obj.toUpperCase()}."camera_id"
            )
            WHERE "cameras"."id" = ${obj.toUpperCase()}."camera_id"
        `
    }

    /** Condition to trigger */
    const afterInsert = `
        ${update_cameras_numberOfSensors('new')};
    `;
    await knex.raw(triggers.create('after', 'insert', afterInsert));

    const afterDelete = `
        ${update_cameras_numberOfSensors('old')};
    `;
    await knex.raw(triggers.create('after', 'delete', afterDelete));

    const afterUpdate = `
    IF OLD."camera_id" <> NEW."camera_id" THEN
        ${update_cameras_numberOfSensors('old')};
        ${update_cameras_numberOfSensors('new')};
    END IF;
    `
    await knex.raw(triggers.create('after', 'update', afterUpdate));
    return
};

exports.down = async function (knex) {
    const triggers = new Triggers('sensors_cameras');
    await knex.raw(triggers.remove('after', 'insert'))
    await knex.raw(triggers.remove('after', 'delete'))
    await knex.raw(triggers.remove('after', 'update'))
    return
};
