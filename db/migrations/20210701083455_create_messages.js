
exports.up = function (knex) {
  return knex.schema.createTable('messages', function (table) {
    table.increments();
    table.string('key').notNullable().unique();
    table.string('name').nullable();
    table.jsonb('value').nullable();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('messages');
};
