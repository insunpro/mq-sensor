
exports.up = function (knex) {
  return knex.schema.createTable('districts', function (table) {
    table.increments();
    table.string('name').notNullable();
    table.string('type').nullable();
    table.integer('province_id').notNullable().index().references('id').inTable('provinces')
      .onUpdate('CASCADE').onDelete('CASCADE');
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('districts');
};
