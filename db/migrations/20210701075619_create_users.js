
exports.up = function (knex) {
  return knex.schema.createTable('users', function (table) {
    table.increments();
    table.string('username').notNullable();
    table.string('password').notNullable();
    table.string('email').nullable();
    table.string('name').nullable();
    table.string('phone').nullable();
    table.integer('role_id').notNullable().index().references('id').inTable('roles')
      .onUpdate('CASCADE').onDelete('CASCADE');
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('users');
};
