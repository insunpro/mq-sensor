
exports.up = function (knex) {
  return knex.schema.createTable('settings', function (table) {
    table.string('key').primary().unique();
    table.jsonb('value').nullable();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('settings');
};
