exports.up = function (knex) {
  return knex.schema.alterTable("sensors", function (table) {
    table.jsonb("modes").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.alterTable("sensors", function (table) {
    table.dropColumn("modes");
  });
};
