
exports.up = function (knex) {
  return knex.schema.createTable('sensors', function (table) {
    table.increments();
    table.integer('device_id').notNullable().index().references('id').inTable('devices')
    .onUpdate('CASCADE').onDelete('CASCADE');
    table.integer('sensorId').nullable();
    table.integer('battery').nullable();
    table.string('lat').nullable();
    table.string('lng').nullable();
    table.integer('status').defaultTo(0);
    table.string('description').nullable();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('sensors');
};
