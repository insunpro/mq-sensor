
exports.up = function(knex) {
  return knex.schema.alterTable("groups", function (table) {
    table.integer("numberOfDevices").nullable().defaultTo(0);
  })
};

exports.down = function(knex) {
  return knex.schema.alterTable("groups", function (table) {
    table.dropColumn("numberOfDevices")
  })
};
