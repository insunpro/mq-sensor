
exports.up = function (knex) {
  return knex.schema.createTable('wards', function (table) {
    table.increments();
    table.string('name').notNullable();
    table.string('type').nullable();
    table.integer('province_id').notNullable().index().references('id').inTable('provinces')
      .onUpdate('CASCADE').onDelete('CASCADE');
    table.integer('district_id').notNullable().index().references('id').inTable('districts')
      .onUpdate('CASCADE').onDelete('CASCADE');
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('wards');
};
