
exports.up = function (knex) {
  return knex.schema.createTable('sensors_cameras', function (table) {
    table.increments();
    table.integer('sensor_id').notNullable().index().references('id').inTable('sensors')
    .onUpdate('CASCADE').onDelete('CASCADE');
    table.integer('camera_id').notNullable().index().references('id').inTable('cameras')
    .onUpdate('CASCADE').onDelete('CASCADE');
    table.jsonb('info').nullable();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').nullable();
    table.timestamp('deleted_at').nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('sensors_cameras');
};
