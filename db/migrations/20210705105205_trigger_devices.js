const Triggers = require('../triggers/pg');

exports.up = async function (knex) {
    const triggers = new Triggers('devices');

    /** Function to trigger */
    const update_groups_numberOfDevices = (obj) => {
        return `
            UPDATE "groups" SET "numberOfDevices" = (
                SELECT COUNT(id)
                FROM "devices"
                WHERE "devices"."group_id" = ${obj.toUpperCase()}."group_id"
            )
            WHERE "groups"."id" = ${obj.toUpperCase()}."group_id"
        `
    }

    /** Condition to trigger */
    const afterInsert = `
        ${update_groups_numberOfDevices('new')};
    `;
    await knex.raw(triggers.create('after', 'insert', afterInsert));

    const afterDelete = `
        ${update_groups_numberOfDevices('old')};
    `;
    await knex.raw(triggers.create('after', 'delete', afterDelete));

    const afterUpdate = `
    IF OLD."group_id" <> NEW."group_id" THEN
        ${update_groups_numberOfDevices('old')};
        ${update_groups_numberOfDevices('new')};
    END IF;
    `
    await knex.raw(triggers.create('after', 'update', afterUpdate));
    return
};

exports.down = async function (knex) {
    const triggers = new Triggers('devices');
    await knex.raw(triggers.remove('after', 'insert'))
    await knex.raw(triggers.remove('after', 'delete'))
    await knex.raw(triggers.remove('after', 'update'))
    return
};
