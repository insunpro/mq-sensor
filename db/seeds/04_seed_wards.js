const samples = require('../samples/wards.json')

exports.seed = function (knex, Promise) {
  const data = samples.RECORDS.map(record => {
    return {
      id: record.id,
      name: record.name,
      type: record.prefix,
      province_id: record.provinceId,
      district_id: record.districtId
    }
  })

  // Deletes ALL existing entries
  return knex("wards")
    .del()
    .then(async () => {
      // Inserts seed entries
      await knex("wards").insert(data);
      await knex.raw("select setval('wards_id_seq', max(id)) from wards");
    });
};
