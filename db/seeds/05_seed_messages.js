exports.seed = function (knex, Promise) {
  const data = [
    {
      key: "deviceOnline",
      name: "Thiết bị hoạt động",
      value: {
        keywords: ["serialNumber", "time"],
        content: "Thiết bị có mã {{serialNumber}} đã hoạt động lúc {{time}}"
      }
    },
    {
      key: "deviceOffline",
      name: "Thiết bị ngừng hoạt động",
      value: {
        keywords: ["serialNumber", "time"],
        content: "Thiết bị có mã {{serialNumber}} đã ngừng hoạt động lúc {{time}}"
      }
    },
    {
      key: "deviceDelete",
      name: "Thiết bị ngắt kết nối với cảm biến",
      value: {
        keywords: ["serialNumber", "time"],
        content: "Thiết bị có mã {{serialNumber}} đã ngắt kết nối với tất cả cảm biến lúc {{time}}"
      }
    },
    {
      key: "deviceChangeMode",
      name: "Thay đổi cài đặt thiết bị",
      value: {
        keywords: ["serialNumber", "time", "mode", "status"],
        content: "Thiết bị có mã {{serialNumber}} đã thay đổi cài đặt lúc {{time}}: {{mode}} - {{status}}"
      }
    },
    {
      key: "nodeJoin",
      name: "Thiết bị cảm biến tham gia vào mạng",
      value: {
        keywords: ["serialNumber", "time", "id"],
        content: "Thiết bị cảm biến có mã {{id}} ({{serialNumber}}) đã tham gia lúc {{time}}"
      }
    },
    {
      key: "nodeOnline",
      name: "Thiết bị cảm biến có kết nối",
      value: {
        keywords: ["serialNumber", "time", "id"],
        content: "Thiết bị cảm biến có mã {{id}} ({{serialNumber}}) đã kết nối lúc {{time}}"
      }
    },
    {
      key: "nodeOffline",
      name: "Thiết bị cảm biến mất kết nối",
      value: {
        keywords: ["serialNumber", "time", "id"],
        content: "Thiết bị cảm biến có mã {{id}} ({{serialNumber}}) đã mất kết nối lúc {{time}}"
      }
    },
    {
      key: "nodeBattery",
      name: "Thiết bị cảm biến sắp hết pin",
      value: {
        keywords: ["serialNumber", "time", "percent", "id"],
        content: "Thiết bị cảm biến có mã {{id}} ({{serialNumber}}) sắp hết pin ({{percent}}%), thời gian {{time}}"
      }
    },
    {
      key: "nodeDelete",
      name: "Thiết bị cảm biến ra khỏi mạng",
      value: {
        keywords: ["serialNumber", "time", "id"],
        content: "Thiết bị cảm biến có mã {{id}} ({{serialNumber}}) đã ra khỏi mạng lúc {{time}}"
      }
    },
    {
      key: "nodeChangeMode",
      name: "Thay đổi cài đặt thiết bị cảm biến",
      value: {
        keywords: ["serialNumber", "time", "id", "mode", "status"],
        content: "Thiết bị cảm biến có mã {{id}} ({{serialNumber}}) đã thay đổi cài đặt lúc {{time}}: {{mode}} - {{status}}"
      }
    },
    {
      key: "warningTrepassing",
      name: "Cảnh báo xâm phạm",
      value: {
        keywords: ["serialNumber", "time", "id"],
        content: "Thiết bị cảm biến có mã {{id}} ({{serialNumber}}) phát hiện xâm phạm lúc {{time}}"
      }
    },
    {
      key: "warningBlocked",
      name: "Cảnh báo bị chặn",
      value: {
        keywords: ["serialNumber", "time", "id"],
        content: "Thiết bị cảm biến có mã {{id}} ({{serialNumber}}) phát hiện bị chặn lúc {{time}}"
      }
    },
  ];

  // Deletes ALL existing entries
  return knex("messages")
    .del()
    .then(async () => {
      // Inserts seed entries
      await knex("messages").insert(data);
      await knex.raw("select setval('messages_id_seq', max(id)) from messages");
    });
};
