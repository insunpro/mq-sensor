const samples = require('../samples/provinces.json')

exports.seed = function (knex, Promise) {
  const data = samples.RECORDS.map(record => {
    return {
      id: record.id,
      name: record.name,
      type: record.prefix
    }
  })

  // Deletes ALL existing entries
  return knex("provinces")
    .del()
    .then(async () => {
      // Inserts seed entries
      await knex("provinces").insert(data);
      await knex.raw("select setval('provinces_id_seq', max(id)) from provinces");
    });
};
