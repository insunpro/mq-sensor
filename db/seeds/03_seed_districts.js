const samples = require('../samples/districts.json')

exports.seed = function (knex, Promise) {
  const data = samples.RECORDS.map(record => {
    return {
      id: record.id,
      name: record.name,
      type: record.prefix,
      province_id: record.provinceId
    }
  })

  // Deletes ALL existing entries
  return knex("districts")
    .del()
    .then(async () => {
      // Inserts seed entries
      await knex("districts").insert(data);
      await knex.raw("select setval('districts_id_seq', max(id)) from districts");
    });
};
