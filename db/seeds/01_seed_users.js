exports.seed = function (knex, Promise) {
  const data = [
    {
      id: 2,
      username: "admin",
      password: "$2b$10$iNT.d38.rdsRvRMU95WTSu0ZMUBi/Dbwsrzw7yu0vT60T9EPu8eNi", //123456@
      name: "Super Admin",
      role_id: 1,
    },
    {
      id: 1,
      username: "user",
      password: "$2b$10$iNT.d38.rdsRvRMU95WTSu0ZMUBi/Dbwsrzw7yu0vT60T9EPu8eNi", //123456@
      name: "Super User",
      role_id: 2,
    },
  ];

  // Deletes ALL existing entries
  return knex("users")
    .del()
    .then(async () => {
      // Inserts seed entries
      await knex("users").insert(data);
      await knex.raw("select setval('users_id_seq', max(id)) from users");
    });
};
