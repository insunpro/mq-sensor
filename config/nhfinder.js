module.exports = {
  ROOTDIR: __ROOTDIR__, //The absolute path to the root directory.
  UPLOAD_PATH: __ROOTDIR__ + "/public/uploads/", //The absolute path to the upload folder.
  PUBLIC_PATH: "/uploads", //public path. For example, http: // localhost: 3000 / uploads / then enter: / uploads
  DOMAIN: "http://localhost:3000", //Domain used for image links.
  ALLOW_EXTENSIONS: [".jpg", ".jpeg", ".png", ".bmp", ".gif", ".txt"], //Extensions are allowed to uploads.
  API_KEY: "123456", //API Key uses security between client and server.
};
