export const deviceModes = {
  config: {
    type: "number",
    default: 0,
    cmd: 6,
    vi: "Hòa mạng"
  },
}

export const sensorModes = {
  alert: {
    type: "number",
    default: 1,
    cmd: 7,
    vi: "Cảnh báo"
  }
}

export const alertLevels = {
  off: 0,
  low: 1,
  medium: 2,
  high: 3
}

export const alertServices = {
  email: 'email',
  zalo: 'zalo',
  telegram: 'telegram',
  skype: 'skype'
}