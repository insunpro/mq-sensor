const mqtt = require("mqtt");

let host;
// host = "https://sensor.test.mqsolutions.vn:12883";
// host= "http://192.168.6.84:1883";
host = "http://127.0.0.1:1883";

let options = {
  protocolId: "MQIsdp",
  protocolVersion: 3,
  username: "akjsda",
  password: "123456",
};

const client = mqtt.connect(host, options);

const nodes = [
  {
    id: 1,
    type: 1,
    battery: 10,
    value: null,
  },
  {
    id: 2,
    type: 1,
    battery: 50,
    value: null,
  },
  {
    id: 3,
    type: 1,
    battery: 90,
    value: null,
  },
];

client.on("connect", function (asd) {
  console.log("connect", asd);
  client.subscribe("Mqsolutions/akjsda/1", function (err) {
    if (!err) {
      nodes.map((node) => {
        client.publish(
          "Mqsolutions/akjsda/1",
          JSON.stringify({
            id: node.id,
            type: node.type,
            bat: node.battery,
            value: 1,
          })
        );
      });
    //   setInterval(() => {
    //     client.publish(
    //       "Mqsolutions/akjsda/2",
    //       JSON.stringify({ id: 1, type: 1, bat: 88, value: 1 })
    //     );
    //     setTimeout(() => {
    //       client.publish(
    //         "Mqsolutions/akjsda/2",
    //         JSON.stringify({ id: 2, type: 1, bat: 88, value: 2 })
    //       );
    //     }, 5000);
    //   }, 20000);
    } else {
      console.error("err ===> ", err);
    }
  });

  // setTimeout(() => client.end(true), 5000)
});

client.on("message", function (topic, message) {
  let topicArr = topic.split("/");
  let cmd = Number(topicArr[2]);
  console.log({ topic, message: message.toString() });

  switch (cmd) {
    case 3:
      setTimeout(() => {
        client.publish(topic, message);
      }, 1000);
      break;
    case 5:
      setTimeout(() => {
        client.publish(topic, message);
      }, 1000);
      break;
    case 6:
      setTimeout(() => {
        client.publish(topic, message);
      }, 1000);
      break;
    case 7:
      setTimeout(() => {
        client.publish(topic, message);
      }, 1000);
      break;
    default:
      console.error("unknown cmd", cmd);
  }
  // message is Buffer
  // client.end()
});

client.on("disconnect", function (abc) {
  console.log("disconnect", abc);
});

client.on("error", function (error) {
  console.log("error", error);
});
