import BaseModel from "./BaseModel";
import GroupModel from "./GroupModel";
import SensorModel from "./SensorModel";
import DeviceLogModel from "./DeviceLogModel";

class DeviceModel extends BaseModel {
  static tableName = "devices";

  //fields
  id: number;
  serialNumber!: string;
  name: string;
  lat: string;
  lng: string;
  status: number;
  group_id: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
  modes: {
    config: number;
  }

  static get relationMappings() {
    return {
      deviceLogs: {
        relation: BaseModel.HasManyRelation,
        modelClass: DeviceLogModel,
        join: {
          from: `${this.tableName}.id`,
          to: `${DeviceLogModel.tableName}.device_id`,
        },
      },
      group: {
        relation: BaseModel.HasOneRelation,
        modelClass: GroupModel,
        join: {
          from: `${this.tableName}.group_id`,
          to: `${GroupModel.tableName}.id`,
        },
      },
      sensors: {
        relation: BaseModel.HasManyRelation,
        modelClass: SensorModel,
        join: {
          from: `${this.tableName}.id`,
          to: `${SensorModel.tableName}.device_id`,
        },
      },
    };
  }

  static async changeMode(deviceId, payload, time, mode) {
    const {id, type, bat, value } = payload
    let device = await this.query().findById(deviceId);
    if (!device) {
      return console.error("cannot change mode device: " + deviceId)
    }
    let modes = device.modes || {};
    modes[mode] = value;
    return await device.$query().patch({modes, updated_at: time})
  }
}

export default DeviceModel;
