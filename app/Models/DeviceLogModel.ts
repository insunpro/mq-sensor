import BaseModel from "./BaseModel";
import DeviceModel from "./DeviceModel";

class DeviceLogModel extends BaseModel {
  static tableName = "device_logs";

  //fields
  id!: string;
  device_id!: string;
  topic: any;
  payload: any;
  created_at: Date;

   static get relationMappings() {
    return {
      device: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: DeviceModel,
        join: {
          from: `${this.tableName}.device_id`,
          to: `${DeviceModel.tableName}.id`,
        },
      },
    };
  }
}

export default DeviceLogModel;
