import BaseModel from "./BaseModel";
import MessageModel from "./MessageModel";

class MessageLogModel extends BaseModel {
  static tableName = "message_logs";

  //fields
  id!: string;
  message_key!: string;
  data: any;
  created_at: Date;

   static get relationMappings() {
    return {
      message: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: MessageModel,
        join: {
          from: `${this.tableName}.message_key`,
          to: `${MessageModel.tableName}.id`,
        },
      },
    };
  }
}

export default MessageLogModel;
