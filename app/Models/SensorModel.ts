import BaseModel from "./BaseModel";
import CameraModel from "./CameraModel";
import DeviceModel from "./DeviceModel";
import SensorCameraModel from "./SensorCameraModel";

class SensorModel extends BaseModel {
  static tableName = "sensors";

  //fields
  id: number;
  device_id!: number;
  sensorId: number;
  battery:number;
  lat: string;
  lng: string;
  status: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
  device?: DeviceModel;
  modes: {
    alert: number;
  };
  cameras?: SensorCameraModel[];

  static get relationMappings() {
    return {
      device: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: DeviceModel,
        join: {
          from: `${this.tableName}.device_id`,
          to: `${DeviceModel.tableName}.id`,
        },
      },
      cameras: {
        relation: BaseModel.HasManyRelation,
        modelClass: SensorCameraModel,
        join: {
          from: `${this.tableName}.id`,
          to: `${SensorCameraModel.tableName}.sensor_id`,
        },
      }
    };
  }

  static modifiers = {
    filterNotDeleted(query) {
      query.whereNull('sensors.deleted_at');
    },
    orderAsc(query) {
      query.orderBy(['status', {column: 'sensorId', order: 'desc'}])
    }
  };

  static async newNode(deviceId, payload, time) {
    const {id, type, bat, value } = payload
    let sensor = await this.query().findOne({device_id: deviceId, sensorId: id});
    if (!sensor) {
      return await this.query().insert({device_id: deviceId, sensorId: id, battery: bat, status: 1, updated_at: time})
    }
    else {
      let params = { status: 1, deleted_at: undefined}
      if (sensor.deleted_at) {
        params.deleted_at = null;
      }
      await sensor.$query().patch(params)
    }
    return sensor
  }

  static async sendData(deviceId, payload, time) {
    const {id, type, bat, value } = payload
    let sensor = await this.query().findOne({device_id: deviceId, sensorId: id}); 
    if (sensor) {
      let params: any = {battery: bat, status:1, updated_at: time}
      if (sensor.deleted_at) params.deleted_at = null
      return sensor.$query().patch(params)
    }
    return false
  }

  static async deleteNode(deviceId, payload) {
    const {id, type, bat, value } = payload
    return await this.query().findOne({device_id: deviceId, sensorId: id}).del(); 
  }

  static async nodeStatus(deviceId, payload, time) {
    const {id, type, bat, value } = payload
    let sensor = await this.query().findOne({device_id: deviceId, sensorId: id}); 
    if (sensor) {
      let params: any = {status: value === 0 ? 0 : 1, updated_at: time}
      if (sensor.deleted_at) params.deleted_at = null
      return sensor.$query().patch(params)
    }
    return false
  }

  static async deleteAllNode(deviceId, payload) {
    const {id, type, bat, value } = payload
    return await this.query().where({device_id: deviceId}).del(); 
  }

  static async changeMode(deviceId, payload, time, mode) {
    const {id, type, bat, value } = payload
    let sensor = await this.query().findOne({device_id: deviceId, sensorId: id});
    if (!sensor) {
      return console.error("cannot change mode sensor: " + deviceId)
    }
    let modes = sensor.modes || {};
    modes[mode] = value;
    return await sensor.$query().patch({modes, updated_at: time})
  }

  static async deviceOffline(deviceId, time) {
    return await this.query().where({device_id: deviceId}).update({status: 0, updated_at: time}); 
  }

  static async ptz(deviceId, payload) {
    const {id, type, bat, value } = payload
    let sensor = await this.query().findOne({device_id: deviceId, sensorId: id}).withGraphJoined('cameras');
    if (sensor && sensor.cameras) {
      for (let sensorCamera of sensor.cameras) {
        let camera = await CameraModel.query().findById(sensorCamera.camera_id);
        if (camera) {
          await camera.ptz({arg1: sensorCamera.info.h, arg2: sensorCamera.info.v, arg3: sensorCamera.info.z})
        }
      }
    }
  }

  static async snap(deviceId, payload) {
    const {id, type, bat, value } = payload
    let sensor = await this.query().findOne({device_id: deviceId, sensorId: id}).withGraphJoined('cameras');
    if (sensor && sensor.cameras) {
      return await Promise.all(sensor.cameras.map(async (sensorCamera) => {
        let camera = await CameraModel.query().findById(sensorCamera.camera_id);
        if (camera) {
          let onvifCamera = await CameraModel.onvifDevice(camera);
          let { profile, PresetToken } = sensorCamera.info;
          if (!profile || PresetToken) return;

          await CameraModel.goToPreset(onvifCamera, profile, PresetToken )

          let snap: any = await CameraModel.fetchSnapShot(onvifCamera);

          await new Promise((resolve) => setTimeout(()=> resolve(true), 3000))

          return {
            cid: camera.id,
            sid: sensor.id,
            data: snap
          }

        }
      }))
    }
  }
}

export default SensorModel;
