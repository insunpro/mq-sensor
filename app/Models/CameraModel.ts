import CameraApi from "../Services/CameraApi";
import Onvif from "../Services/Onvif";
import BaseModel from "./BaseModel";

class CameraModel extends BaseModel {
  static tableName = "cameras";

  //fields
  id: number;
  name: string;
  host: string;
  username: string;
  password: string;
  port: number;
  info: any;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
  numberOfSensors: number;
  profiles: any;

  // static get relationMappings() {
  //   return {
  //     province: {
  //       relation: BaseModel.HasOneRelation,
  //       modelClass: ProvinceModel,
  //       join: {
  //         from: `${this.tableName}.province_id`,
  //         to: `${ProvinceModel.tableName}.id`,
  //       },
  //     },
  //     district: {
  //       relation: BaseModel.HasOneRelation,
  //       modelClass: DistrictModel,
  //       join: {
  //         from: `${this.tableName}.district_id`,
  //         to: `${DistrictModel.tableName}.id`,
  //       },
  //     },
  //     ward: {
  //       relation: BaseModel.HasOneRelation,
  //       modelClass: WardModel,
  //       join: {
  //         from: `${this.tableName}.ward_id`,
  //         to: `${WardModel.tableName}.id`,
  //       },
  //     },
  //   };
  // }

  async ptz({ arg1, arg2, arg3 }) {
    let cameraApi = new CameraApi(this);
    return await cameraApi.ptz({ arg1, arg2, arg3 });
  }

  async snapshot() {
    let cameraApi = new CameraApi(this);
    return await cameraApi.snapshot();
  }

  // ################################### STATIC METHOD ###################################
  static async onvifDevice(camera: CameraModel) {
    let device = new Onvif.OnvifDevice({
      xaddr: camera.info.onvif || this.makeOnvifUrl(camera.host),
      user: camera.username,
      pass: camera.password,
    });

    await new Promise((resolve, reject) =>
      device
        .init()
        .then((info) => {
          console.log("OnvifDevice object has been initialized successfully.");
          resolve(info);
        })
        .catch((error) => {
          console.log("[ERROR] " + error.message);
          reject(error);
        })
    );

    return device;
  }

  static makeOnvifUrl(host: string) {
    return `http://${host}/onvif/device_service`;
  }

  static async fetchSnapShot(device) {
    return await new Promise((resolve, reject) =>
      device.fetchSnapshot((error, result) => {
        if (error) {
          console.error("onvifDevice.fetchSnapshot.error", error);
          return reject(error);
        }
        let buffer = result["body"];
        let b64 = buffer.toString("base64");
        resolve(b64);
      })
    );
  }

  static async getCurrentProfile(device) {
    return device.getCurrentProfile();
  }

  static async getProfileList(device) {
    return device.getProfileList();
  }

  static async getPresets(device, profileToken) {
    let params = {
      ProfileToken: profileToken,
    };
    return new Promise((resolve, reject) =>
      device.services.ptz
        .getPresets(params)
        .then((result) => {
          resolve(
            Array.isArray(result.data["GetPresetsResponse"]["Preset"])
              ? result.data["GetPresetsResponse"]["Preset"]
              : [result.data["GetPresetsResponse"]["Preset"]]
          );
        })
        .catch((error) => {
          reject(error);
        })
    );
  }

  static async setPreset(device, profileToken, presetName) {
    let params = {
      ProfileToken: profileToken,
      PresetName: presetName,
    };
    return new Promise((resolve, reject) =>
      device.services.ptz
        .setPreset(params)
        .then((result) => {
          resolve(result.data["SetPresetResponse"]);
        })
        .catch((error) => {
          reject(error);
        })
    );
  }

  static async removePreset(device, profileToken, presetToken) {
    let params = {
      ProfileToken: profileToken,
      PresetToken: presetToken,
    };
    return new Promise((resolve, reject) =>
      device.services.ptz
        .removePreset(params)
        .then((result) => {
          resolve(result.data);
        })
        .catch((error) => {
          reject(error);
        })
    );
  }

  static async goToPreset(device, profileToken, presetToken) {
    let params = {
      ProfileToken: profileToken,
      PresetToken: ""+presetToken,
      Speed: {x:1, y:1, z:1}
    };
    return new Promise((resolve, reject) =>
      device.services.ptz
        .gotoPreset(params)
        .then((result) => {
          resolve(result.data);
        })
        .catch((error) => {
          reject(error);
        })
    );
  }
}

export default CameraModel;
