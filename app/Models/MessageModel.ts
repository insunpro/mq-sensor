import BaseModel from "./BaseModel";

class MessageModel extends BaseModel {
  static tableName = "messages";

  //fields
  id!: string;
  key!: string;
  name: string;
  value: any;
  created_at: Date;
  updated_at: Date;
  alertLevel: number;
}

export default MessageModel;
