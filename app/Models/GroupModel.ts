import BaseModel from "./BaseModel";
import DistrictModel from "./DistrictModel";
import ProvinceModel from "./ProvinceModel";
import WardModel from "./WardModel";

class GroupModel extends BaseModel {
  static tableName = "groups";

  //fields
  id: number;
  name: string;
  description: string;
  address: string;
  province_id: number;
  district_id: number;
  ward_id: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
  numberOfDevices: number;

  static get relationMappings() {
    return {
      province: {
        relation: BaseModel.HasOneRelation,
        modelClass: ProvinceModel,
        join: {
          from: `${this.tableName}.province_id`,
          to: `${ProvinceModel.tableName}.id`,
        },
      },
      district: {
        relation: BaseModel.HasOneRelation,
        modelClass: DistrictModel,
        join: {
          from: `${this.tableName}.district_id`,
          to: `${DistrictModel.tableName}.id`,
        },
      },
      ward: {
        relation: BaseModel.HasOneRelation,
        modelClass: WardModel,
        join: {
          from: `${this.tableName}.ward_id`,
          to: `${WardModel.tableName}.id`,
        },
      },
    };
  }
}

export default GroupModel;
