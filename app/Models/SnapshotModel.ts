import path from "path";
import BaseModel from "./BaseModel";
import CameraModel from "./CameraModel";
import SensorModel from "./SensorModel";
import fs from "fs";
import moment from "moment";

interface SnapData {
    cid: number;
    sid?: number;
    data: string;
}
class SnapshotModel extends BaseModel {
  static tableName = "snapshots";

  //fields
  id: string;
  camera_id: number;
  sensor_id: number;
  src: string;
  size: number;
  detection: number;
  created_at: Date;

  static get relationMappings() {
    return {
      camera: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: CameraModel,
        join: {
          from: `${this.tableName}.camera_id`,
          to: `${CameraModel.tableName}.id`,
        },
      },
      sensor: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: SensorModel,
        join: {
          from: `${this.tableName}.sensor_id`,
          to: `${SensorModel.tableName}.id`,
        },
      }
    };
  }

  static async save(snaps: SnapData[]) {
    let folderPath = path.resolve("public/snapshot")
    if (!fs.existsSync(folderPath)) {
      fs.mkdirSync(folderPath);
    }
    let files = [];
    await Promise.all(snaps.map(async (snap) => {
      let fileName = snap.cid+"-"+moment().valueOf()+".jpg"
      let filePath = path.join(folderPath, fileName);
      let buff = Buffer.from(snap.data, "binary")
      fs.writeFileSync(filePath, buff)
      await SnapshotModel.query().insert({camera_id: snap.cid, sensor_id: snap.sid, src: filePath, size: Buffer.byteLength(buff)})
      files.push(fileName); 
    }))

    return files
  }
}

export default SnapshotModel;
