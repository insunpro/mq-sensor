import BaseModel from "./BaseModel";

class SettingModel extends BaseModel {
  static tableName = "settings";

  static idColumn = "key";

  //fields
  key!: string;
  value: any;
  created_at: Date;
  updated_at: Date;
}

export default SettingModel;
