import BaseModel from "./BaseModel";
import CameraModel from "./CameraModel";
import SensorModel from "./SensorModel";

class SensorCameraModel extends BaseModel {
  static tableName = "sensors_cameras";

  //fields
  id: number;
  sensor_id: number;
  camera_id: number;
  info: any;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;

  static get relationMappings() {
    return {
      camera: {
        relation: BaseModel.HasOneRelation,
        modelClass: CameraModel,
        join: {
          from: `${this.tableName}.camera_id`,
          to: `${CameraModel.tableName}.id`,
        },
      },
      sensor: {
        relation: BaseModel.HasOneRelation,
        modelClass: SensorModel,
        join: {
          from: `${this.tableName}.sensor_id`,
          to: `${SensorModel.tableName}.id`,
        },
      }
    };
  }
}

export default SensorCameraModel;
