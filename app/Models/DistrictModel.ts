import BaseModel from "./BaseModel";
import ProvinceModel from "./ProvinceModel";
import WardModel from "./WardModel";

class DistrictModel extends BaseModel {
  static tableName = "districts";

  //fields
  id: number;
  name: string;
  type: string;
  province_id: number;

  static get relationMappings() {
    return {
      province: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: ProvinceModel,
        join: {
          from: `${this.tableName}.province_id`,
          to: `${ProvinceModel.tableName}.id`,
        },
      },
      wards: {
        relation: BaseModel.HasManyRelation,
        modelClass: WardModel,
        join: {
          from: `${this.tableName}.id`,
          to: `${WardModel.tableName}.province_id`,
        },
      }
    };
  }

  // static async getPermissions(roleId: number) {
  //   let role = await this.query()
  //     .withGraphJoined("permissions.permission")
  //     .findById(roleId);
  //   if (!role || !role.permissions) return {};
  //   let permissions = {};
  //   role.permissions.map(object => {
  //     permissions[object.permission.key] = object.value;
  //   })
  //   return permissions;
  // }
}

export default DistrictModel;
