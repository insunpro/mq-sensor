import BaseModel from "./BaseModel";

class RoleModel extends BaseModel {
  static tableName = "roles";

  //fields
  id: number;
  name: string;
  description: string;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;

  // static get relationMappings() {
  //   return {
  //     permissions: {
  //       relation: BaseModel.HasManyRelation,
  //       modelClass: RolePermissionModel,
  //       join: {
  //         from: `${this.tableName}.id`,
  //         to: `${RolePermissionModel.tableName}.roleId`,
  //       },
  //     },
  //   };
  // }

  // static async getPermissions(roleId: number) {
  //   let role = await this.query()
  //     .withGraphJoined("permissions.permission")
  //     .findById(roleId);
  //   if (!role || !role.permissions) return {};
  //   let permissions = {};
  //   role.permissions.map(object => {
  //     permissions[object.permission.key] = object.value;
  //   })
  //   return permissions;
  // }
}

export default RoleModel;
