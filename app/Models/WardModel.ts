import BaseModel from "./BaseModel";
import DistrictModel from "./DistrictModel";
import ProvinceModel from "./ProvinceModel";

class WardModel extends BaseModel {
  static tableName = "wards";

  //fields
  id: number;
  name: string;
  type: string;
  province_id: number;
  district_id: number;

  static get relationMappings() {
    return {
      province: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: ProvinceModel,
        join: {
          from: `${this.tableName}.province_id`,
          to: `${ProvinceModel.tableName}.id`,
        },
      },
      district: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: DistrictModel,
        join: {
          from: `${this.tableName}.district_id`,
          to: `${DistrictModel.tableName}.id`,
        },
      },
    };
  }

  // static async getPermissions(roleId: number) {
  //   let role = await this.query()
  //     .withGraphJoined("permissions.permission")
  //     .findById(roleId);
  //   if (!role || !role.permissions) return {};
  //   let permissions = {};
  //   role.permissions.map(object => {
  //     permissions[object.permission.key] = object.value;
  //   })
  //   return permissions;
  // }
}

export default WardModel;
