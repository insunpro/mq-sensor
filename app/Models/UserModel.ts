import BaseModel from "./BaseModel";
import ApiException from "@app/Exceptions/ApiException";
import RoleModel from "./RoleModel";
const bcrypt = require("bcrypt");
const authConfig = require("@config/auth");

class UserModel extends BaseModel {
  static tableName = "users";

  //fields
  id!: number;
  username!: string;
  password!: string;
  name: string;
  email: string;
  phone: string;
  role_id: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;

  static get allowSelect() {
    return [
      "users.id",
      "users.username",
      "users.name",
      "users.email",
      "users.phone",
      "users.role_id",
      "users.created_at",
      "users.updated_at",
      "users.deleted_at",
    ];
  }

  static get relationMappings() {
    return {
      role: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: RoleModel,
        join: {
          from: `${this.tableName}.role_id`,
          to: `${RoleModel.tableName}.id`,
        },
      },
    };
  }

  static async checkLogin({ username, password }) {
    const user = await this.query().findOne({ username: username });
    if (!user) return false;
    let checkPassword = await this.compare(password, user.password);
    delete user.password;
    if (checkPassword) return user;
    return false;
  }

  static async hash(plainPassword) {
    return await bcrypt.hash(plainPassword + authConfig.SECRET_KEY, 10);
  }

  static async compare(plainPassword, encryptedPassword) {
    return await bcrypt.compare(
      plainPassword + authConfig.SECRET_KEY,
      encryptedPassword
    );
  }

  async changePassword(newPassword) {
    newPassword = await UserModel.hash(newPassword);
    return await this.$query().patchAndFetchById(this.id, {
      password: newPassword,
    });
  }
}

export default UserModel;
