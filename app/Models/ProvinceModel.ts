import BaseModel from "./BaseModel";
import DistrictModel from "./DistrictModel";
import WardModel from "./WardModel";

class ProvinceModel extends BaseModel {
  static tableName = "provinces";

  //fields
  id: number;
  name: string;
  type: string;

  static get relationMappings() {
    return {
      districts: {
        relation: BaseModel.HasManyRelation,
        modelClass: DistrictModel,
        join: {
          from: `${this.tableName}.id`,
          to: `${DistrictModel.tableName}.province_id`,
        },
      },
      wards: {
        relation: BaseModel.HasManyRelation,
        modelClass: WardModel,
        join: {
          from: `${this.tableName}.id`,
          to: `${WardModel.tableName}.province_id`,
        },
      }
    };
  }

  // static async getPermissions(roleId: number) {
  //   let role = await this.query()
  //     .withGraphJoined("permissions.permission")
  //     .findById(roleId);
  //   if (!role || !role.permissions) return {};
  //   let permissions = {};
  //   role.permissions.map(object => {
  //     permissions[object.permission.key] = object.value;
  //   })
  //   return permissions;
  // }
}

export default ProvinceModel;
