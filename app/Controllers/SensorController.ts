import BaseController from "./BaseController";
import SensorModel from "@app/Models/SensorModel";
import ApiException from "@app/Exceptions/ApiException";
import _ from "lodash";
import MQTT from "../Services/MQTT";
import moment from "moment-timezone";
import DeviceModel from "../Models/DeviceModel";
import { sensorModes } from "@root/config/constant";
import CameraModel from "../Models/CameraModel";
import SensorCameraModel from "../Models/SensorCameraModel";

export default class SensorController extends BaseController {
  model = SensorModel;
  deviceModel = DeviceModel;
  cameraModel = CameraModel;
  sensorCameraModel = SensorCameraModel;


  async index() {
    const inputs = this.request.all();
    return await this.model.query().getForGridTable(inputs);
  }

  async getByDevice() {
    const { deviceId } = this.request.all();
    if (!deviceId) throw new ApiException(400, "no deviceid");
    let device = await this.deviceModel.query().findById(deviceId);
    if (!device) throw new ApiException(404, "no device");
    return await this.model.query().whereNull("deleted_at").andWhere({device_id: deviceId}).orderBy([{column: 'status', order: 'desc'}, {column: 'sensorId', order: 'asc'}])
  }

  async select2() {
    const project = ["name as label", "id as value"];
    let result = await this.model.query().select(project);
    return result;
  }

  async select2camera() {
    const inputs = this.request.all();
    const { id } = inputs;

    let record = await this.model.query().findById(id).withGraphJoined("cameras");
    let camera_ids = record.cameras.map(val => val.camera_id)
    const project = ["name as label", "id as value"];
    let result = await this.cameraModel.query().select(project).whereNotIn("id", camera_ids);
    return result;
  }

  async detail() {
    const allowFields = {
      id: "number!",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let result = await this.model.getById(params.id);
    if (!result) throw new ApiException(6000, "Group doesn't exist!");
    return result;
  }

  async store() {
    return
  }
  
  async update() {
    const allowFields = {
      id: "number!",
      lat: "string",
      lng: "string",
      description: "string",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let { id } = params;
    delete params.id;
    let record = await this.model.getById(id);
    if (!record) throw new ApiException(6016, "Sensor doesn't exist!");

    let result = await this.model.query().patchAndFetchById(id, params);
    return {
      result,
      old: record,
    };
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();

    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.model.query().withGraphJoined('device').findById(params.id);
    if (!record) throw new ApiException(6016, "Sensor doesn't exist!");
    
    if (record.device.status !== 1) { //Nếu hub không online
      await record.$query().patch({status: 0, updated_at: moment()})
      throw new ApiException(7004, "Không có kết nối đến thiết bị!")
    }

    let deviceClient = MQTT.getClient(record.device.serialNumber);
    if (!deviceClient) {
      await this.deviceModel.query().findById(record.device_id).patch({status: 0, updated_at: moment()})
      await record.$query().patch({status: 0, updated_at: moment()})
      throw new ApiException(7004, "Không có kết nối đến thiết bị!")
    }
    let topic = `Mqsolutions/${record.device.serialNumber}/3`;
    let payload = {id: record.sensorId, type: 1, bat: record.battery, value: null}
    let [err] = await deviceClient.publish(topic, payload, true)
    if (err) throw new ApiException(500, err);

    let checkResponse = await deviceClient.checkResponse(topic, payload, 2);
    if (!checkResponse) {
      throw new ApiException(7005, "Không có phản hồi từ thiết bị!")
    }

    await record.$query().patch({deleted_at: moment()});
    return {
      message: "Delete successfully",
      old: record,
    };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let records = await this.model.query().whereIn("id", params.ids);
    let result = await this.model.query().findByIds(params.ids).del();
    return {
      message: `Delete ${result} records successfully`,
      old: {
        names: (records || []).map((record) => record.sensorId).join(", "),
      },
    };
  }

  async deleteAll() {
    const allowFields = {
      device_id: "number!",
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let device = await this.deviceModel.query().findById(params.device_id);
    if (!device) throw new ApiException(6016, "Device doesn't exist!");
    if (device.status !== 1) { // Nếu hub không online
      await this.model.query().where("device_id", params.device_id).update({status: 0, updated_at: moment()})
      throw new ApiException(7004, "Không có kết nối đến thiết bị!")
    }
    
    let deviceClient = MQTT.getClient(device.serialNumber);
    if (!deviceClient) {
      await this.deviceModel.query().findById(params.device_id).patch({status: 0, updated_at: moment()})
      await this.model.query().where("device_id", params.device_id).update({status: 0, updated_at: moment()})
      throw new ApiException(7004, "Không có kết nối đến thiết bị!")
    }
    let topic = `Mqsolutions/${device.serialNumber}/5`;
    let payload = {id: 1, type: 1, bat: 0, value: null}
    let [err] = await deviceClient.publish(topic, payload, true)
    if (err) throw new ApiException(500, err);

    let checkResponse = await deviceClient.checkResponse(topic, payload, 2);
    if (!checkResponse) {
      throw new ApiException(7005, "Không có phản hồi từ thiết bị!")
    }

    let result = await this.model.query().where({device_id: params.device_id}).update({deleted_at: moment()});
    return {
      message: "Delete successfully",
      old: result,
    };
  }

  async changeMode() {
    const allowFields = {
      id: "number!",
      mode: "string!",
      value: "number!"
    }
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let record = await this.model.query().findById(params.id).withGraphJoined('device');
    if (!record) throw new ApiException(6016, "Sensor doesn't exist!")

    if (record.device.status !== 1) { // Nếu không có kết nối
      throw new ApiException(7004, "Không có kết nối đến thiết bị!")
    }

    let allowModes = Object.keys(sensorModes);
    if (!allowModes.includes(params.mode)) throw new ApiException(500, `Sensor mode ${params.mode} is not allowed!`);
    if (typeof params.value !== sensorModes[params.mode].type) throw new ApiException(500, `Sensor mode value ${params.value} is not valid!`);
    let cmd = sensorModes[params.mode].cmd

    let deviceClient = MQTT.getClient(record.device.serialNumber);
    if (!deviceClient) {
      await record.$query().patch({status: 0, updated_at: moment()})
      throw new ApiException(7004, "Không có kết nối đến thiết bị 2!")
    }

    let topic = `Mqsolutions/${record.device.serialNumber}/${cmd}`;
    let payload = {id: record.sensorId, type: 1, bat: 0, value: params.value}
    let [err] = await deviceClient.publish(topic, payload, true)
    if (err) throw new ApiException(500, err);

    let checkResponse = await deviceClient.checkResponse(topic, payload, 2);
    if (!checkResponse) {
      throw new ApiException(7005, "Không có phản hồi từ thiết bị!")
    }

    let modes = record.modes || {};
    modes[params.mode] = params.value;

    return await record.$query().patch({modes, updated_at: moment()});
  }

  async indexCamera() {
    const inputs = this.request.all()
    const allowFields = {
      id: "number!",
    }
    let params = this.validate(inputs, allowFields, {removeNotAllow: true})
    let {id} = params;
    let record = await this.model.query().findById(id);
    if (!record) throw new ApiException(404, "Sensor not found");
    return await this.sensorCameraModel.query().where("sensor_id", id).withGraphJoined('camera');
  }

  async getCamera() {
    const inputs = this.request.all()
    const allowFields = {
      id: "number!",
      cid: "number!", // camera id
    }
    let params = this.validate(inputs, allowFields, {removeNotAllow: true})
    let {id, cid} = params;
    let camera: any = await this.cameraModel.query().findById(cid);
    if (!camera) throw new ApiException(404, "Camera not found")
    let sCamera = await this.sensorCameraModel.query().findOne({camera_id: cid, sensor_id: id});
    camera.setting = sCamera ? sCamera.info : null
    return camera
  }

  async updateCamera() {
    const inputs = this.request.all()
    const allowFields = {
      id: "number!",
      cid: "number!", // camera id
      h: "number!", // horizontal
      v: "number!", // vertical
      z: "number!", // zoom
    }
    let params = this.validate(inputs, allowFields, {removeNotAllow: true})
    let {id, cid} = params;
    delete params.id;
    delete params.cid;


    let record = await this.model.query().findById(id);
    if (!record) throw new ApiException(404, "Sensor not found");

    let camera = await this.cameraModel.query().findById(cid);
    if (!camera) throw new ApiException(404, "Camera not found");

    let checkData = await this.sensorCameraModel.query().findOne({camera_id: cid, sensor_id: id});

    if (checkData) {
      return await checkData.$query().patch({info: {...params}})
    }

    return await this.sensorCameraModel.query().insert({camera_id: cid, sensor_id: id, info: {...params}});
  }

  async removeCamera() {
    const inputs = this.request.all()
    const allowFields = {
      id: "number!",
      cid: "number!", // camera id
    }
    let params = this.validate(inputs, allowFields, {removeNotAllow: true})
    let {id, cid} = params;
    let record = await this.model.query().findById(id);
    if (!record) throw new ApiException(404, "Sensor not found");

    let camera = await this.cameraModel.query().findById(cid);
    if (!camera) throw new ApiException(404, "Camera not found");

    return await this.sensorCameraModel.query().where({camera_id: cid, sensor_id: id}).del()
  }
}
