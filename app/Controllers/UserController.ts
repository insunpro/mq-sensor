import BaseController from './BaseController'
import UserModel from '@app/Models/UserModel'
import ApiException from '@app/Exceptions/ApiException'
import roles from '@root/config/roles'
import RoleModel from '@app/Models/RoleModel'
export default class UserController extends BaseController {
  Model = UserModel
  roleModel = RoleModel

  async index() {
    const inputs = this.request.all()
    return await this.Model.query()
      .select(this.Model.allowSelect)
      .withGraphJoined('role')
      .getForGridTable(inputs);
  }

  async detail() {
    const inputs = this.request.all()
    const allowFields = {
      id: "number!",
    }
    let params = this.validate(inputs, allowFields);

    return await this.Model.query()
      .select(this.Model.allowSelect)
      .findById(params.id);
  }

  async store() {
    const { auth } = this.request
    let inputs = this.request.all()
    const allowFields = {
      username: "string!",
      password: "string!",
      role_id: "number!",
      name: "string",
      email: "string",
      phone: "string",
    }
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });

    params.username = params.username.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')
    let usernameExist = await this.Model.query().findOne({ 'username': params.username })
    if (usernameExist) throw new ApiException(6007, "Username already exists!")

    params.email = params.email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '').toLowerCase();
    let emailExist = await this.Model.query().findOne({ 'email': params.email });
    if (emailExist) throw new ApiException(6021, "Email already exists!")

    let role = await this.roleModel.query().findById(params.role_id)
    if (!role) throw new ApiException(7005, "User role does not exist.")

    params.password = await this.Model.hash(params['password']);

    let result = await this.Model.query().insert(params);
    delete result['password']
    return result
  }

  async update() {
    const auth = this.request.auth
    const inputs = this.request.all()
    const allowFields = {
      id: "number!",
      username: "string!",
      role_id: "number!",
      name: "string",
      email: "string",
      phone: "string",
    }
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const { id } = params
    delete params.id

    let user = await this.Model.query().findById(id)
    if (!user) throw new ApiException(7001, "User doesn't exists!")

    params.username = params.username.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')
    if(user.username !== params.username) {
      let usernameExist = await this.Model.query().findOne({'username': params.username})
      if (usernameExist) throw new ApiException(6021, "Username already exists!")
    }

    params.email = params.email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '').toLowerCase();
    if(user.email !== params.email) {
      let emailExist = await this.Model.query().findOne({'email': params.email})
      if (emailExist) throw new ApiException(6021, "Email already exists!")
    }

    let role = await this.roleModel.query().findById(params.role_id)
    if (!role) throw new ApiException(7005, "User role does not exist.")

    let result = await this.Model.query().patchAndFetchById(id, params);
    delete result['password']
    return {
      result,
      old: user
    }
  }

  async destroy() {
    const { auth } = this.request
    let params = this.request.all();
    let id = params.id;
    if (!id) throw new ApiException(9996, "ID is required!");
    if([id].includes(auth.id)) throw new ApiException(6022, "You can not remove your account.")
    let user = await this.Model.query().findById(id)
    await user.$query().delete()
    return {
      message: `Delete successfully`,
      old: user
    }
  }

  async delete() {
    const { auth } = this.request
    const allowFields = {
      ids: ["number!"]
    }
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    if(params.ids.includes(auth.id)) throw new ApiException(6022, "You can not remove your account.")
    let users = await this.Model.query().findByIds(params.ids)
    let result = await this.Model.query().findByIds(params.ids).del();
    return {
      message: `Delete ${result} records successfully`,
      old: {
        usernames: (users || []).map(user => user.username).join(', ')
      }
    };
  }
}
