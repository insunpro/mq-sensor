import BaseController from "./BaseController";
import RoleModel from "@app/Models/RoleModel";
import UserModel from "@app/Models/UserModel";
import ApiException from "@app/Exceptions/ApiException";
import _ from "lodash";

export default class RoleController extends BaseController {
  model = RoleModel;
  userModel = UserModel;

  async index() {
    const inputs = this.request.all();
    return await this.model.query().getForGridTable(inputs);
  }

  async select2() {
    const project = ["name as label", "id as value"];
    let result = await this.model.query().select(project);
    return result;
  }

  async store() {
    let inputs = this.request.all();
    const allowFields = {
      name: "string!",
      description: "string",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let name = params.name.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    let exist = await this.model.query().findOne({ name: name });
    if (exist) throw new ApiException(6014, "Role name already exists!");

    return await this.model.query().insert(params);
  }

  async detail() {
    const allowFields = {
      id: "number!",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let result = await this.model.getById(params.id);
    if (!result) throw new ApiException(6000, "Role doesn't exist!");
    return result;
  }

  async update() {
    const allowFields = {
      id: "number!",
      name: "string!",
      description: "string",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let { id } = params;
    delete params.id;
    let role = await this.model.getById(id);
    if (!role) throw new ApiException(6016, "Role doesn't exist!");

    params.name = params.name.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    if (role.name !== params.name) {
      let exist = await this.model.query().findOne({ name: params.name });
      if (exist) throw new ApiException(6014, "Group name already exists!");
    }

    let result = await this.model.query().patchAndFetchById(id, params);
    return {
      result,
      old: role,
    };
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();
    const auth = this.request.auth;

    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let role = await this.model.query().findById(params.id);
    if (!role) throw new ApiException(6016, "Group doesn't exist!");

    let checkUser = await this.userModel.query().whereIn("role_id", [role.id]);
    if (checkUser.length) {
      throw new ApiException(6017, "Group contains user cannot be deleted!");
    }

    await role.$query().delete();
    return {
      message: "Delete successfully",
      old: role,
    };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let roles = await this.model.query().whereIn("id", params.ids);
    if (roles.length !== params.ids.length)
      throw new ApiException(6016, "Group doesn't exist!");
    let checkUser = await this.userModel.query().whereIn("role_id", params.ids);
    if (!_.isEmpty(checkUser))
      throw new ApiException(6017, "Group contains user cannot be deleted!");
    let result = await this.model.query().findByIds(params.ids).del();
    return {
      message: `Delete ${result} records successfully`,
      old: {
        names: (roles || []).map((group) => group.name).join(", "),
      },
    };
  }
}
