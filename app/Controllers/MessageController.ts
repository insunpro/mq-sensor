import BaseController from "./BaseController";
import MessageModel from "@app/Models/MessageModel";
import ApiException from "@app/Exceptions/ApiException";
import _ from "lodash";
import UserModel from "../Models/UserModel";

export default class DeviceController extends BaseController {
  model = MessageModel;

  async index() {
    const inputs = this.request.all();
    if (!_.has(inputs, "sorting")) {
      inputs.sorting = [
        JSON.stringify({ field: "messages.alertLevel", direction: "desc" }),
        JSON.stringify({ field: "messages.key", direction: "asc" }),
      ];
    }
    return await this.model.query().getForGridTable(inputs);
  }

  async detail() {
    const allowFields = {
      id: "number!",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let result = await this.model.getById(params.id);
    if (!result) throw new ApiException(6000, "Message doesn't exist!");
    return result;
  }

  async store() {
    return;
  }
  
  async update() {
    const inputs = this.request.all();
    const allowFields = {
      id: "number!",
      alertLevel: "number!"
    }
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.model.query().findById(params.id);
    if (!record) throw new ApiException(404, "Record not found");

    return await record.$query().patch(params);
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();

    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.model.query().findById(params.id);
    if (!record) throw new ApiException(6016, "Message doesn't exist!");

    await record.$query().delete();
    return {
      message: "Delete successfully",
      old: record,
    };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let records = await this.model.query().whereIn("id", params.ids);
    let result = await this.model.query().findByIds(params.ids).del();
    return {
      message: `Delete ${result} records successfully`,
      old: {
        names: (records || []).map((record) => record.key).join(", "),
      },
    };
  }
}
