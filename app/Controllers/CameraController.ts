import BaseController from "./BaseController";
import CameraModel from "@app/Models/CameraModel";
import ApiException from "@app/Exceptions/ApiException";
import _, { reject } from "lodash";
import moment from "moment-timezone";
import CameraApi from "../Services/CameraApi";
import SensorModel from "../Models/SensorModel";
import fs from "fs";
import path, { resolve } from "path";
import SnapshotModel from "../Models/SnapshotModel";
import TelegramService from "../Services/Telegram";
import Onvif from "../Services/Onvif";
import SensorCameraModel from "../Models/SensorCameraModel";

export default class GroupController extends BaseController {
  model = CameraModel;
  sensorModel = SensorModel;
  sensorCameraModel = SensorCameraModel;

  async index() {
    const { auth } = this.request;
    const inputs = this.request.all();
    return await this.model.query().getForGridTable(inputs);
  }

  async select2() {
    const project = ["name as label", "id as value"];
    let result = await this.model.query().select(project);
    return result;
  }

  async store() {
    let inputs = this.request.all();
    const allowFields = {
      name: "string!",
      host: "string!",
      // port: "number!",
      username: "string!",
      password: "string!",
      info: "object",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let name = params.name.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    let exist = await this.model.query().findOne({ name: name });
    if (exist) throw new ApiException(6014, "Camera name already exists!");

    return await this.model.query().insert(params);
  }

  async detail() {
    const allowFields = {
      id: "number!",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let result = await this.model.getById(params.id);
    if (!result) throw new ApiException(6000, "Group doesn't exist!");
    return result;
  }

  async update() {
    const allowFields = {
      id: "number!",
      name: "string!",
      host: "string!",
      // port: "number!",
      username: "string!",
      password: "string!",
      info: "object",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let { id } = params;
    delete params.id;
    let record = await this.model.getById(id);
    if (!record) throw new ApiException(6016, "Camera doesn't exist!");

    params.name = params.name.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    if (record.name !== params.name) {
      let exist = await this.model.query().findOne({ name: params.name });
      if (exist) throw new ApiException(6014, "Camera name already exists!");
    }
    params.updated_at = moment();

    if (params.info) {
      let info = { ...record.info, ...params.info };
      params.info = info;
    }

    let result = await this.model.query().patchAndFetchById(id, params);
    return {
      result,
      old: record,
    };
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();

    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.model.query().findById(params.id);
    if (!record) throw new ApiException(6016, "Camera doesn't exist!");

    await record.$query().delete();
    return {
      message: "Delete successfully",
      old: record,
    };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let records = await this.model.query().whereIn("id", params.ids);
    let result = await this.model.query().findByIds(params.ids).del();
    return {
      message: `Delete ${result} records successfully`,
      old: {
        names: (records || []).map((record) => record.name).join(", "),
      },
    };
  }

  async snapshot() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();

    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.model.query().findById(params.id);
    if (!record) throw new ApiException(6016, "Camera doesn't exist!");

    let snapData: any = await record.snapshot();
    let data = {
      cid: record.id,
      data: snapData,
    };
    let imgs = await SnapshotModel.save([data]);
    return {
      img: imgs[0] ? imgs[0] : null,
    };
  }

  async lastSnap() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();

    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.model.query().findById(params.id);
    if (!record) throw new ApiException(6016, "Camera doesn't exist!");

    let snap = await SnapshotModel.query()
      .findOne({ camera_id: record.id })
      .orderBy("created_at", "DESC");
    if (!snap) return {};

    record["img"] = path.basename(snap.src);

    return record;
  }

  /******************** ONVIF ************************/
  async onvifSearch() {
    let res: any = await new Promise((resolve, reject) => {
      Onvif.startProbe()
        .then((device_info_list) => {
          console.log(device_info_list.length + " devices were found.");
          resolve(device_info_list);
        })
        .catch((error) => {
          console.error(error);
          reject(error);
        });
    });

    res = res || [];

    // if (res && res instanceof Array && res.length == 0) {
    //   res = [
    //     {
    //       urn: "urn:uuid:48534d41-5254-4c49-4e4b-BD-8A-1B-AE-A3-00",
    //       name: "NETCAM",
    //       hardware: "MODEL",
    //       location: "ShenZhen",
    //       types: ["dn:NetworkVideoTransmitter"],
    //       xaddrs: ["http://192.168.6.101:10080/onvif/device_service"],
    //       scopes: [
    //         "onvif://www.onvif.org/type/video_encoder",
    //         "onvif://www.onvif.org/type/audio_encoder",
    //         "onvif://www.onvif.org/location/city/ShenZhen",
    //         "onvif://www.onvif.org/hardware/MODEL",
    //         "onvif://www.onvif.org/name/NETCAM",
    //         "onvif://www.onvif.org/VENDOR_MODEL",
    //         "onvif://www.onvif.org/Profile/Streaming",
    //         "",
    //       ],
    //     },
    //     {
    //       urn: "urn:uuid:694fc012-55a2-43b1-84ea-000b42229de5",
    //       name: "",
    //       hardware: "",
    //       location: "",
    //       types: ["tdn:NetworkVideoTransmitter", "tds:Device"],
    //       xaddrs: ["http://192.168.6.49:8888/onvif/device_service"],
    //       scopes: [
    //         "onvif://www.onvif.org/Profile/T",
    //         "onvif://www.onvif.org/Profile/Streaming",
    //         "onvif://www.onvif.org/type/Network_Video_Transmitter",
    //         "onvif://www.onvif.org/type/video_encoder",
    //         "onvif://www.onvif.org/MAC/00:0B:42:22:9D:E5",
    //         "",
    //       ],
    //     },
    //     {
    //       urn: "uuid:1b49d5c5-9b72-1ee6-9a69-6dc5465e9b72",
    //       name: "General",
    //       hardware: "DHI-ASI7213X-T1",
    //       location: "china",
    //       types: ["dn:NetworkVideoTransmitter", "tds:Device"],
    //       xaddrs: ["http://192.168.6.253/onvif/device_service"],
    //       scopes: [
    //         "onvif://www.onvif.org/location/country/china",
    //         "onvif://www.onvif.org/name/General",
    //         "onvif://www.onvif.org/hardware/DHI-ASI7213X-T1",
    //         "onvif://www.onvif.org/extension/unique_identifier/1",
    //         "onvif://www.onvif.org/Profile/C",
    //       ],
    //     },
    //     {
    //       urn: "uuid:f9411fda-9471-bb86-ddb0-7bcb4e9c9471",
    //       name: "General",
    //       hardware: "MQ-Xsenses-TPC_XZ8-0002",
    //       location: "china",
    //       types: ["dn:NetworkVideoTransmitter", "tds:Device"],
    //       xaddrs: ["http://192.168.6.208/onvif/device_service"],
    //       scopes: [
    //         "onvif://www.onvif.org/location/country/china",
    //         "onvif://www.onvif.org/name/General",
    //         "onvif://www.onvif.org/hardware/MQ-Xsenses-TPC_XZ8-0002",
    //         "onvif://www.onvif.org/Profile/Streaming",
    //         "onvif://www.onvif.org/type/Network_Video_Transmitter",
    //         "onvif://www.onvif.org/extension/unique_identifier/1",
    //         "onvif://www.onvif.org/Profile/T",
    //       ],
    //     },
    //     {
    //       urn: "uuid:73521892-8052-0739-ed7d-9db649ca8052",
    //       name: "General",
    //       hardware: "IP_PTZ_Camera",
    //       location: "china",
    //       types: ["dn:NetworkVideoTransmitter", "tds:Device"],
    //       xaddrs: ["http://192.168.6.46/onvif/device_service"],
    //       scopes: [
    //         "onvif://www.onvif.org/location/country/china",
    //         "onvif://www.onvif.org/name/General",
    //         "onvif://www.onvif.org/hardware/IP_PTZ_Camera",
    //         "onvif://www.onvif.org/Profile/Streaming",
    //         "onvif://www.onvif.org/type/Network_Video_Transmitter",
    //         "onvif://www.onvif.org/extension/unique_identifier/1",
    //         "onvif://www.onvif.org/Profile/T",
    //       ],
    //     },
    //     {
    //       urn: "uuid:df3112b8-872e-baad-e0a6-4b3248a1872e",
    //       name: "General",
    //       hardware: "IP_PTZ_Camera",
    //       location: "china",
    //       types: ["dn:NetworkVideoTransmitter", "tds:Device"],
    //       xaddrs: ["http://192.168.6.43/onvif/device_service"],
    //       scopes: [
    //         "onvif://www.onvif.org/location/country/china",
    //         "onvif://www.onvif.org/name/General",
    //         "onvif://www.onvif.org/hardware/IP_PTZ_Camera",
    //         "onvif://www.onvif.org/Profile/Streaming",
    //         "onvif://www.onvif.org/type/Network_Video_Transmitter",
    //         "onvif://www.onvif.org/extension/unique_identifier/1",
    //         "onvif://www.onvif.org/Profile/T",
    //       ],
    //     },
    //   ];
    // }
    res = _.compact(res);

    const ipv4 =
      /((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/;
    let ips = [];
    res.map((r: any) => {
      r.xaddrs = r.xaddrs && r.xaddrs.length ? r.xaddrs[0] : "";
      r.existed = false;
      r.host = "";
      let matches = r.xaddrs.match(ipv4);
      if (matches && matches[0]) {
        ips.push(matches[0]);
        r.host = matches[0];
      }
    });

    if (ips.length) {
      let cameras = await this.model.query().whereIn("host", ips);
      if (cameras.length) {
        cameras.map((camera) => {
          res.find((r) => r.xaddrs.indexOf(camera.host) > -1).existed = true;
        });
      }
    }

    return _.orderBy(res, "existed", "asc");
  }

  async onvifConnect() {
    const inputs = this.request.all();
    const allowFields = {
      username: "string!",
      password: "string!",
      xaddrs: "string!",
    };
    const { xaddrs, username, password } = this.validate(inputs, allowFields);
    let camera = new Onvif.OnvifDevice({
      xaddr: xaddrs,
      user: username,
      pass: password,
    });

    let connection: any = await new Promise((resolve, reject) =>
      camera
        .init()
        .then((info) => {
          console.log(
            "The OnvifDevice object has been initialized successfully."
          );
          // console.log(JSON.stringify(info, null, '  '));
          resolve(info);
        })
        .catch((error) => {
          reject(error);
        })
    );

    let profiles = camera.getProfileList();

    return { ...connection, profiles };
  }

  async onvifSave() {
    const inputs = this.request.all();
    const allowFields = {
      username: "string!",
      password: "string!",
      xaddrs: "string!",
      host: "string!",
      Manufacturer: "string",
      Model: "string",
      FirmwareVersion: "string",
      SerialNumber: "string",
      HardwareId: "string",
      hardware: "string",
      location: "string",
      profiles: "object",
    };
    const params = this.validate(inputs, allowFields);
    let newCamera = {
      name: params.host,
      host: params.host,
      username: params.username,
      password: params.password,
      info: {
        onvif: params.xaddrs,
        manufacturer: params.Manufacturer,
        model: params.Model,
        firmwareVersion: params.FirmwareVersion,
        serialNumber: params.SerialNumber,
        hardwareId: params.HardwareId,
        hardware: params.hardware,
        location: params.location,
        verticalAngle: 60,
        horizontalAngle: 30,
        zoom: 1,
      },
      profiles: JSON.stringify(params.profiles),
    };
    return await this.model.query().insert(newCamera);
  }

  async savePreset() {
    const inputs = this.request.all();
    const allowFields = {
      id: "number!",
      sid: "number!",
    };
    const { id, sid } = this.validate(inputs, allowFields);
    let camera = await this.model.query().findById(id);
    if (!camera) throw new ApiException(404, "no camera");
    let sensor = await this.sensorModel.query().findById(sid);
    if (!sensor) throw new ApiException(404, "no sensor");
    let device = await this.model.onvifDevice(camera);
    let profile = await this.model.getCurrentProfile(device);

    let presetName = "cid"+id+"-"+"sid"+sid;

    let exist = await this.sensorCameraModel.query().findOne({camera_id: id, sensor_id: sid});
    if (exist) {
      let currentPresets: any = await this.model.getPresets(device, profile.token);
      let existPreset = currentPresets.find(value => value["Name"] === presetName || value["Name"] === "sid"+sid+"-"+"cid"+id);
      if (existPreset) {
        await this.model.removePreset(device, profile.token, existPreset["$"]["token"]);
      }
      await exist.$query().del();
    }

    let preset: any = await this.model.setPreset(
      device,
      profile.token,
      presetName
    );

    let info = {
      ...preset,
      profile: profile.token
    }

    return await this.sensorCameraModel
      .query()
      .insert({
        camera_id: camera.id,
        sensor_id: sensor.id,
        info: JSON.stringify(info),
      });
  }

  async removePreset() {
    const inputs = this.request.all();
    const allowFields = {
      id: "number!",
      sid: "number!",
    };
    const { id, sid } = this.validate(inputs, allowFields);
    let camera = await this.model.query().findById(id);
    if (!camera) throw new ApiException(404, "no camera");
    let sensor = await this.sensorModel.query().findById(sid);
    if (!sensor) throw new ApiException(404, "no sensor");
    let device = await this.model.onvifDevice(camera);
    let profile = await this.model.getCurrentProfile(device);

    let presetName = "cid"+id+"-"+"sid"+sid;

    let exist = await this.sensorCameraModel.query().findOne({camera_id: id, sensor_id: sid});
    if (exist) {
      let currentPresets: any = await this.model.getPresets(device, profile.token);
      console.log(currentPresets)
      let existPreset = currentPresets.find(value => value["Name"] === presetName || value["Name"] === "sid"+sid+"-"+"cid"+id);
      if (existPreset) {
        await this.model.removePreset(device, profile.token, existPreset["$"]["token"]);
      }
      return await exist.$query().del();
    }
  }

  async ptz() {
    // const allowFields = {
    //   cid: "number!",
    //   arg1: "number!",
    //   arg2: "number!",
    //   arg3: "number!"
    // };
    // const inputs = this.request.all();
    // let {cid, arg1, arg2, arg3} = this.validate(inputs, allowFields);

    // let camera = await this.model.query().findById(cid);
    // if (!camera) return {};

    // let cameraApi = new CameraApi(camera);

    // return await cameraApi.ptz({arg1, arg2, arg3});
    // let status: any = await cameraApi.ptz({action: "getStatus"});
    // let config: any = await cameraApi.getConfig("Ptz");

    // return {status: status.split("\r\n"), config: config.split("\r\n")}

    // const allowFields = {
    //   sid: "number!",
    //   value: "number!",
    // };
    // const inputs = this.request.all();
    // let params = this.validate(inputs, allowFields);

    // let snaps = await SensorModel.snap(2, {id: params.sid, value: params.value})

    // return await SnapshotModel.save(snaps)

    // return await TelegramService.send("Tin nhắn tự động", ['4-1628214799774.jpg'])

    const allowFields = {
      cid: "number!",
      token: "number"
    };
    const inputs = this.request.all();
    let { cid, token } = this.validate(inputs, allowFields);
    let camera = await this.model.query().findById(cid);
    if (!camera) throw new ApiException(404, "no camera");

    let device = await this.model.onvifDevice(camera);

    let profile = await this.model.getCurrentProfile(device);

    return await this.model.goToPreset(device, profile.token, ""+token)

    let params = {
      ProfileToken: "MediaProfile00000",
    };
    return new Promise((resolve, reject) =>
      device.services.ptz
        .getPresets(params)
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        })
    );

    // let params = {
    //   ProfileToken: "MediaProfile00000",
    //   PresetName: "sensor1"
    // }
    // return new Promise((resolve, reject) => device.services.ptz.setPreset(params).then(result => {
    //   resolve(result)
    // }).catch((error) => {
    //   reject(error)
    // })
    // )

    // return new Promise((resolve, reject) => device.fetchSnapshot((error, result) => {
    //   if (error) return reject("onvifDevice.fetchSnapshot.error" + error);
    //   let buffer = result['body'];
    //   let b64 = buffer.toString('base64');
    //   resolve(b64)
    // }))
  }
}
