import BaseController from "./BaseController";
import DeviceModel from "@app/Models/DeviceModel";
import ApiException from "@app/Exceptions/ApiException";
import _ from "lodash";
import UserModel from "../Models/UserModel";
import MQTT from "../Services/MQTT";
import { deviceModes } from "@root/config/constant";
import moment from "moment-timezone";

export default class DeviceController extends BaseController {
  model = DeviceModel;

  async index() {
    const { auth } = this.request;
    const inputs = this.request.all();
    let result = await this.model
      .query()
      .withGraphJoined("[group]")
      .getForGridTable(inputs);
    return result;
  }

  async select2() {
    const project = ["name as label", "id as value"];
    let result = await this.model.query().select(project);
    return result;
  }

  async detail() {
    const allowFields = {
      id: "number!",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let result = await this.model.getById(params.id);
    if (!result) throw new ApiException(6000, "Group doesn't exist!");
    return result;
  }

  async store() {
    return;
    // let inputs = this.request.all();
    // const allowFields = {
    //   serialNumber: "string!",
    //   name: "string",
    //   lat: "string",
    //   lng: "string",
    //   status: "number",
    //   group_id: "number"
    // };
    // let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    // let name = params.name.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    // let exist = await this.model.query().findOne({ name: name });
    // if (exist) throw new ApiException(6014, "Group name already exists!");

    // return await this.model.query().insert(params);
  }

  async update() {
    const allowFields = {
      id: "number!",
      serialNumber: "string",
      name: "string",
      lat: "string",
      lng: "string",
      status: "number",
      group_id: "number",
      modes: "object",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let { id } = params;
    delete params.id;
    let record = await this.model.getById(id);
    if (!record) throw new ApiException(6016, "Device doesn't exist!");

    if (params.name && record.name !== params.name) {
      params.name = params.name.replace(
        /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        ""
      );
      let exist = await this.model.query().findOne({ name: params.name });
      if (exist) throw new ApiException(6014, "Device name already exists!");
    }

    let result = await this.model.query().patchAndFetchById(id, params);
    return {
      result,
      old: record,
    };
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();

    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.model.query().findById(params.id);
    if (!record) throw new ApiException(6016, "Device doesn't exist!");

    await record.$query().delete();
    return {
      message: "Delete successfully",
      old: record,
    };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let records = await this.model.query().whereIn("id", params.ids);
    let result = await this.model.query().findByIds(params.ids).del();
    return {
      message: `Delete ${result} records successfully`,
      old: {
        names: (records || []).map((record) => record.name).join(", "),
      },
    };
  }

  async changeMode() {
    const allowFields = {
      id: "number!",
      mode: "string!",
      value: "number!",
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let record = await this.model.query().findById(params.id);
    if (!record) throw new ApiException(6016, "Device doesn't exist!");

    if (record.status !== 1) {
      // Nếu không có kết nối
      throw new ApiException(7004, "Không có kết nối đến thiết bị!");
    }

    let allowModes = Object.keys(deviceModes);
    if (!allowModes.includes(params.mode))
      throw new ApiException(500, `Device mode ${params.mode} is not allowed!`);
    if (typeof params.value !== deviceModes[params.mode].type)
      throw new ApiException(
        500,
        `Device mode value ${params.value} is not valid!`
      );
    let cmd = deviceModes[params.mode].cmd;

    let deviceClient = MQTT.getClient(record.serialNumber);
    if (!deviceClient) {
      await record.$query().patch({ status: 0, updated_at: moment() });
      throw new ApiException(7004, "Không có kết nối đến thiết bị 2!");
    }

    let topic = `Mqsolutions/${record.serialNumber}/${cmd}`;
    let payload = { id: 1, type: 1, bat: 0, value: params.value };
    let [err] = await deviceClient.publish(topic, payload, true);
    if (err) throw new ApiException(500, err);

    let checkResponse = await deviceClient.checkResponse(
      topic,
      payload,
      2,
      true
    );
    if (!checkResponse) {
      throw new ApiException(7005, "Không có phản hồi từ thiết bị!");
    }

    let modes = record.modes || {};
    modes[params.mode] = params.value;

    return await record.$query().patch({ modes, updated_at: moment() });
  }

  async getClients() {
    return MQTT.clients;
  }
}
