import BaseController from "./BaseController";
import GroupModel from "@app/Models/GroupModel";
import ApiException from "@app/Exceptions/ApiException";
import _ from "lodash";

export default class GroupController extends BaseController {
  model = GroupModel;

  async index() {
    const { auth } = this.request;
    const inputs = this.request.all();
    return await this.model.query().withGraphJoined("[province, district, ward]").getForGridTable(inputs);
  }

  async select2() {
    const project = ["name as label", "id as value"];
    let result = await this.model.query().select(project);
    return result;
  }

  async store() {
    let inputs = this.request.all();
    const allowFields = {
      name: "string!",
      description: "string",
      address: "string",
      province_id: "number",
      district_id: "number",
      ward_id: "number"
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let name = params.name.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    let exist = await this.model.query().findOne({ name: name });
    if (exist) throw new ApiException(6014, "Group name already exists!");

    return await this.model.query().insert(params);
  }

  async detail() {
    const allowFields = {
      id: "number!",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let result = await this.model.getById(params.id);
    if (!result) throw new ApiException(6000, "Group doesn't exist!");
    return result;
  }

  async update() {
    const allowFields = {
      id: "number!",
      name: "string!",
      description: "string",
      address: "string",
      province_id: "number",
      district_id: "number",
      ward_id: "number"
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let { id } = params;
    delete params.id;
    let record = await this.model.getById(id);
    if (!record) throw new ApiException(6016, "Group doesn't exist!");

    params.name = params.name.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    if (record.name !== params.name) {
      let exist = await this.model.query().findOne({ name: params.name });
      if (exist) throw new ApiException(6014, "Group name already exists!");
    }

    let result = await this.model.query().patchAndFetchById(id, params);
    return {
      result,
      old: record,
    };
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();

    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.model.query().findById(params.id);
    if (!record) throw new ApiException(6016, "Group doesn't exist!");

    await record.$query().delete();
    return {
      message: "Delete successfully",
      old: record,
    };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let records = await this.model.query().whereIn("id", params.ids);
    let result = await this.model.query().findByIds(params.ids).del();
    return {
      message: `Delete ${result} records successfully`,
      old: {
        names: (records || []).map((record) => record.name).join(", "),
      },
    };
  }
}
