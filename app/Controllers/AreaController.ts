import BaseController from "./BaseController";
import ApiException from "@app/Exceptions/ApiException";
import _ from "lodash";
import ProvinceModel from "@app/Models/ProvinceModel";
import DistrictModel from "@app/Models/DistrictModel";
import WardModel from "@app/Models/WardModel";


export default class AreaController extends BaseController {
  provinceModel = ProvinceModel;
  districtModel = DistrictModel;
  wardModel = WardModel;

  async province() {
    const inputs = this.request.all();
    return await this.provinceModel.query().getForGridTable(inputs);
  }

  async select2province() {
    const project = ["name as label", "id as value"];
    let result = await this.provinceModel.query().select(project);
    return result;
  }

  async district() {
    const inputs = this.request.all();
    return await this.districtModel.query().getForGridTable(inputs);
  }

  async select2district() {
    const project = ["name as label", "id as value"];
    const {province_id} = this.request.all();
    if (!province_id) {
      return await this.districtModel.query().select(project);
    }
    return await this.districtModel.query().select(project).where("province_id", province_id);
  }

  async ward() {
    const inputs = this.request.all();
    return await this.wardModel.query().getForGridTable(inputs);
  }

  async select2ward() {
    const project = ["name as label", "id as value"];
    const {district_id} = this.request.all();
    if (!district_id) {
      return await this.wardModel.query().select(project);
    }
    return await this.wardModel.query().select(project).where("district_id", district_id);
  }

  async detail() {
    return;
  }

  async store() {
    return;
  }

  async update() {
    return;
  }

  async destroy() {
    return Promise.resolve({message: ""});
  }

  async delete() {
    return;
  }
}
