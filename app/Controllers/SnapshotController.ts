import BaseController from "./BaseController";
import SnapshotModel from "@app/Models/SnapshotModel";
import ApiException from "@app/Exceptions/ApiException";
import _ from "lodash";
import fs from "fs";
import path from "path";

export default class SnapshotController extends BaseController {
  model = SnapshotModel;

  async index() {
    const inputs = this.request.all();
    if (!_.has(inputs, "sorting")) {
      inputs.sorting = [
        JSON.stringify({ field: "created_at", direction: "desc" }),
      ];
    }
    let imgs = await this.model.query().whereNull("size");
    for(let img of imgs) {
      if (fs.existsSync(img.src)) {
        let stat = fs.statSync(img.src);
        await img.$query().patch({size: stat.size})
      }
    }
    return await this.model.query().getForGridTable(inputs);
  }

  async detail() {
    const allowFields = {
      id: "number!",
    };
    let inputs = this.request.all();
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let result = await this.model.getById(params.id);
    if (!result) throw new ApiException(6000, "Record doesn't exist!");
    return result;
  }

  async store() {
    return;
  }
  
  async update() {
    return;
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();

    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.model.query().findById(params.id);
    if (!record) throw new ApiException(6016, "Record doesn't exist!");

    await record.$query().delete();
    return {
      message: "Delete successfully",
      old: record,
    };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    let params = this.validate(inputs, allowFields);
    let records = await this.model.query().whereIn("id", params.ids);
    let result = await this.model.query().findByIds(params.ids).del();
    return {
      message: `Delete ${result} records successfully`,
      old: {
        names: (records || []).map((record) => path.basename(record.src)).join(", "),
      },
    };
  }
}
