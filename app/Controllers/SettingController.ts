import BaseController from "./BaseController";
import ApiException from "@app/Exceptions/ApiException";
import _ from "lodash";
import SettingModel from "@app/Models/SettingModel";
import moment from "moment-timezone";

export default class AreaController extends BaseController {
  settingModel = SettingModel;

  async getMain() {
    let result = await this.settingModel.query().findOne({ key: "main" });
    return result || {};
  }

  async updateMain() {
    const inputs = this.request.all();
    const allowFields = {
      alertLevel: 'number!',
      services: 'object!',
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });

    let record = await this.settingModel.query().findOne({ key: "main" });
    if (!record) {
      record = await this.settingModel.query().insert({ key: "main" });
    }
    return record.$query().patch({ value: params, updated_at: moment() });
  } 

  async getZalo() {
    let result = await this.settingModel.query().findOne({ key: "zalo" });
    return result || {};
  }

  async updateZalo() {
    const inputs = this.request.all();
    const allowFields = {
      uuid: "string!",
      cookie: "string!",
      groupId: "string!",
      delay: "number!"
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.settingModel.query().findOne({ key: "zalo" });
    if (!record) {
      record = await this.settingModel.query().insert({ key: "zalo" });
    }
    return record.$query().patch({ value: params, updated_at: moment() });
  }

  async getEmail() {
    let result = await this.settingModel.query().findOne({ key: "email" });
    return result || {};
  }

  async updateEmail() {
    const inputs = this.request.all();
    const allowFields = {
      email: "string!",
      password: "string!",
      toEmails: "string!",
      delay: "number!",
      name: "string!"
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.settingModel.query().findOne({ key: "email" });
    if (!record) {
      record = await this.settingModel.query().insert({ key: "email" });
    }
    return record.$query().patch({ value: params, updated_at: moment() });
  }

  async getTelegram() {
    let result = await this.settingModel.query().findOne({ key: "telegram" });
    return result || {};
  }

  async updateTelegram() {
    const inputs = this.request.all();
    const allowFields = {
      token: "string!",
      toIds: "string!",
      delay: "number!"
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });
    let record = await this.settingModel.query().findOne({ key: "telegram" });
    if (!record) {
      record = await this.settingModel.query().insert({ key: "telegram" });
    }
    return record.$query().patch({ value: params, updated_at: moment() });
  }

  async detail() {
    return;
  }

  async store() {
    return;
  }

  async update() {
    return;
  }

  async destroy() {
    return Promise.resolve({ message: "" });
  }

  async delete() {
    return;
  }
}
