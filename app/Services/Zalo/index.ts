import superagent from "superagent";
import SettingModel from "@app/Models/SettingModel";
import _ from "lodash"
class Zalo {
  static async getSetting() {
    let result = await SettingModel.query().findOne({key: "zalo"});
    return result ? result.value || {} : {};
  }

  static async sendToGroup(message) {
    let setting = await this.getSetting();
    if (_.isEmpty(setting)) {
      console.error("Zalo.sendToGroup.noSetting")
      return;
    }
    if (!message) {
      console.error("Zalo.sendToGroup.noMessage")
      return;
    }
    try {
      const res = await superagent
        .post("http://zalo.ngochip.net/api/v1/zalo/group")
        .send({
          groupCode: setting.groupId,
          message: message,
          uuid: setting.uuid,
          cookie: setting.cookie,
        });
    } catch (err) {
      throw err;
    }
    await new Promise(resolve => setTimeout(resolve, setting.delay * 1000));
  }

  static async sendToPhone(message) {
    let setting = await this.getSetting();
    if (_.isEmpty(setting)) {
      console.error("Zalo.sendToGroup.noSetting")
      return;
    }
    if (!message) {
      console.error("Zalo.sendToGroup.noMessage")
      return;
    }
    try {
      const res = await superagent
        .post("http://zalo.ngochip.net/api/v1/zalo/group")
        .send({
          phone: "0838585085",
          message: message,
          uuid: setting.uuid,
          cookie: setting.cookie,
        });
    } catch (err) {
      throw err;
    }
    await new Promise(resolve => setTimeout(resolve, setting.delay * 1000));
  }
}

export default Zalo;
