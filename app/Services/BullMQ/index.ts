import CameraQueue from "./camera";
import EmailQueue from "./email";
import TelegramQueue from "./telegram";
import ZaloQueue from "./zalo";

export default class BullMQ {
  static start() {
    if (process.env.MODE == "dev-client") return false;
    try {
      ZaloQueue.start();
      EmailQueue.start();
      TelegramQueue.start();
      // CameraQueue.start();
    }
    catch (err) {
      console.error("BullMQ.error", err)
    }
  }
}

export {
  ZaloQueue,
  EmailQueue,
  TelegramQueue
}