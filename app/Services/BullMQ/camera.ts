import { Queue, Worker, Job, QueueScheduler } from "bullmq";
import ZaloService from "@app/Services/Zalo";
import IORedis from "ioredis";
import CameraModel from "@root/app/Models/CameraModel";

const defaultConnection = {
  port: Number(process.env.REDIS_PORT) || 6379, // Redis port
  host: process.env.REDIS_HOST || "127.0.0.1", // Redis host
  password: process.env.REDIS_PASS,
  db: Number(process.env.REDIS_CAMERA) || 1,
  showFriendlyErrorStack: true,
}

export default class CameraQueue {
  private static connection;
  private static queues: Queue[] = [];
  private static workers: Worker[] = [];

  static start() {
    this.connection = new IORedis(defaultConnection)
    this.connection.on("error", (error) => this.handleError(error, 'connection'))

    try {
      (async () => {
        let cameras = await CameraModel.query();
        cameras.map(camera => {
          let queue = new Queue('cam-'+camera.id, {connection: this.connection});
          queue.on("error", (error) => this.handleError(error, 'queue-'+camera.id));

          let worker = new Worker('cam-'+camera.id, this.handleProcess, {connection: this.connection});
          worker.on("completed", this.handleComplete)
          worker.on("failed", this.handleFailed)
          worker.on("error", (error) => this.handleError(error, 'worker-'+camera.id))
          this.queues.push(queue);
          this.workers.push(worker);
          console.log(worker.name)
        })
      })()
    }
    catch (err) {
      console.error("CameraQueue.start", err)
    }

    console.log("[CameraQueue] has started on", defaultConnection.host + ':' + defaultConnection.port + ':' + defaultConnection.db);
  }

  // Add job
  static async add(name: "user" | "group", data: any) {
    return
    // return await this.queue.add(name, data)
  }

  // Handle job
  private static async handleProcess(job: Job) {
    let { message } = job.data
    if (job.name === "ptz") {
      // await ZaloService.sendToPhone(message)
    }
    if (job.name === "snap") {
      // await ZaloService.sendToGroup(message)
    }
    return true
  }

  private static async handleComplete(job: Job, returnvalue: any) {
    // console.log('camera.handleComplete', returnvalue)
  }

  private static async handleFailed(job: Job, failedReason: string) {
    console.error('camera.handleFailed', job, failedReason)
  }

  private static async handleError(error, from="") {
    console.error('camera.handleError', from, error)
  }
}