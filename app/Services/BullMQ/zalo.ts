import { Queue, Worker, Job, QueueScheduler } from "bullmq";
import ZaloService from "@app/Services/Zalo";
import IORedis from "ioredis";

const defaultConnection = {
  port: Number(process.env.REDIS_PORT) || 6379, // Redis port
  host: process.env.REDIS_HOST || "127.0.0.1", // Redis host
  password: process.env.REDIS_PASS,
  db: Number(process.env.REDIS_ZALO) || 2,
  showFriendlyErrorStack: true,
}

export default class ZaloQueue {
  private static connection;
  private static queue: Queue;
  private static worker: Worker;

  static start() {
    if (!this.queue) {
      this.connection = new IORedis(defaultConnection)
      this.connection.on("error", (error) => this.handleError(error, 'connection'))

      // this.scheduler = new QueueScheduler('zalo', {connection: this.connection})

      this.queue = new Queue('zalo', {connection: this.connection});
      this.queue.on("error", (error) => this.handleError(error, 'queue'));

      this.worker = new Worker('zalo', this.handleProcess, {connection: this.connection});
      this.worker.on("completed", this.handleComplete)
      this.worker.on("failed", this.handleFailed)
      this.worker.on("error", (error) => this.handleError(error, 'worker'))

      console.log("[ZaloQueue] has started on", defaultConnection.host + ':' + defaultConnection.port + ':' + defaultConnection.db);
    }
  }

  // Add job
  static async add(name: "user" | "group", data: any) {
    return await this.queue.add(name, data, { removeOnComplete: 100, removeOnFail: 100 })
  }

  // Handle job
  private static async handleProcess(job: Job) {
    let { message } = job.data
    if (job.name === "user") {
      await ZaloService.sendToPhone(message)
    }
    if (job.name === "group") {
      await ZaloService.sendToGroup(message)
    }
    return true
  }

  private static async handleComplete(job: Job, returnvalue: any) {
    // console.log('zalo.handleComplete', returnvalue)
  }

  private static async handleFailed(job: Job, failedReason: string) {
    console.error('zalo.handleFailed', job, failedReason)
  }

  private static async handleError(error, from="") {
    console.error('zalo.handleError', from, error)
  }
}