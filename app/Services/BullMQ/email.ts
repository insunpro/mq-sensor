import { Queue, Worker, Job, QueueScheduler } from "bullmq";
import EmailService from "@app/Services/Mail";
import IORedis from "ioredis";

const defaultConnection = {
  port: Number(process.env.REDIS_PORT) || 6379, // Redis port
  host: process.env.REDIS_HOST || "127.0.0.1", // Redis host
  password: process.env.REDIS_PASS,
  db: Number(process.env.REDIS_EMAIL) || 3,
  showFriendlyErrorStack: true,
}

export default class EmailQueue {
  private static connection;
  private static queue: Queue;
  private static worker: Worker;

  static start() {
    if (!this.queue) {
      this.connection = new IORedis(defaultConnection)
      this.connection.on("error", (error) => this.handleError(error, 'connection'))

      // this.scheduler = new QueueScheduler('email', {connection: this.connection})

      this.queue = new Queue('email', {connection: this.connection});
      this.queue.on("error", (error) => this.handleError(error, 'queue'));

      this.worker = new Worker('email', this.handleProcess, {connection: this.connection});
      this.worker.on("completed", this.handleComplete)
      this.worker.on("failed", this.handleFailed)
      this.worker.on("error", (error) => this.handleError(error, 'worker'))

      console.log("[EmailQueue] has started on", defaultConnection.host + ':' + defaultConnection.port + ':' + defaultConnection.db);
    }
  }

  // Add job
  static async add(message: string, subject: string, files: any[]) {
    return await this.queue.add("email", {message, subject, files}, { removeOnComplete: 100, removeOnFail: 100 })
  }

  // Handle job
  private static async handleProcess(job: Job) {
    let { message, files, subject } = job.data
    await EmailService.send(message, subject, files)
    return true
  }

  private static async handleComplete(job: Job, returnvalue: any) {
    // console.log('email.handleComplete', returnvalue)
  }

  private static async handleFailed(job: Job, failedReason: string) {
    console.error('email.handleFailed', job, failedReason)
  }

  private static async handleError(error, from="") {
    console.error('email.handleError', from, error)
  }
}