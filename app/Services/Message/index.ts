import SettingModel from "@app/Models/SettingModel";
import { ZaloQueue, EmailQueue, TelegramQueue } from "@app/Services/BullMQ";
import MessageModel from "@app/Models/MessageModel";
import moment from "moment";
import { Payload } from "../MQTT/Client";
import { alertLevels, deviceModes, sensorModes } from "@root/config/constant";

const DOMAIN = process.env.DOMAIN
const TIME_ZONE = "+07:00";

export default class Message {
  // static delay = 10; // thời gian giữa các tin nhắn

  static async getSetting() {
    let main = await SettingModel.query().findOne({key: "main"});
    let services = [], alertLevel = 0;
    if (main && main.value && main.value.services) services = main.value.services
    if (main && main.value && main.value.alertLevel) alertLevel = main.value.alertLevel
    return { services, alertLevel };
  }

  static async send(level, message, title="", files=[]) {
    let { services, alertLevel } = await this.getSetting();
    if (alertLevel === alertLevels.off || level === alertLevels.off) return;
    if (alertLevel === alertLevels.low && level !== alertLevels.high) return;
    if (alertLevel === alertLevels.medium && level === alertLevels.low) return;

    if (services.includes("zalo")) {
      let content = message
      if (files.length) {
        files.map(file =>{
          content += `\n${makeUrl(file)}`;
        })
      }
      await ZaloQueue.add("group", { message: content });
    }

    let mailSubject = title + " " + moment().utcOffset(TIME_ZONE).format("HH:mm:ss - DD/MM/YYYY")
    // let mailFiles = ["4-1628214799774.jpg"]
    if (services.includes("email")) await EmailQueue.add(message, mailSubject, files);

    if (services.includes("telegram")) await TelegramQueue.add(message, files)
    return true
  }

  // ############# DEVICE #############
  static async deviceOffline(device) {
    let message = await MessageModel.query().findOne({ key: "deviceOffline" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }

  static async deviceOnline(device) {
    let message = await MessageModel.query().findOne({ key: "deviceOnline" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }

  static async deviceDelete(device) {
    let message = await MessageModel.query().findOne({ key: "deviceDelete" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }

  static async deviceChangeMode(device, payload: Payload, mode) {
    let { value } = payload;
    let message = await MessageModel.query().findOne({ key: "deviceChangeMode" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["mode"] = deviceModes[mode].vi
      keys["status"] = value === 0 ? "Tắt" : "Bật"
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }

  // ############# NODES #############
  static async nodeJoin(device, payload: Payload) {
    let { id } = payload;
    let message = await MessageModel.query().findOne({ key: "nodeJoin" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      keys["id"] = id;
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }

  static async nodeOnline(device, payload: Payload) {
    let { id } = payload;
    let message = await MessageModel.query().findOne({ key: "nodeOnline" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      keys["id"] = id;
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }

  static async nodeOffline(device, payload: Payload) {
    let { id } = payload;
    let message = await MessageModel.query().findOne({ key: "nodeOffline" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      keys["id"] = id;
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }

  static async nodeBattery(device, topic: string, payload: Payload) {
    let { bat, id } = payload;
    if (bat > 10) return;
    let message = await MessageModel.query().findOne({ key: "nodeBattery" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      keys["percent"] = bat;
      keys["id"] = id;
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }

  static async nodeDelete(device, payload: Payload) {
    let { id } = payload;
    let message = await MessageModel.query().findOne({ key: "nodeDelete" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      keys["id"] = id;
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }

  static async warningTrepassing(device, topic: string, payload: Payload, files: string[] = []) {
    let { id, value } = payload;
    if (value !== 1) return;
    let message = await MessageModel.query().findOne({ key: "warningTrepassing" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      keys["id"] = id;
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name, files);
    }
  }

  static async warningBlocked(device, topic: string, payload: Payload, files: string[] = []) {
    let { id, value } = payload;
    if (value !== 2) return;
    let message = await MessageModel.query().findOne({ key: "warningBlocked" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      keys["id"] = id;
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name, files);
    }
  }

  static async nodeChangeMode(device, payload: Payload, mode) {
    let { id, value } = payload;
    let message = await MessageModel.query().findOne({ key: "nodeChangeMode" });
    if (message) {
      let keys = parseKeys(message.value.keywords, device);
      keys["mode"] = sensorModes[mode].vi
      keys["status"] = value === 0 ? "Tắt" : "Bật"
      keys["time"] = moment()
        .utcOffset(TIME_ZONE)
        .format("HH:mm:ss, DD/MM/YYYY");
      keys["id"] = id;
      let content = sprinf(message.value.content, keys);
      this.send(message.alertLevel, content, message.name);
    }
  }
}

export function sprinf(message, keys) {
  const regexp = /{{([^{]+)}}/g;
  let result = message.replace(regexp, function (ignore, key) {
    return (key = keys[key]) == null ? "" : key;
  });
  return result;
}

export function parseKeys(keys: string[], object: any = {}) {
  let result = {};
  keys.map((key) => {
    if (object[key]) {
      result[key] = object[key];
    }
  });
  return result;
}

export function checkCondition(val: any, con) {
  const comparators = {
    "=": (a, b) => a === b,
    ">": (a, b) => a > b,
    "<": (a, b) => a < b,
    ">=": (a, b) => a >= b,
    "<=": (a, b) => a <= b,
  };

  return comparators[con.operator](val, con.value)
}

export function makeUrl(file) {
  return DOMAIN + "/snapshot/" + file
}

interface Condition {
  field: string;
  operator: ">" | "=" | "<" | ">=" | "<="
}