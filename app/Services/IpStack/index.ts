import Env from "@core/Env";
import axios from "axios";

export default class IpStack {
  static baseUrl = "http://api.ipstack.com";
  static ipv4 = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
  static ignoreIps = /(^127\.)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^192\.168\.)/ //local ip
  static attempts = 0;
  static apiKeys = [
    "d7db74275f9d669d544a969391fa3810", //t.nguyentuananh123@gmail.com
    "0b7eba492666e7cb669958cfd8ad86bd", //t.nguyentuananh124@gmail.com  MK : @@123456
    "9f5b55e488400424c7c545e657327fb3", //t.nguyentuananh125@gmail.com  MK : @@123456
    "96e7c6624b13b4ca19dd631eb7db6285", //t.nguyentuananh126@gmail.com  MK : @@123456
    "920ca0850b0f037f1a718aacd6b415d3", //t.nguyentuananh1@gmail.com  MK : @@123456
    "2c737bf7c1916463e96a97f9e238767c", //nobita2111@gmail.com  MK : @@123456
    "326d486c29ac0850690f83092eef49e2", //Test05011989@gmail.com  MK : @@123456
    "b689838bb547ebe27caf7ab7a7ee86e9", //nguyen.tuan.anh.2@mqsolutions.com.vn  MK : @@123456
    "ee3c27588c787fb917b30ddfde824cf2", //le.tien.cong@mqsolutions.com.vn  MK : @@123456
    "b60f20c4a299e18b2f9b366428066d84", //letiencong96@gmail.com  MK : @@123456
  ]

  static async getInfo(ip: string) {
    if (this.ignoreIps.test(ip) || !this.ipv4.test(ip)) {
      return {}
    }

    let url = this.baseUrl + '/' + ip + '?access_key='+this._getKey()+'&format=1'
    let result = await axios.get(url)

    return result.data

  }

  static _getKey() {
    let random = Math.floor(Math.random() * this.apiKeys.length)
    return this.apiKeys[random];
  }
}
