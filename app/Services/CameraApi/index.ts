import digestRequest from "./request";
import CameraModel from "@root/app/Models/CameraModel";

interface RequestOptions {
    hostname: string,
    port: number,
    path: string,
    method: 'GET' | 'POST',
    headers: any
}

export default class CameraApi {
    private type: "digest" | "basic" = "digest";
    private options: RequestOptions = {
        hostname: '127.0.0.1',
        port: 80,
        path: '/cgi-bin/global.login?userName=admin',
        method: 'GET',
        headers: {
            'Connection': 'Keep-Alive',
            'Content-Type': 'text/plain',
            'Host': '127.0.0.1'
        }
    }
    private auth = {
        user: 'admin',
        pass: ''
    }

    constructor(camera: CameraModel) {
        this.options.hostname = camera.host;
        this.options.port = camera.port;
        this.options.headers.Host = camera.host;
        this.auth.user = camera.username;
        this.auth.pass = camera.password;
    }

    async getConfig(name: string) {
        const path = '/cgi-bin/configManager.cgi?action=getConfig&name=' + name;
        const options = {...this.options, path }
        return this.handleRequest(options);
    }

    async snapshot() {
        const path = '/cgi-bin/snapshot.cgi';
        const options = {...this.options, path, encode: 'binary' }
        return this.handleRequest(options)
    }

    async ptz({action='start', channel=0, code='PositionABS', arg1=10, arg2=20, arg3=30}) {
        let path;
        if (action === 'getStatus') {
            path = '/cgi-bin/ptz.cgi?action=getStatus'
        }
        else {
            path = `/cgi-bin/ptz.cgi?action=${action}&channel=${channel}&code=${code}&arg1=${arg1}&arg2=${arg2}&arg3=${arg3}`
        }
        const options = {...this.options, path };
        return this.handleRequest(options)
    }

    private async handleRequest(options) {
        return new Promise((resolve, reject) => {
            digestRequest(options, null, this.auth.user, this.auth.pass, (err, data) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(data)
                }
            })
        })
    }
}