import CameraModel from "@root/app/Models/CameraModel";
import { Server } from "socket.io";
import Onvif from "../Onvif";

export default class SocketIo {
  static io;
  static rooms: any = {};
  static devices: any = {};

  static start(expressServer) {
    this.io = new Server(expressServer, {
      cors: {
        credentials: false,
      },
    });
    console.log("[SocketIO] has been starterd");
    this.io.on("connection", (socket) => this.handleSocket(socket));
    this.handleSocket(this.io);
  }

  private static handleSocket(socket) {
    socket.on("getStream", (data) => {
      this.handleGetStream(socket, data);
    });

    socket.on("ptzMove", (data) => {
      this.handlePtzMove(data);
    });

    socket.on("ptzStop", (data) => {
      this.handlePtzStop(data);
    });

    socket.on("ptzGotoHome", (data) => {
      this.handlePtzGotoHome(data);
    });

    socket.on("disconnecting", (reason) => {
      this.handleDisconnect(socket, reason);
    });
  }

  private static async handleGetStream(socket, data) {
    let { cid } = data;
    let roomName = "cid-" + cid;
    socket.join(roomName);
    if (!this.rooms[roomName]) {
      if (this.devices[roomName]) delete this.devices[roomName];
      let camera = await CameraModel.query().findById(cid);
      console.log("cid", cid, camera.name);
      if (camera) {
        this.devices[roomName] = await CameraModel.onvifDevice(camera);
        this.rooms[roomName] = true;
        this.fetchRoom(roomName)
      }
    }
  }

  private static async fetchRoom(roomName) {
    while (this.rooms[roomName] && this.devices[roomName]) {
      let snapshot = await CameraModel.fetchSnapShot(this.devices[roomName])
      this.io.to(roomName).emit("cameraStream", snapshot); 
    }
  }

  private static async handlePtzMove(data) {
    let { cid, direction, speed } = data;
    console.log("handlePtzMove", cid, direction);
    let roomName = "cid-" + cid;
    if (!this.devices[roomName])
      return console.error("handlePtzMove.error: ", "no device for room");
    let x = 0,
      y = 0,
      z = 0;
    switch (direction) {
      case "up":
        y = speed;
        break;
      case "down":
        y = 0 - speed;
        break;
      case "left":
        x = 0 - speed;
        break;
      case "right":
        x = speed;
        break;
      case "in":
        z = speed;
        break;
      case "out":
        z = -speed;
        break;
    }
    this.devices[roomName].ptzMove(
      {
        speed: { x, y, z },
        timeout: 1,
      },
      (error) => (error ? console.error("handlePtzMove.error", error) : "")
    );
  }

  private static async handlePtzStop(data) {
    let { cid } = data;
    let roomName = "cid-" + cid;
    if (!this.devices[roomName])
      return console.error("handlePtzStop.error: ", "no device for room");
    this.devices[roomName].ptzStop((error) =>
      error ? console.error("handlePtzStop.error", error) : ""
    );
  }

  private static async handlePtzGotoHome(data) {
    let { cid } = data;
    console.log("handlePtzGotoHome", cid);
    let roomName = "cid-" + cid;
    if (!this.devices[roomName])
      return console.error("handlePtzMove.error: ", "no device for room");
    let profile = this.devices[roomName].getCurrentProfile();
    let { token } = profile;
    if (!token) return console.error("handlePtzMove.error: ", "no token");
    this.devices[roomName].services.ptz.gotoHomePosition(
      { ProfileToken: token, speed: 1 },
      (error) => console.error("handlePtzGotoHome.error", error)
    );
  }

  private static handleDisconnect(socket, reason) {
    console.log("disconnecting", socket.rooms, reason);
    if (socket.rooms && socket.rooms.size > 1) {
      socket.rooms.forEach((room) => {
        if (room === socket.id) return;
        let roomSize = this.io.sockets.adapter.rooms.get(room).size;
        console.log({ room, roomSize });
        if (roomSize <= 1 && this.rooms[room]) {
          this.rooms[room] = false
          delete this.devices[room];
        }
      });
    }
  }

  private static getDevice(cid) {}
}
