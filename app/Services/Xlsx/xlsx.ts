
import Xlsx from 'xlsx';
import ExcelJS from 'exceljs';
import fs from 'fs';
import path from 'path';

const rootPath = process.cwd();
const directory = process.env.DIRECTORY || `/public/static/download`;

class ExcelService {

    constructor() {

    }

    static readFile(uri: any, ignoreRows: number = 1): any[] {
        if (!fs.existsSync(uri)) return [];
        let workbook = Xlsx.readFile(uri, { sheetRows: 1000 });
        let sheetNames = workbook.SheetNames;
        let records = [];
        for (let sheetName of sheetNames) {
            let data = Xlsx.utils.sheet_to_json(workbook.Sheets[sheetName], {
                header: 1,
                defval: '',
                blankrows: true
            });
            if (ignoreRows) data = data.slice(ignoreRows);
            records.push(...data);
        }

        return records;
    }

    static readFistSheet(uri: any, ignoreRows: number = 1): any[] {
        if (!fs.existsSync(uri)) return [];
        let workbook = Xlsx.readFile(uri);
        let sheetNames = workbook.SheetNames;

        let data = Xlsx.utils.sheet_to_json(workbook.Sheets[sheetNames[0]], {
            header: 1,
            defval: '',
            blankrows: true
        });
        if (ignoreRows) data = data.slice(ignoreRows);

        return data;

    }

    static async writeArrayToFile(data: any[][], fileName: string, fillColor: boolean = false) {
        let exportPath = path.join(rootPath, directory, fileName);
        const worksheet = Xlsx.utils.aoa_to_sheet(data);
        const workbook = Xlsx.utils.book_new();
        
        Xlsx.utils.book_append_sheet(workbook, worksheet, "Sheet 0");
        Xlsx.writeFile(workbook, exportPath);
        return exportPath;
    }

    static async fillColor(uri: string, worksheetColumns: any[]) {
        console.log(">>>>> Auto fill color with cell start with '@'  >>>>>>>>>>");
        const workbook = new ExcelJS.Workbook();
        await workbook.xlsx.readFile(uri);

        await workbook.eachSheet((worksheet, sheetId) => {
            if (worksheetColumns) worksheet.columns = worksheetColumns;
            
            worksheet.eachRow( (row, rowNumber) => {
                row.eachCell( (cell, colNumber) => {
                    let text: string = cell.text;
                    if (/^@/.test(text)) {
                        row.getCell(colNumber).style = {
                            fill: {
                                type: 'pattern',
                                pattern: 'darkVertical',
                                bgColor: {argb: 'FFFFFF00'}
                            },
                        }
                        row.getCell(colNumber).value = text.replace("@", "");
                    }
                })
            });
        });
        return await workbook.xlsx.writeFile(uri);
    }
}

export default ExcelService;