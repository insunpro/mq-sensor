import net from "net";
import MQTTClient from "./Client";
import mqttCon from "mqtt-connection";
import Logger from "@core/Logger";
import moment from "moment";

const logger = Logger('mqtt');
class MQTT {
  static server;
  static clients: MQTTClient[] = [];

  static start() {
    if (process.env.MODE == "dev-client") return false;

    MQTT.server = new net.Server();
    MQTT.server.on("connection", (stream) => {
      let client = new MQTTClient(stream);
        this.clients.push(client)
        console.log("connection->this.clients: ", this.clients.length)
    });

    MQTT.server.on("error", function (error) {
      console.log("connection error expected", error)
    })

    MQTT.server.on("close", function (reason) {
      console.log("connection close expected", reason)
    })

    console.log("[MQTT] server has started on port:", 1883);
    MQTT.server.listen(1883);
  }
  
  static remove(clientId) {
    let index = this.clients.findIndex(value => value.clientId === clientId);
    if (index !== -1) {
      this.clients.splice(index, 1)
    }
    console.log("connection->remove: ", this.clients.length)
  }

  static getExistClients(serialNumber: string) {
    return this.clients.filter(client => client.username === serialNumber)
  }

  static getClient(serialNumber: string) {
    return this.clients.find(client => client.username === serialNumber)
  }

  static info({ip, clientId, event, data}: {ip: string, clientId: string, event: string, data: any}) {
    logger.info(JSON.stringify({time: moment(), ip, clientId, event, data}))
  }

  static error({ip, clientId, error}: {ip: string, clientId: string, error: any}) {
    logger.error(JSON.stringify({time: moment(), ip, clientId, error}))
  }
}

export default MQTT;