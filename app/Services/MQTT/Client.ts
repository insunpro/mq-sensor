import mqttCon from "mqtt-connection";
import MQTT from "./index";
import DeviceModel from "@root/app/Models/DeviceModel";
import DeviceLogModel from "@root/app/Models/DeviceLogModel";

import Env from "@core/Env";
import moment from "moment";
import MessageService from "@app/Services/Message"
import SensorModel from "@root/app/Models/SensorModel";
import _ from "lodash";
import SnapshotModel from "@root/app/Models/SnapshotModel";

const MQTT_SECRET = Env.get("MQTT_SECRET", "123456")
export default class MQTTClient {
  socket=null;
  status=null;
  ip=null;
  clientId;
  device: DeviceModel | null;
  username=null;
  waitingForResponses: any[] = [];

  constructor(stream) {
    this.socket = new mqttCon(stream);
    this.ip = stream.remoteAddress
    this.initStream(stream);
    this.initClient();
  }

  initStream(stream) {
    // timeout idle streams after 5 minutes
    stream.setTimeout(1000 * 60 * 5);
    // stream timeout
    stream.on("timeout", () => {
      this.destroy("stream.timeout");
      console.log("timeout", this.clientId);
      MQTT.error({ip: this.ip, clientId: this.clientId, error: "connection timeout"})
    });
  }

  initClient() {
    // client connected
    this.socket.on("connect", async (packet) => {
      let {clientId, username, password, protocolId, protocolVersion} = packet;

      // verify protocol here
      if (!protocolId || protocolVersion < 3) {
        console.log("Client.verify.error.1 ", "protocol")
        this.socket.connack({ returnCode: 1})
        this.status=1
      }

      // verify clientId here
      if (!clientId) {
        console.log("Client.verify.error.2", "clientId")
        this.socket.connack({ returnCode: 2 })
        this.status=2
        // this.destroy("connack.2")
        return;
      }

      // verify username/password
      if (!username || !password || typeof username !== "string") {
        console.log("Client.verify.error.4", "username/password")
        this.socket.connack({ returnCode: 4})
        this.status=4
      }

      if (Buffer.isBuffer(password)) password = password.toString();
      if (password !== MQTT_SECRET) {
        console.log("Client.verify.error.5", "password")
        this.socket.connack({ returnCode: 5 })
        this.status=5
        // this.destroy("connack.5")
        return;
      }

      let existClients = MQTT.getExistClients(username)
      if (existClients.length) {
        console.log("existClients", existClients.length)
        existClients.forEach(existClient => existClient.disconnect())
      }

      this.clientId = clientId;
      this.username = username;
      this.device = await DeviceModel.query().findOne({serialNumber: username});
      if (!this.device) {
        this.device = await DeviceModel.query().insert({serialNumber: username});
      }
      let time = moment();
      await this.device.$query().patch({status: 1, updated_at: time});
      MessageService.deviceOnline({...this.device})
      this.socket.connack({ returnCode: 0 });
      MQTT.info({ip: this.ip, clientId: this.clientId, event: "connect", data: packet})
    });

    // client published
    this.socket.on("publish", async (packet) => {
      // send a puback with messageId (for QoS > 0)
      let { topic, payload } = packet;
      if (Buffer.isBuffer(payload)) payload = payload.toString();
      if (this.device) {
        await DeviceLogModel.query().insert({
          device_id: this.device.id,
          topic: JSON.stringify(topic),
          payload: JSON.stringify(payload)
        })
        this.handleTopic(topic, parseData(payload))
      }

      if (packet.qos > 0) {
        this.socket.puback({ messageId: packet.messageId });
      }
      MQTT.info({ip: this.ip, clientId: this.clientId, event: "publish", data: packet})
    });

    // client pinged
    this.socket.on("pingreq", async () => {
      // send a pingresp
      if (this.device) {
        await this.device.$query().patch({status: 1, updated_at: moment()})
      }
      this.socket.pingresp();
      MQTT.info({ip: this.ip, clientId: this.clientId, event: "pingreq", data: null})

    });

    // client subscribed
    this.socket.on("subscribe", (packet) => {
      // send a suback with messageId and granted QoS level
      this.socket.suback({
        granted: [packet.qos],
        messageId: packet.messageId,
      });
      MQTT.info({ip: this.ip, clientId: this.clientId, event: "subscribe", data: packet})
    });

    // connection error handling
    this.socket.on("close", async () => {
      if (this.device) {
        MessageService.deviceOffline({...this.device})
        await this.device.$query().patch({status: 0, updated_at: moment()});
        await SensorModel.deviceOffline(this.device.id, moment())
      }
      this.destroy("close");
      MQTT.info({ip: this.ip, clientId: this.clientId, event: "subscribe", data: null})
    });

    this.socket.on("error", (err) => {
      // console.log("error", err);
      this.destroy("error");
      MQTT.error({ip: this.ip, clientId: this.clientId, error: err})
    });

    this.socket.on("disconnect", (reason) => {
      console.log("disconnect", reason);
      this.disconnect("disconnect");
      MQTT.info({ip: this.ip, clientId: this.clientId, event: "disconnect", data: reason})
    });
  }

  async publish(topic: string, payload: Payload, waitForResponse: boolean): Promise<[any, any]> {
    if (this.socket) {
      try {
        this.socket.publish({qos: 0, topic, payload: JSON.stringify(payload)})
        if (waitForResponse) {
          this.waitingForResponses.push({topic, payload});
        }
      }
      catch (err) {
        console.log("publish.err", err)
        return [err, null]
      }
      return [null, true]
    }
    return [null, true]
  }

  async checkResponse(topic: string, payload: Payload, timeout: number, hub:boolean = false) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let index;
        if (hub) {
          index = this.waitingForResponses.findIndex(value => value.topic === topic);
        }
        else {
          index = this.waitingForResponses.findIndex(value => value.topic === topic && value.payload.id === payload.id);
        }

        if (index !== -1) {
          this.waitingForResponses.splice(index, 1)
          resolve(false);
        }
        else resolve(true);
      }, timeout * 1000)
    })
  }
  
  private async cleanWaitingResponse(topic: string, payload: Payload) {
    let topicArr = topic.split("/");
    let cmd = Number(topicArr[2]);

    if (cmd === 6) {
      let waitings = this.waitingForResponses.filter(value => value.topic === topic);
      waitings.map(waiting => {
        let index = this.waitingForResponses.findIndex(value => value.topic === waiting.topic);
        if (index !== -1) {
          this.waitingForResponses.splice(index, 1)
        }
      })
      return;
    }
    
    let waitings = this.waitingForResponses.filter(value => value.topic === topic && value.payload.id === payload.id);
    waitings.map(waiting => {
      let index = this.waitingForResponses.findIndex(value => value.topic === waiting.topic && value.payload.id === waiting.payload.id);
      if (index !== -1) {
        this.waitingForResponses.splice(index, 1)
      }
    })
  }

  async destroy(event='') {
    if (this.socket) {
      this.socket.destroy();
      this.socket=null;
      MQTT.remove(this.clientId)
    }
  }

  async disconnect(reason='') {
    if (this.socket) {
      this.socket.disconnect();
      // MQTT.remove(this.clientId);
    }
  }

  async handleTopic(topic, payload) {
    let topicArr = topic.split("/");
    let cmd = Number(topicArr[2]);
    let { id, type, bat, value } = parseData(payload)
    
    this.cleanWaitingResponse(topic, parseData(payload))

    switch (cmd) {
      case 1: // Thiết bị cảm biến mới hòa mạng
        MessageService.nodeJoin({...this.device}, parseData(payload))
        SensorModel.newNode(this.device.id, payload, moment())
        break;
      case 2: // Thiết bị cảm biến gửi dữ liệu về
        if (value === 1 || value === 2) {
          let snaps = await SensorModel.snap(this.device.id, payload)
          let files = await SnapshotModel.save(snaps);
          MessageService.warningBlocked({...this.device}, topic, parseData(payload), files)
          MessageService.warningTrepassing({...this.device}, topic, parseData(payload), files)
        }
        MessageService.nodeBattery({...this.device}, topic, parseData(payload))
        SensorModel.newNode(this.device.id, payload, moment())
        SensorModel.sendData(this.device.id, payload, moment())
        break;
      case 3: // Xóa 1 thiết bị cảm biến
        MessageService.nodeDelete({...this.device}, parseData(payload))
        SensorModel.deleteNode(this.device.id, payload)
        break;
      case 4: // Cập nhật trạng thái on / off của thiết bị cảm biến
        if (payload.value == 0) {
          MessageService.nodeOffline({...this.device}, parseData(payload))
        }
        if (payload.value == 1) {
          MessageService.nodeOnline({...this.device}, parseData(payload))
        }
        SensorModel.newNode(this.device.id, payload, moment())
        SensorModel.nodeStatus(this.device.id, payload, moment())
        break;
      case 5: // Xóa tất cả thiết bị cảm biến
        MessageService.deviceDelete({...this.device})
        SensorModel.deleteAllNode(this.device.id, payload)
        break;
      case 6: // Tắt bật hòa mạng
        MessageService.deviceChangeMode({...this.device}, payload, "config")
        DeviceModel.changeMode(this.device.id, payload, moment(), "config")
        break;
      case 7: // Tắt bật cảnh báo của thiết bị cảm biến
        MessageService.nodeChangeMode({...this.device}, payload, "alert")
        SensorModel.changeMode(this.device.id, payload, moment(), "alert")
        break;
      default:
        console.error("unknown cmd", cmd)
    }
    
  }
}

export function parseData(str: string) {
  try {
    JSON.parse(str);
  }
  catch (e) {
    return str
  }
  return JSON.parse(str);
}

export interface Payload {
  id: number,
  type: number,
  bat: number,
  value: number
}