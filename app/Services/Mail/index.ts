import nodemailer from 'nodemailer'
import SettingModel from "@app/Models/SettingModel";
import _ from 'lodash';
import fs from 'fs';
import path from 'path';

// const transporter = nodemailer.createTransport({
//   host: process.env.MAIL_HOST || "smtp.gmail.com",
//   port: process.env.MAIL_PORT || 587,
//   secure: process.env.MAIL_SECURE == "1" ? true : false, // upgrade later with STARTTLS
//   auth: {
//     user: process.env.MAIL_USER,
//     pass: process.env.MAIL_PASS
//   }
// });

class EmailService {
  static getTransporter(user, pass, host="smtp.gmail.com", port=587, secure=false) {
    let transporter = nodemailer.createTransport({
      host: host,
      port: port,
      secure: secure, // upgrade later with STARTTLS
      auth: {
        user: user,
        pass: pass
      }
    });
    return transporter
  }
  static async getSetting() {
    let result = await SettingModel.query().findOne({key: "email"});
    return result ? result.value || {} : {};
  }

  static async send(message, subject, files) {
    let setting = await this.getSetting();
    if (_.isEmpty(setting)) {
      console.error("EmailService.send.noSetting")
      return;
    }
    if (!message) {
      console.error("EmailService.send.noMessage")
      return;
    }

    let mailer = this.getTransporter(setting.email, setting.password)

    let attachs = this.getAttachments(files)

    let mailOptions = {
      from: {
        name: setting.name,
        address: setting.email
      },
      to: this.getEmails(setting.toEmails),
      subject: subject,
      html: this.addEmbeddedImages(message, attachs),
      attachments: attachs,
    };

    try {
      mailer.sendMail(mailOptions)
    }
    catch (err) {
      throw err
    }
    // mailer.sendMail(mailOptions, function (error, info) {
    //   if (error) {
    //     console.error(error);
    //   } else {
    //     console.log('Email sent: ' + info.response);
    //   }
    // });
    await new Promise(resolve => setTimeout(resolve, setting.delay * 1000));
  }

  static getEmails(str: string) {
    let txt = str.trim();
    let arr = txt.split("\n");
    arr.forEach((value, index) => arr[index] = value.trim());
    return arr
  }

  static getAttachments(files: string[]) {
    let attachs = [];
    files.map(file => {
      let filePath = path.join(path.resolve('public/snapshot'), file);
      if (fs.existsSync(filePath)) {
        attachs.push({
          filename: file,
          path: filePath,
          cid: file
        })
      }
    })
    return attachs
  }

  static addEmbeddedImages(message: string, attachs: any[]) {
    let embbed = `<p>${message}</p>`;
    attachs.map(attach => {
      embbed += `<br/><img width="600" src="cid:${attach.cid}" />`
    })

    return embbed
  }

}

export default EmailService
