import SettingModel from "@root/app/Models/SettingModel";
import _ from "lodash";
import TelegramBot from "node-telegram-bot-api";
import path from "path";
import fs from "fs";
const TELEGRAM_POLLING = process.env.TELEGRAM_POLLING == '1' ? true : false

export default class TelegramService {
    static bot: TelegramBot;
    static token;
    static polling=true;

    static async init() {
        let setting = await this.getSetting();
        if (_.isEmpty(setting)) {
            throw new Error("TelegramService.error: noSetting")
            console.error("TelegramService.error: noSetting")
        } 
        else {
            this.bot = new TelegramBot(setting.token, {polling: TELEGRAM_POLLING});
            this.token = setting.token;
            // Matches "/echo [whatever]"
            this.bot.onText(/\/echo (.+)/, (msg, match) => {
                // 'msg' is the received Message from Telegram
                // 'match' is the result of executing the regexp above on the text content
                // of the message
            
                const chatId = msg.chat.id;
                const resp = match[1]; // the captured "whatever"
            
                // send back the matched "whatever" to the chat
                this.bot.sendMessage(chatId, resp==="id" ? "" + chatId : resp);
            });
            this.bot.on("error", (error) => {
                throw error
                console.error("TelegramService.bot.error", error)
            })
        }
    }

    static async getSetting() {
        let result = await SettingModel.query().findOne({key: "telegram"});
        return result ? result.value || {} : {};
    }

    static async send(message, files) {
        let setting = await this.getSetting();
        if (_.isEmpty(setting)) {
            console.error("TelegramService.send.noSetting")
            return;
        }
        if (!message) {
            console.error("TelegramService.send.noMessage")
            return;
        }
        if (this.token !== setting.token) {
            await this.init()
        }
        if (!this.bot) {
            console.error("TelegramService.send.noBot")
            return;
        }

        let ids = this.getIds(setting.toIds);
        let attachs = this.getAttachments(files);
        try {
            ids.map(id => {
                this.bot.sendMessage(id, message)
                for(let attach of attachs) {
                    this.bot.sendPhoto(id, attach.data)
                }
            })
        }
        catch (err) {
            throw err
        }

        await new Promise(resolve => setTimeout(resolve, setting.delay * 1000));
    }

    static getIds(str: string) {
        let txt = str.trim();
        let arr = txt.split("\n");
        arr.forEach((value, index) => arr[index] = value.trim());
        return arr
    }

    static getAttachments(files: string[]) {
        let attachs = [];
        files.map(file => {
            let filePath = path.join(path.resolve('public/snapshot'), file);
            if (fs.existsSync(filePath)) {
                // const buffer = fs.readFileSync(filePath)
                const stream = fs.createReadStream(filePath)
                attachs.push({
                    filename: file,
                    // data: buffer,
                    data: stream,
                    filePath,
                    contentType: "img/jpeg"
                })
            }
        })
        return attachs
    }
}