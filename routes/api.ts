import Route from '@core/Routes'
const ExtendMiddleware = require("@app/Middlewares/ExtendMiddleware");
const  nhfinderRoutes = require('@ngochipx/nhfinder/nodejs/routes')({
  ROOTDIR: "/", //The absolute path to the root directory.
  UPLOAD_PATH: "" + "/public/uploads/", //The absolute path to the upload folder.
  PUBLIC_PATH: "/uploads", //public path. For example, http: // localhost: 3000 / uploads / then enter: / uploads
  DOMAIN: "http://localhost:3000", //Domain used for image links.
  ALLOW_EXTENSIONS: [".jpg", ".jpeg", ".png", ".bmp", ".gif", ".txt"], //Extensions are allowed to uploads.
  API_KEY: "123456", //API Key uses security between client and server.
})
const AuthApiMiddleware = require('@app/Middlewares/AuthApiMiddleware');
//const { permission, permissionResource, permissionMethod } = require('@app/Middlewares/PermissionMiddleware');

Route.group(() => {
  // ---------------------------------- Auth Routes ---------------------------------------//
  Route.post("/login", "AuthController.login").name('auth.login')
  Route.post("/forgotPassword", "AuthController.forgotPassword").name('auth.forgotPassword')
  Route.post("/resetPassword", "AuthController.resetPassword").name('auth.resetPassword')

  // ---------------------------------- End Auth Routes -----------------------------------//

  // ---------------------------------- Route Routes ---------------------------------------//
  Route.get("/routes", "RouteController.index").name('routes.index')
  // ---------------------------------- End Route Routes -----------------------------------//

  Route.group(() => {
    Route.post("/changePassword", "AuthController.changePassword").name("auth.changePassword")
    Route.post("/logout", "AuthController.logout").name('auth.logout')

    // ---------------------------------- User Routes ---------------------------------------//
    Route.get("/roles/select2", "RoleController.select2").name('roles.select2')
    Route.resource("/roles", "RoleController").name('roles')

    // ---------------------------------- End User Routes -----------------------------------//

    // ---------------------------------- User Routes ---------------------------------------//
    Route.resource("/users", "UserController").name('users')
    // ---------------------------------- End User Routes -----------------------------------//

    // ---------------------------------- Area Routes ---------------------------------------//
    Route.get("/areas/province", "AreaController.province").name('areas.province')
    Route.get("/areas/district", "AreaController.district").name('areas.district')
    Route.get("/areas/ward", "AreaController.ward").name('areas.ward')
    Route.get("/areas/province/2", "AreaController.select2province").name('areas.province.select2')
    Route.get("/areas/district/2", "AreaController.select2district").name('areas.district.select2')
    Route.get("/areas/ward/2", "AreaController.select2ward").name('areas.ward.select2')
    // ---------------------------------- End Area Routes -----------------------------------//

    // ---------------------------------- Group Routes ---------------------------------------//
    Route.get("/groups/select2", "GroupController.select2").name('groups.select2')
    Route.resource("/groups", "GroupController").name('groups')
    // ---------------------------------- End Group Routes -----------------------------------//

    // ---------------------------------- Device Routes ---------------------------------------//
    Route.put("/devices/mode", "DeviceController.changeMode").name('devices.changeMode')
    Route.get("/devices/clients", "DeviceController.getClients").name('devices.getClients')
    Route.resource("/devices", "DeviceController").name('devices')
    // ---------------------------------- End Device Routes -----------------------------------//

    // ---------------------------------- Device Log Routes ---------------------------------------//
    Route.resource("/device-logs", "DeviceLogController").name('deviceLogs')
    // ---------------------------------- End Device Log Routes -----------------------------------//

    // ---------------------------------- Sensor Routes ---------------------------------------//
    Route.get("/sensors/byDevice", "SensorController.getByDevice").name('sensors.getByDevice')
    Route.get("/sensors/:id/camera/:cid", "SensorController.getCamera").name('sensors.getCamera')
    Route.get("/sensors/:id/camera/", "SensorController.indexCamera").name('sensors.indexCamera')
    Route.delete("/sensors/:id/camera", "SensorController.removeCamera").name('sensors.removeCamera')
    Route.post("/sensors/:id/camera", "SensorController.updateCamera").name('sensors.updateCamera')
    Route.get("/sensors/:id/select2camera", "SensorController.select2camera").name('sensors.select2camera')
    Route.put("/sensors/mode", "SensorController.changeMode").name('sensors.changeMode')
    Route.delete("/sensors/all", "SensorController.deleteAll").name('sensors.deleteAll')
    Route.resource("/sensors", "SensorController").name('sensors')
    // ---------------------------------- End Sensor Routes -----------------------------------//

    // ---------------------------------- Message Routes ---------------------------------------//
    Route.resource("/messages", "MessageController").name('messages')
    // ---------------------------------- End Message Routes -----------------------------------//

    // ---------------------------------- Message Routes ---------------------------------------//
    Route.get("/settings/main", "SettingController.getMain").name('settings.getMain')
    Route.put("/settings/main", "SettingController.updateMain").name('settings.updateMain')
    Route.get("/settings/email", "SettingController.getEmail").name('settings.getEmail')
    Route.put("/settings/email", "SettingController.updateEmail").name('settings.updateEmail')
    Route.get("/settings/zalo", "SettingController.getZalo").name('settings.getZalo')
    Route.put("/settings/zalo", "SettingController.updateZalo").name('settings.updateZalo')
    Route.get("/settings/telegram", "SettingController.getTelegram").name('settings.getTelegram')
    Route.put("/settings/telegram", "SettingController.updateTelegram").name('settings.updateTelegram')
    
    // ---------------------------------- End Message Routes -----------------------------------//

    // ---------------------------------- Device Routes ---------------------------------------//
    Route.delete("/cameras/preset", "CameraController.removePreset").name('cameras.removePreset')
    Route.post("/cameras/preset", "CameraController.savePreset").name('cameras.savePreset')
    Route.post("/cameras/save", "CameraController.onvifSave").name('cameras.onvifSave')
    Route.post("/cameras/connect", "CameraController.onvifConnect").name('cameras.onvifConnect')
    Route.get("/cameras/search", "CameraController.onvifSearch").name('cameras.onvifSearch')
    Route.get("/cameras/:id/lastSnap", "CameraController.lastSnap").name('cameras.lastSnap')
    Route.get("/cameras/:id/snapshot", "CameraController.snapshot").name('cameras.snapshot')
    Route.post("/cameras/ptz", "CameraController.ptz").name('cameras.ptz')
    Route.get("/cameras/select2", "CameraController.select2").name('cameras.select2')
    Route.post("/cameras/sync", "CameraController.sync").name('cameras.sync')
    Route.resource("/cameras", "CameraController").name('cameras')
    // ---------------------------------- End Device Routes -----------------------------------//

    // ---------------------------------- Snapshot Routes ---------------------------------------//
    Route.resource("/snapshots", "SnapshotController").name('snapshots')
    // ---------------------------------- End Snapshot Routes -----------------------------------//
  }).middleware([AuthApiMiddleware]);

}).middleware([ExtendMiddleware]).name('api').prefix("/api/v1")
Route.use('/nhfinder', nhfinderRoutes)
