import Route from '@core/Routes'
/**
 * Route:
 * Function:
 *    Method: get, post, put, delete, resource
 *    Route.<method>(path, Action).middleware([middleware1, middleware2])
 *    Ex: Route.get("/user", "UserController.index").middleware([auth])
 *    Route.resource("/user", "UserController")
 *
 *    Route.group(() =>{...}).prefix(path).middleware([middleware1, middleware2])
 *    Ex: Route.group(() =>{
 *        Route.get("/user", "UserController.index")
 *        Route.group("/user", "UserController.index")
 *        require("./setting") //load all router in ./setting.js
 *    }).prefix("/api/v1").middleware([auth])
 */
require("./api")
require("./admin")
// require("./user")

//Route.get("/", "pages/home").name("frontend.home")
Route.router.get("/", function(req, res, next){
  res.redirect(301, "/login")
})
Route.get("/health", function ({ request, response, next }) {
  response.status(200).send("OK");
})

Route.get("/login", "pages/common/login").name("frontend.login")
Route.get("/forgot-password", "pages/common/forgot").name("frontend.forgotPassword")
Route.get("/reset-password/:token", "pages/common/reset").name("frontend.resetPassword")

Route.router.use("/", require("express").static('@root/../public'));