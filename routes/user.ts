import Route from '@core/Routes'
const AuthUserMiddleware = require('@app/Middlewares/AuthUserMiddleware')

Route.group(() => {
  Route.get("/", "pages/user/dashboard").name("dashboard.index").sidebar('dashboard.index')
  {
    let name = 'dashboard'
    Route.get(`/${name}`, `pages/user/${name}`).name(`${name}`).sidebar(`${name}.index`)
  }
  {
    let name = 'camera'
    Route.get(`/${name}`, `pages/user/${name}`).name(`${name}.index`).sidebar(`${name}.index`)
    Route.get(`/${name}/create`, `pages/user/${name}/create`).name(`${name}.create`).parent(`${name}.index`).sidebar(`${name}.index`)
    Route.get(`/${name}/:id/edit`, `pages/user/${name}/edit`).name(`${name}.edit`).parent(`${name}.index`).sidebar(`${name}.index`)
  }
  {
    let name = 'monitor'
    Route.get(`/${name}`, `pages/user/${name}`).name(`${name}.index`).sidebar(`${name}.index`)
    Route.get(`/${name}/create`, `pages/user/${name}/create`).name(`${name}.create`).parent(`${name}.index`).sidebar(`${name}.index`)
    Route.get(`/${name}/:id/edit`, `pages/user/${name}/edit`).name(`${name}.edit`).parent(`${name}.index`).sidebar(`${name}.index`)
  }
  {
    let name = 'video'
    Route.get(`/${name}`, `pages/user/${name}`).name(`${name}.index`).sidebar(`${name}.index`)
    Route.get(`/${name}/:id/edit`, `pages/user/${name}/edit`).name(`${name}.edit`).parent(`${name}.index`).sidebar(`${name}.index`)
  }
}).name("frontend.user").prefix("/user").middleware([AuthUserMiddleware])
