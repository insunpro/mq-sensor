if (!process.env.IS_TS_NODE) {
  // tslint:disable-next-line:no-var-requires
  require('module-alias/register');
}
import Server from '@core/Server'

(async () => {
  try {
    let server = new Server();
    let result = await server.start()
    const MQTTService = require('@app/Services/MQTT').default;
    MQTTService.start();
    const BullMQ = require('@app/Services/BullMQ').default;
    BullMQ.start();
    const SocketIO = require("@app/Services/SocketIO").default;
    SocketIO.start(result.server)
    //require('@app/Cronjob')
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();
