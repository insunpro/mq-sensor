
init:
	mkdir -p .docker/db/pg
	mkdir -p .docker/db/redis
	chown -R 1001:1001 .docker/db
clean:
	rm -rf .docker/db/*
build:
	docker-compose -f docker-compose.yml build mqsensor
dev:
	make init
	docker-compose -f docker-compose.yml up nginx mqsensor mqsensor-db mqsensor-redis
start:
	make init
	docker-compose -f docker-compose.yml up -d nginx mqsensor mqsensor-db mqsensor-redis
stop:
	docker-compose -f docker-compose.yml down
build-arm:
	docker-compose -f docker-compose.arm.yml build armsensor
dev-arm:
	make init
	docker-compose -f docker-compose.arm.yml up nginx armsensor armsensor-db armsensor-redis
start-arm:
	make init
	docker-compose -f docker-compose.arm.yml up -d nginx armsensor armsensor-db armsensor-redis
stop-arm:
	docker-compose -f docker-compose.arm.yml down
seed-arm:
	docker exec -it armsensor knex seed:run