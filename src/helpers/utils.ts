
export const formatNumber = (num: number) => {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

export const phoneNumberSGP = (value: any) => {
  let re = /^([3|6|8|9]\d{7}|65[3|6|8|9]\d{7}|\+65(\s)?[3|6|8|9]\d{7})$/;
  if(!re.test(String(value).toLowerCase())){
    return false
  }
  return true
}

export function formatterCurrency(value: any) {
  return `${value}`.replace(/[^0-9]+/g, "0").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export function parserCurrency(value: any) {
  return value ? Number(value.replace(/\$\s?|(,*)/g, "")) : 0;
}

export function toStringDuration(seconds: number) {
  // Hours, minutes and seconds
  let hrs = ~~(seconds / 3600);
  let mins = ~~((seconds % 3600) / 60);
  let secs = ~~seconds % 60;

  // Output like "1:01" or "4:03:59" or "123:03:59"
  let ret = "";
  if (hrs > 0) {
      ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
  }
  ret += "" + mins + ":" + (secs < 10 ? "0" : "");
  ret += "" + secs;
  return ret;
}
