import {
  LockOutlined,
  UserOutlined,
  LoginOutlined,
  QrcodeOutlined,
} from "@ant-design/icons";
import { Button, Form, Input, Row, Col, Checkbox, Space } from "antd";
import useBaseHook from "@src/hooks/BaseHook";
import getConfig from "next/config";
import React from "react";
const { publicRuntimeConfig } = getConfig();

const Login = ({
  onSubmit,
  loading,
  link,
  text,
  icon,
}: {
  onSubmit: Function;
  loading: any;
  link: string;
  text: string;
  icon: any;
}) => {
  const { t, redirect } = useBaseHook();
  const [form] = Form.useForm();
  const onFinish = async (values: any) => {
    await onSubmit(values);
  };

  return (
    <div className="content-form">
      <Row>
        <Col span={24} style={{ textAlign: "center" }}>
          <Space direction="vertical">
            <div className="img">
              <img src={"/logo/logo.png"}></img>
            </div>
            <div className="sitename">{t("pages:login.content")}</div>
          </Space>
        </Col>
      </Row>
      <br />
      <Row>
        <Form
          onFinish={onFinish}
          form={form}
          name="loginForm"
          layout="horizontal"
          initialValues={{
            username: "",
            password: "",
            otp: "",
          }}
          style={{ width: "100%" }}
        >
          <Col span={24}>
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: t("messages:form.required", {
                    name: t("pages:login.username"),
                  }),
                },
                {
                  max: 32,
                  message: t("messages:form.maxLength", {
                    name: t("pages:login.username"),
                    length: 32,
                  }),
                },
              ]}
            >
              <Input
                placeholder={t("pages:login.username")}
                prefix={<UserOutlined />}
              />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: t("messages:form.required", {
                    name: t("pages:login.password"),
                  }),
                },
                {
                  max: 32,
                  message: t("messages:form.maxLength", {
                    name: t("pages:login.password"),
                    length: 32,
                  }),
                },
              ]}
            >
              <Input.Password
                placeholder={t("pages:login.password")}
                prefix={<LockOutlined />}
                autoComplete="off"
              />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item>
              <Row>
                <Col span={12}>
                  <Form.Item name="remember" valuePropName="checked" noStyle>
                    <Checkbox>{t("pages:login.rememberMe")}</Checkbox>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  {/* <a className="forgot-text" onClick={() => redirect("frontend.forgotPassword")}>
                {t ('pages:login.forgotPassword')}
              </a> */}
                </Col>
              </Row>
            </Form.Item>
          </Col>
          <Col span={24} style={{textAlign: "center"}}>
            <Button type="primary" htmlType="submit" loading={loading}>
              <LoginOutlined />
              {t("buttons:login")}
            </Button>
          </Col>
        </Form>
      </Row>
    </div>
  );
};

export default Login;
