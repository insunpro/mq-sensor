
import React, { useRef, useState } from "react";
import Xlsx from './Xlsx'
import { CloudUploadOutlined } from "@ant-design/icons"
import { Button } from 'antd';
const SheetJSFT = [
  "xlsx", "xlsb", "xlsm", "xls", "xml", "csv",
  "txt", "ods", "fods", "uos", "sylk", "dif",
  "dbf", "prn", "qpw", "123", "wb*",
  "wq*", "html", "htm"
].map(function (x) { return "." + x; }).join(",");

const ImportExcel = ({ getData, uploadTxt = "Upload File", setLoading = null, loading = false }) => {
  const fileInput = useRef(null);
  const handleChange = async (e: any) => {
    if(setLoading) setLoading(true)
    const files = e.target.files;
    if (files && files[0]) {
      let data = await Xlsx.readFileFromInput(files[0]);
      if (Array.isArray(data)) {
        data = data.filter(row => row.length !== 0);
      }
      if(typeof getData === 'function') getData(data);
    }
  }
  return <>
    <Button
      style={{ marginBottom: 10 }}
      onClick={() => fileInput.current.click()}
      loading={loading}>
      <CloudUploadOutlined /> {uploadTxt}
      
    </Button>
    <input
      ref={fileInput}
      onClick={(e: any) => e.target.value = null}
      type="file"
      hidden
      accept={SheetJSFT}
      onChange={handleChange}
    />
  </>
}

export default ImportExcel;
