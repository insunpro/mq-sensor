import { Modal } from "antd";
import React, { useEffect, useState } from "react";

interface ApiModalProps {
  visible: boolean;
  cid: number;
  onClose: () => any;
}

export default function ApiModal({ visible , cid, onClose}: ApiModalProps) {
    const [data, setData] = useState(null)

    useEffect(() => {
      console.log(cid)
    }, [cid])
    
    const fetchData = async () => {

    }
  return (
    <Modal visible={visible} title={"Api Setting"}>
      <div>content</div>
    </Modal>
  );
}
