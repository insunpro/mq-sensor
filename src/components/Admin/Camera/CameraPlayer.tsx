import { Button, Radio } from "antd";
import React from "react";
import ReactPlayer from "react-player";

interface CameraPlayerProps {
  url: string
}

export default function CameraPlayer({url} : CameraPlayerProps) {
  
  return (
    <div style={{border: "solid 1px"}}>
      <ReactPlayer
        url={url}
        playing={true}
        controls={false}
        loop={true}
        width="100%"
      />
    </div>
  );
}
