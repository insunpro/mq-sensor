import cameraService from "@root/src/services/cameraService";
import { Button, Modal, Radio } from "antd";
import to from "await-to-js";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import LiveCam from "./LiveCam";
interface StreamModalProps {
  visible: boolean;
  cid: number;
  onClose: () => any;
}

export default function StreamModal({
  visible,
  cid,
  onClose,
}: StreamModalProps) {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetchData();
    return () => {
      console.log("StreamModal.onUnmount");
      setData(null);
    };
  }, [cid]);

  const fetchData = async () => {
    let [err, res] = await to(cameraService().withAuth().detail({ id: cid }));
    if (err) return console.log("StreamModal.fetchData.err", err);
    return setData(res);
  };

  return (
    <Modal
      visible={visible}
      title={"Stream Setting"}
      footer={false}
      onCancel={() => {
        setData(null);
        onClose();
      }}
      width={920}
    >
      <LiveCam cid={cid} snap/>
    </Modal>
  );
}
