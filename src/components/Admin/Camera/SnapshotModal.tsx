import useBaseHooks from "@root/src/hooks/BaseHook";
import cameraService from "@root/src/services/cameraService";
import { Button, Empty, Modal, Space } from "antd";
import to from "await-to-js";
import React, { useEffect, useState } from "react";
import CameraLineIcon from "remixicon-react/CameraLineIcon";

interface SnapshotModalProps {
  visible: boolean;
  cid: number;
  onClose: () => any;
}

export default function SnapshotModal({
  visible=false,
  cid=null,
  onClose,
}: SnapshotModalProps) {
  const { t, notify } = useBaseHooks();
  const [loading, setLoading] = useState(false);
  const [img, setImg] = useState(null);
  const [camera, setCamera] = useState(null);

  useEffect(() => {
    fetchData()
  }, [cid])

  const fetchData = async () => {
    setLoading(true);
    let [err, res] = await to(cameraService().withAuth().lastSnap({ id: cid }));
    setLoading(false);
    if (err) return console.error("SnapshotModal.fetchData.err", err);
    res.img ? setImg("/snapshot/" + res.img) : setImg(null);
    setCamera(res);
  };

  const snapshot = async () => {
    setLoading(true);
    let [err, res] = await to(cameraService().withAuth().snapshot({ id: cid }));
    setLoading(false);
    if (err) return console.error("SnapshotModal.fetchData.err", err);
    res.img ? setImg("/snapshot/" + res.img) : setImg(null)
    return
  };
  return (
    <Modal
      visible={visible}
      title={camera && camera.name ? `Máy quay ${camera.name}` : "Chụp ảnh từ máy quay"}
      onCancel={() => {
        onClose();
      }}
      width={640}
      footer={
        <div style={{textAlign:"center"}}>
          <Button
            type={"primary"}
            loading={loading}
            onClick={snapshot}
            size="large"
          >
            <Space>
              <CameraLineIcon />
              {" Chụp ảnh"}
            </Space>
          </Button>
        </div>
      }
    >
      <div style={{ minHeight: 300 }}>
        {img ? <img src={img} width={600} /> : <Empty />}
      </div>
    </Modal>
  );
}
