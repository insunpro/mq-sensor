import { Button, Form, Input, Modal, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { EditOutlined } from "@ant-design/icons";
import to from "await-to-js";
import cameraService from "@root/src/services/cameraService";
import _ from "lodash";
import useBaseHooks from "@root/src/hooks/BaseHook";

interface ApiModalProps {
  visible: boolean;
  cid: number;
  onClose: () => any;
}

export default function ApiModal({ visible, cid, onClose }: ApiModalProps) {
  const { notify, t } = useBaseHooks()
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  useEffect(() => {
    fetchData()
    return () => {
      console.log("ApiModal.onUnmount")
      setData(null)
    }
  }, [cid])

  const fetchData = async () => {
    let [err, res] = await to(cameraService().withAuth().detail({id: cid}));
    if (err) return console.log("ApiModal.fetchData.err", err);
    form.setFieldsValue({
      api: {
        stream: _.get(res, 'info.api.stream', ''),
        ptz: _.get(res, 'info.api.ptz', ''),
        snapshot: _.get(res, 'info.api.snapshot', ''),
      }
    })
    return setData(res);
  };

  const onFinish = async (values) => {
    setLoading(true);
    let newData = {...data, info: {...values } }
    let [err, res] = await to(cameraService().withAuth().edit(newData));
    setLoading(false);
    if (err) {
      notify(t(`errors:${err.code}`), "", "error");
      return console.log("ApiModal.onFinish.err", err);
    }
    notify(t("messages:message.recordUpdated"));
    return onClose()
  }
  return (
    <Modal
      visible={visible}
      title={"Api Setting"}
      onCancel={() => onClose()}
      okButtonProps={{loading: loading}}
      onOk={async () => {
        form.submit();
      }}
      cancelButtonProps={{ hidden: true }}
      // footer={false}
    >
      {data && <div style={{textAlign: "center"}}>
        <Typography.Title level={5}>{data.name}</Typography.Title>
      </div>}
      <Form layout="vertical" form={form} onFinish={onFinish}>
        <Form.Item name={["api", "stream"]} label={"Api Streaming"}>
          <Input placeholder={"Streaming Url"} disabled={!data}/>
        </Form.Item>
        <Form.Item name={["api", "ptz"]} label={"Api Ptz"}>
          <Input placeholder={"Ptz Url"} disabled={!data}/>
        </Form.Item>
        <Form.Item name={["api", "snapshot"]} label={"Api Snapshot"}>
          <Input placeholder={"Snapshot Url"} disabled={!data}/>
        </Form.Item>
      </Form>
    </Modal>
  );
}
