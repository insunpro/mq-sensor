import useBaseHooks from "@root/src/hooks/BaseHook";
import cameraService from "@root/src/services/cameraService";
import {
  Button,
  Empty,
  Modal,
  Space,
  Form,
  Input,
  Alert,
  Typography,
} from "antd";
import to from "await-to-js";
import React, { useEffect, useState } from "react";
import { CheckOutlined, VideoCameraAddOutlined } from "@ant-design/icons";
import LinksFillIcon from "remixicon-react/LinksFillIcon";
import Save3FillIcon from "remixicon-react/Save3FillIcon";

interface ConnectModalProps {
  visible: boolean;
  data: any;
  onClose: () => any;
  onSuccess: () => any;
}

export default function ConnectModal({
  visible = false,
  data,
  onClose,
  onSuccess,
}: ConnectModalProps) {
  const { t, notify } = useBaseHooks();
  const [loading, setLoading] = useState(false);
  const [camera, setCamera] = useState(data);
  const [form] = Form.useForm();
  const [success, setSuccess] = useState(null);

  useEffect(() => {
    setCamera(data);
    form.setFieldsValue({ username: "", password: "" });
    setSuccess(false);
  }, [data]);

  const connect = async (values) => {
    setLoading(true);
    let [err, res] = await to(
      cameraService()
        .withAuth()
        .onvifConnect({ ...camera, ...values })
    );
    setLoading(false);
    if (err)
      return notify(
        t("messages:message.loginFailed"),
        t(`errors:${err.code}`),
        "error"
      );
    setSuccess(res);
  };

  const onSave = async () => {
    setLoading(true);
    let [err, res] = await to(
      cameraService()
        .withAuth()
        .onvifSave({ ...camera, ...form.getFieldsValue(), ...success })
    );
    setLoading(false);
    if (err)
      return notify(
        t("messages:message.loginFailed"),
        t(`errors:${err.code}`),
        "error"
      );
    notify(t("messages:message.recordCreated"));
    onClose();
    onSuccess();
  };
  return (
    <Modal
      visible={visible}
      title={
        <>
          Kết nối đến địa chỉ:{" "}
          <Typography.Text strong type="danger">
            {camera && `${camera.host}`}
          </Typography.Text>{" "}
          ({camera.hardware})
        </>
      }
      onCancel={() => {
        onClose();
      }}
      width={640}
      footer={
        <div style={{ textAlign: "center" }}>
          {success ? (
            <Button type={"primary"} loading={loading} onClick={() => onSave()}>
              <Space>
                <Save3FillIcon />
                {" Lưu máy quay"}
              </Space>
            </Button>
          ) : (
            <Button
              type={"primary"}
              loading={loading}
              onClick={() => form.submit()}
            >
              <Space>
                <LinksFillIcon />
                {" Kết nối"}
              </Space>
            </Button>
          )}
        </div>
      }
    >
      <Form form={form} layout="vertical" onFinish={connect}>
        <Form.Item
          label={t("pages:cameras.table.username")}
          name="username"
          rules={[
            {
              required: true,
              message: t("messages:form.required", {
                name: t("pages:cameras.table.username"),
              }),
            },
            {
              max: 64,
              message: t("messages:table.maxLength", {
                name: t("pages:cameras.table.username"),
                length: 64,
              }),
            },
          ]}
        >
          <Input
            placeholder={t("pages:cameras.table.username")}
            disabled={success ? true : false}
          />
        </Form.Item>
        <Form.Item
          label={t("pages:cameras.table.password")}
          name="password"
          rules={[
            {
              required: true,
              message: t("messages:form.required", {
                name: t("pages:cameras.table.password"),
              }),
            },
            {
              max: 64,
              message: t("messages:table.maxLength", {
                name: t("pages:cameras.table.password"),
                length: 64,
              }),
            },
          ]}
        >
          <Input
            placeholder={t("pages:cameras.table.password")}
            disabled={success ? true : false}
          />
        </Form.Item>
        {success ? (
          <Alert
            message={
              <Typography.Text strong>Đã kết nối đến máy quay</Typography.Text>
            }
            description={
              <>
                <br />
                <p>Hãng sản xuất: {success.Manufacturer}</p>
                <p>Mẫu sản phẩm: {success.Model}</p>
                <p>Phiên bản: {success.FirmwareVersion}</p>
                <p>Mã sản phẩm: {success.SerialNumber}</p>
                <p>ID: {success.HardwareId}</p>
              </>
            }
            type="success"
            showIcon
          />
        ) : (
          ""
        )}
      </Form>
    </Modal>
  );
}
