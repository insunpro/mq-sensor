import React, { useEffect, useState } from "react";
import {
  LeftOutlined,
  RightOutlined,
  UpOutlined,
  DownOutlined,
  HomeOutlined,
  ZoomInOutlined,
  ZoomOutOutlined,
} from "@ant-design/icons";
import { Button, Radio, Spin } from "antd";
import MapPinLine from "remixicon-react/MapPinLineIcon";
import CameraLine from "remixicon-react/CameraLineIcon";
import SocketService from "@root/src/services/socket/socketService";
import cameraService from "@root/src/services/cameraService";
import to from "await-to-js";
import useBaseHook from "@src/hooks/BaseHook";

interface LiveCamProps {
  cid: number;
  snap: boolean;
  sid?: number;
  cb?: () => any;
}

const LiveCam = ({ cid, snap = false, sid = null, cb }: LiveCamProps) => {
  const { t, notify } = useBaseHook();

  const [snapshot, setSnap] = useState(null);
  const [socket, setSocket] = useState(null);
  const [speed, setSpeed] = useState(0.75);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    let sk = new SocketService(cid);
    const callbackImg = (data) => {
      setSnap(data);
    };
    sk.getStream(callbackImg);
    setSocket(sk);
    return () => {
      console.log("StreamModal.onUnmount");
      socket && socket.close();
    };
  }, [cid]);

  const handlePtzMove = (anySocket, direction, speed) => {
    console.log("handlePtzMove", direction);
    if (!anySocket || !anySocket.connected) return;
    if (direction === "home") return anySocket.ptzGotoHome();
    return anySocket.ptzMove(direction, speed);
  };

  const handlePtzStop = (anySocket) => {
    console.log("handlePtzStop");
    if (!anySocket || !anySocket.connected) return;
    return anySocket.ptzStop();
  };

  const handleSnapshot = async () => {
    console.log("snapshot");
  };

  const handleSavePreset = async (cameraId, sensorId, callback) => {
    setLoading(true);
    let [err, res] = await to(cameraService().withAuth().savePreset({id: cameraId, sid: sensorId}))
    setLoading(false);
    if (err) return notify(t(`errors:${err.code}`), "", "error");
    console.log("handleSavePreset", res)
    if (callback && typeof callback === "function") {
      callback()
    }
  }

  return (
    <div className="connected-device">
      {snapshot ? (
        <img
          className="snapshot"
          src={"data:image/png;base64," + snapshot}
          style={{ width: "100%" }}
        />
      ) : (
        <Spin style={{ marginTop: 200 }} />
      )}
      {/* <div className="device-info-box">
        <span className="name"></span> (<span className="address"></span>)
      </div> */}
      <div className="ptz-ctl-box">
        <div className="ptz-pad-box">
          <button type="button" className="ptz-goto-home">
            {/* <span className="glyphicon glyphicon-home"></span> */}
            <HomeOutlined
              onClick={() => handlePtzMove(socket, "home", speed)}
            />
          </button>
          <LeftOutlined
            className="left"
            onMouseDown={() => handlePtzMove(socket, "left", speed)}
            onMouseUp={() => handlePtzStop(socket)}
          />
          <UpOutlined
            className="up"
            onMouseDown={() => handlePtzMove(socket, "up", speed)}
            onMouseUp={() => handlePtzStop(socket)}
          />
          <DownOutlined
            className="down"
            onMouseDown={() => handlePtzMove(socket, "down", speed)}
            onMouseUp={() => handlePtzStop(socket)}
          />
          <RightOutlined
            className="right"
            onMouseDown={() => handlePtzMove(socket, "right", speed)}
            onMouseUp={() => handlePtzStop(socket)}
          />
        </div>
      </div>
      <div className="ptz-spd-ctl-box">
        <Radio.Group
          options={[
            {
              label: "Chậm",
              value: 0.5,
            },
            {
              label: "Trung bình",
              value: 0.75,
            },
            {
              label: "Nhanh",
              value: 1.0,
            },
          ]}
          value={speed}
          onChange={(e) => setSpeed(e.target.value)}
          optionType="button"
          buttonStyle="solid"
        />
      </div>
      <div
        className="ptz-zom-ctl-box btn-group btn-group-lg"
        role="group"
        aria-label="Zoom"
      >
        <Button
          className="ptz-zom ptz-zom-ot"
          onMouseDown={() => handlePtzMove(socket, "out", speed)}
          onMouseUp={() => handlePtzStop(socket)}
        >
          <ZoomOutOutlined />
        </Button>
        <Button
          className="ptz-zom ptz-zom-in"
          onMouseDown={() => handlePtzMove(socket, "in", speed)}
          onMouseUp={() => handlePtzStop(socket)}
        >
          <ZoomInOutlined />
        </Button>
      </div>
      <div className="disconnect-box">
        {sid && (
          <Button
            className="form-control"
            icon={<MapPinLine size="1.2em" />}
            onClick={() => handleSavePreset(cid, sid, cb)}
            loading={loading}
            type="primary"
          >
            {" Lưu vị trí"}
          </Button>
        )}
        {snap && (
          <Button
            className="form-control"
            icon={<CameraLine size="1.2em" />}
            onClick={() => handleSnapshot()}
            loading={loading}
            type="primary"
          >
            Chụp ảnh
          </Button>
        )}
      </div>
    </div>
  );
};

export default LiveCam;
