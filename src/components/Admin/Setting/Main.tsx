import {
  Button,
  Checkbox,
  Col,
  Form,
  Radio,
  Row,
  Space,
  Typography,
} from "antd";
import React, { useEffect, useState } from "react";
import useBaseHook from "@root/src/hooks/BaseHook";
import { alertLevels, alertServices } from "@root/config/constant";
import settingService from "@root/src/services/settingService";
import to from "await-to-js";
import { SaveFilled, CheckOutlined, CloseOutlined } from "@ant-design/icons";

export default function SettingMain() {
  const { t, getData, notify, getStore } = useBaseHook();
  const [loading, setLoading] = useState(false);
  const [changed, setChanged] = useState(false);
  const [form] = Form.useForm();
  const defaultPreview = {low: false, medium: false, high: false};
  const [preview, setPreview] = useState(defaultPreview)
  let isMobile = getStore("isMobile");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setLoading(true);
    let [err, res] = await to(settingService().withAuth().getMain({}));
    setLoading(false);
    if (err) return notify(t(`errors:${err.code}`), "", "error");
    onChangePreview(res.value.alertLevel)
    form.setFieldsValue(res.value);
  };

  const onFinish = async (values) => {
    setLoading(true);
    let [err, res] = await to(settingService().withAuth().editMain(values));
    setLoading(false);
    if (err) return notify(t(`errors:${err.code}`), "", "error");
    setChanged(false);
    return notify(t("messages:message.recordUpdated"));
  };

  const onChangePreview = (value) => {
    switch (value) {
      case 1:
        setPreview({low: false, medium: false, high: true})
        break;
      case 2:
        setPreview({low: false, medium: true, high: true})
        break;
      case 3:
        setPreview({low: true, medium: true, high: true})
        break;
      default:
        setPreview(defaultPreview)
    }
  }

  return (
    <div>
      <Row>
        <Col span={24}>
          <Typography.Title level={4} style={{ textAlign: "center" }}>
            Cài đặt Hệ thống
          </Typography.Title>
        </Col>
      </Row>
      <Row>
        <Form
          form={form}
          layout="vertical"
          name="settings"
          scrollToFirstError
          onFinish={onFinish}
          style={{ width: "100%" }}
          onChange={() => setChanged(true)}
        >
          <Col md={24} lg={12}>
            <Form.Item name="alertLevel" label={"1. Mức độ cảnh báo"}>
              <Radio.Group onChange={(e) => onChangePreview(e.target.value)}>
                <Space direction={isMobile ? "vertical" : "horizontal"}>
                  {Object.keys(alertLevels).map((key) => {
                    return (
                      <Radio key={key} value={alertLevels[key]}>
                        {t("pages:settings.alertLevels." + key)}
                      </Radio>
                    );
                  })}
                </Space>
              </Radio.Group>
            </Form.Item>
            <Form.Item>
              <Space direction="vertical">
                <div>
                  {preview.low ? <CheckOutlined style={{color: "#52c41a"}}/> : <CloseOutlined style={{color: "#ff4d4f"}}/>}
                  {" Tin nhắn cảnh báo mức độ thấp"}
                </div>
                <div>
                  {preview.medium ? <CheckOutlined style={{color: "#52c41a"}}/> : <CloseOutlined style={{color: "#ff4d4f"}}/>}
                  {" Tin nhắn cảnh báo mức độ trung bình"}
                </div>
                <div>
                  {preview.high ? <CheckOutlined style={{color: "#52c41a"}}/> : <CloseOutlined style={{color: "#ff4d4f"}}/>}
                  {" Tin nhắn cảnh báo mức độ cao"}
                </div>
              </Space>
            </Form.Item>
          </Col>
          <Col md={24} lg={12}></Col>
          <Col md={24} lg={12}>
            <Form.Item name="services" label={"2. Dịch vụ cảnh báo"}>
              <Checkbox.Group>
                <Space direction="vertical">
                  {Object.keys(alertServices).map((key) => {
                    return (
                      <Checkbox key={key} value={alertServices[key]} disabled={key === "skype"}>
                        {t("pages:settings.services." + key)}
                      </Checkbox>
                    );
                  })}
                </Space>
              </Checkbox.Group>
            </Form.Item>
          </Col>
          <Col md={24} lg={12}></Col>

          <Form.Item wrapperCol={{ span: 24 }} style={{ textAlign: "center" }}>
            <Button
              type="primary"
              htmlType="submit"
              loading={loading}
              disabled={!changed}
            >
              <SaveFilled /> {t("buttons:submit")}
            </Button>
          </Form.Item>
        </Form>
      </Row>
    </div>
  );
}
