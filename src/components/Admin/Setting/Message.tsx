import messageService from "@root/src/services/messageService";
import { Badge, Button, Col, Row, Table, Tooltip, Typography } from "antd";
import to from "await-to-js";
import React, { useEffect, useState } from "react";
import { EditOutlined } from "@ant-design/icons";
import useBaseHooks from "@root/src/hooks/BaseHook";
import { alertLevels } from "@root/config/constant";
import MessageModal from "../Message/MessageModal";

export default function SettingMessage() {
  const { t } = useBaseHooks()
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [mid, setMid] = useState(null);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setLoading(true);
    let [err, result] = await to(
      messageService().withAuth().index({ pageSize: -1 })
    );
    if (err) {
      console.error(err);
      return;
    }
    setData(result.data);
    setLoading(false);
  };

  const columns: any = [
    // {
    //   title: "#",
    //   key: "id",
    //   dataIndex: "id",
    // },
    // {
    //   title: "Từ khóa",
    //   key: "key",
    //   dataIndex: "key",
    // },
    {
      title: "Tên",
      key: "name",
      dataIndex: "name",
    },
    {
      title: "Nội dung",
      key: "value",
      dataIndex: "value",
      render: (text: string, record: any) =>
        record.value.content || "Chưa có nội dung",
    },
    {
      title: "Mức độ cảnh báo",
      key: "alertLevel",
      dataIndex: "alertLevel",
      render: (text: string, record: any) => {
        let key = Object.keys(alertLevels).find(key => alertLevels[key] === record.alertLevel);
        let status = "offline";
        if (record.alertLevel === alertLevels.low) status = "online";
        if (record.alertLevel === alertLevels.medium) status = "disabled";
        if (record.alertLevel === alertLevels.high) status = "deleted";
        return (<><span className={`status status-${status} status-medium`}></span>{t("pages:settings.alertLevels."+key)}</>)
      }
    },
    {
      title: "Tùy chỉnh",
      key: "value",
      align: "center",
      render: (text: string, record: any) => {
        return (
          <Tooltip title="Chỉnh sửa nội dung">
            <Button type="default" icon={<EditOutlined />} onClick={() => onEdit(record.id)}></Button>
          </Tooltip>
        );
      },
    },
  ];

  const onEdit = (mid: number) => {
    setVisible(true);
    setMid(mid);
  }

  return (
    <div>
      <Row>
        <Col span={24}>
          <Typography.Title level={4} style={{ textAlign: "center" }}>
            Cài đặt tin nhắn
          </Typography.Title>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Table
            bordered
            columns={columns}
            dataSource={data}
            pagination={false}
            loading={loading}
          />
          {
            mid && <MessageModal visible={visible} mid={mid} onClose={() => setVisible(false)} onChange={() => fetchData()}/>
          }
        </Col>
      </Row>
    </div>
  );
}
