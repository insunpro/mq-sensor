import { Form, Input, Modal, Typography } from "antd";
import React, { useEffect, useState } from "react";
import useBaseHook from "@src/hooks/BaseHook";
import to from "await-to-js";
import messageService from "@root/src/services/messageService";

interface MessageModalProps {
  mid: number;
  visible: boolean;
  onClose: () => any;
}

export default function MessageModal({
  mid,
  visible,
  onClose,
}: MessageModalProps) {
  const { t, notify, redirect } = useBaseHook();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);

  useEffect(() => {
    fetchData()
    return () => {
      console.log("ApiModal.onUnmount")
      setData(null)
    }
  }, [mid])

  const fetchData = async () => {
    let [err, res] = await to(messageService().withAuth().detail({id: mid}));
    if (err) return console.log("ApiModal.fetchData.err", err);
    // form.setFieldsValue({
    //   api: {
    //     stream: _.get(res, 'info.api.stream', ''),
    //     ptz: _.get(res, 'info.api.ptz', ''),
    //     snapshot: _.get(res, 'info.api.snapshot', ''),
    //   }
    // })
    return setData(res);
  };

  const onFinish = async (values) => {
    setLoading(true);
    let [error, result] = await to(
      messageService()
        .withAuth()
        .edit({ ...values, id: mid })
    );
    if (error) {
      setLoading(false)
      return notify(t(`errors:${error.code}`), error, "error");
    }
    onClose()
    setLoading(false)
    return notify(t("messages:message.recordUpdated"));
  };

  return (
    <Modal
      visible={visible}
      title={"Chỉnh sửa tin nhắn"}
      cancelButtonProps={{ hidden: true }}
      onCancel={() => {
        onClose();
      }}
      destroyOnClose={true}
      onOk={async () => {
        form.submit();
      }}
      okButtonProps={{loading: loading}}
    >
      {/* <Typography.Text>Mã thiết bị: {device.serialNumber}</Typography.Text>
      <br/>
      <Typography.Text>Mã cảm biến: {data?.sensorId}</Typography.Text> */}
      <br/>
      <br/>
      <Form
        name="edit"
        onFinish={onFinish}
        preserve={false}
        form={form}
      >
        <Form.Item name="description">
          <Input.TextArea placeholder={t("common:description")} />
        </Form.Item>
      </Form>
    </Modal>
  );
}
