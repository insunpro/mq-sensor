import { Button, Col, Form, Input, InputNumber, Row, Typography } from "antd";
import React, { useEffect, useState } from "react";
import useBaseHook from "@src/hooks/BaseHook";
import to from "await-to-js";
import settingService from "@root/src/services/settingService";
import { SaveFilled } from "@ant-design/icons";

export default function SettingZalo() {
  const { t, getData, notify } = useBaseHook();
  const [loading, setLoading] = useState(false);
  const [changed, setChanged] = useState(false);
  const [form] = Form.useForm();

  useEffect(() => {
    fetchData()
  }, [])

  const fetchData = async () => {
    setLoading(true)
    let [err, res] = await to(settingService().withAuth().getZalo({}));
    setLoading(false)
    if (err) return notify(t(`errors:${err.code}`), "", "error");
    form.setFieldsValue(res.value)
  };

  const onFinish = async (values: any): Promise<void> => {
    setLoading(true);
    let [error] = await to(
      settingService()
        .withAuth()
        .editZalo(values)
    );
    setLoading(false);
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    setChanged(false)
    return notify(t("messages:message.recordUpdated"));
  };

  return (
    <div>
      <Row>
        <Col span={24}>
        <Typography.Title level={4} style={{textAlign: "center"}}>Cài đặt Zalo</Typography.Title>
        </Col>
      </Row>
      <Row>
        <Form
          form={form}
          layout="vertical"
          name="settings"
          onFinish={onFinish}
          scrollToFirstError
          style={{width: "100%"}}
          onChange={() => setChanged(true)}
        >
          <Form.Item
            label={t("pages:settings.zalo.groupId")}
            name="groupId"
            wrapperCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: t("messages:form.required", {
                  name: t("pages:settings.zalo.groupId"),
                }),
              },
              {
                max: 128,
                message: t("messages:form.maxLength", {
                  name: t("pages:settings.zalo.groupId"),
                  length: 128,
                }),
              },
            ]}
          >
            <Input placeholder={t("pages:settings.zalo.groupId")} />
          </Form.Item>
          <Form.Item
            label={t("pages:settings.zalo.uuid")}
            name="uuid"
            wrapperCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: t("messages:form.required", {
                  name: t("pages:settings.zalo.uuid"),
                }),
              },
              {
                max: 128,
                message: t("messages:form.maxLength", {
                  name: t("pages:settings.zalo.uuid"),
                  length: 128,
                }),
              },
            ]}
          >
            <Input placeholder={t("pages:settings.zalo.uuid")} />
          </Form.Item>
          <Form.Item
            label={t("pages:settings.zalo.cookie")}
            name="cookie"
            wrapperCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: t("messages:form.required", {
                  name: t("pages:settings.zalo.cookie"),
                }),
              },
              {
                max: 5000,
                message: t("messages:form.maxLength", {
                  name: t("pages:settings.zalo.cookie"),
                  length: 5000,
                }),
              },
            ]}
          >
            <Input.TextArea
              placeholder={t("pages:settings.zalo.cookie")}
              rows={10}
            />
          </Form.Item>
          <Form.Item
            label={t("common:delayTime")}
            name="delay"
            wrapperCol={{ span: 24 }}
            rules={[
              {
                required: true,
                message: t("messages:form.required", {
                  name: t("common:delayTime"),
                }),
              },
            ]}
          >
            <InputNumber placeholder={"1 - 3600"} min={1} max={3600} style={{width: "100%"}}/>
          </Form.Item>
          <Form.Item wrapperCol={{ span: 24 }} style={{ textAlign: "center" }}>
            <Button type="primary" htmlType="submit" loading={loading} disabled={!changed}>
              <SaveFilled /> {t("buttons:submit")}
            </Button>
          </Form.Item>
        </Form>
      </Row>
    </div>
  );
}
