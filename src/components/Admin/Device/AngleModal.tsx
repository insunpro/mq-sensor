import {
  Button,
  Col,
  Divider,
  Empty,
  Form,
  Input,
  Modal,
  Row,
  Select,
  Slider,
  Space,
  Table,
  Tabs,
  Tooltip,
  Typography,
} from "antd";
import React, { useCallback, useEffect, useState } from "react";
import useBaseHook from "@src/hooks/BaseHook";
import sensorService from "@root/src/services/sensorService";
import to from "await-to-js";
import SensorAngle from "../Camera/SensorAngle";
import _ from "lodash";
import useSWR from "swr";
import cameraService from "@root/src/services/cameraService";
import EditBoxLineIcon from "remixicon-react/EditBoxLineIcon";
import DeleteBin2LineIcon from "remixicon-react/DeleteBin2LineIcon";
import AngleSetting from "./AngleSetting";
import LiveCam from "../Camera/LiveCam";
import moment from "moment";

interface AngleModalModalProps {
  record: any;
  visible: boolean;
  onClose: () => void;
  device: any;
}

export default function AngleModal({
  record,
  visible,
  onClose,
  device,
}: AngleModalModalProps) {
  const defaulValues = {
    cid: null,
    h: null,
    v: null,
    z: null,
  };
  const { t, notify, redirect, getStore } = useBaseHook();
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [cameras, setCameras] = useState([]);
  const [values, setValues] = useState({
    cid: null,
    h: null,
    v: null,
    z: null,
  });

  const onSelect = (value) => {
    let newVal = { ...values };
    _.set(newVal, "cid", value);
    setValues(newVal);
  };

  const columns: any = [
    {
      title: "Tên máy quay",
      dataIndex: "name",
      key: "name",
      render: (text: string, record: any) => record.camera?.name,
    },
    // {
    //   title: "Góc quay nằm ngang",
    //   dataIndex: "info.h",
    //   key: "info.h",
    //   render: (text: string, record: any) => record.info.h + "°",
    // },
    // {
    //   title: "Góc quay thẳng đứng",
    //   dataIndex: "info.v",
    //   key: "info.v",
    //   render: (text: string, record: any) => record.info.v + "°",
    // },
    // {
    //   title: "Độ phóng đại (zoom)",
    //   dataIndex: "info.z",
    //   key: "info.z",
    //   render: (text: string, record: any) => record.info.z + "%",
    // },
    {
      title: t("common:updated_at"),
      dataIndex: "info.created_at",
      key: "info.created_at",
      render: (text: string, record: any) => moment(record.created_at).format("HH:mm:ss DD/MM/YYYY"),
    },
    {
      title: "Tác vụ",
      key: "task",
      align: "center",
      render: (text: string, record: any) => {
        return (
          <Space>
            <Tooltip title={"Sửa vị trí"}>
              <Button
                type="default"
                icon={<EditBoxLineIcon />}
                onClick={() => onSelect(record.camera_id)}
              ></Button>
            </Tooltip>
            <Tooltip title={"Xóa vị trí"}>
              <Button
                type="default"
                danger
                icon={<DeleteBin2LineIcon />}
                onClick={() => removeCamera(record.camera_id)}
                loading={loading}
              ></Button>
            </Tooltip>
          </Space>
        );
      },
    },
  ];

  const fetchData = async () => {
    setLoading(true);
    let [err, res] = await to(
      sensorService().withAuth().indexCamera({ id: record.id })
    );
    setLoading(false);
    if (err) return console.error("AngleModal.fetchData.err", err);
    return setData(res);
  };

  const fetchCamera = async () => {
    setLoading(true);
    let [err, res] = await to(
      sensorService().withAuth().select2camera({ id: record.id })
    );
    setLoading(false);
    if (err) return console.error("AngleModal.fetchCamera.err", err);
    return setCameras(res);
  };

  useEffect(() => {
    fetchData();
    fetchCamera();
  }, [values, record]);

  const removeCamera = async (cid) => {
    setLoading(true);
    let [err, res] = await to(
      cameraService().withAuth().removePreset({ id: cid, sid: record.id })
    );
    setLoading(false);
    if (err) return console.error("AngleModal.removeCamera.err", err);
    fetchData();
    fetchCamera();
  };

  return (
    <Modal
      visible={visible}
      title={"Cài đặt vị trí của cảm biến"}
      cancelButtonProps={{ hidden: true }}
      onCancel={() => {
        setValues(defaulValues);
        onClose();
      }}
      destroyOnClose={true}
      okButtonProps={{ hidden: true }}
      width={1000}
    >
      <Row>
        <Col span={24}>
          <Typography.Title level={5} type="danger">
            Thiết bị: {device.name} ({device.serialNumber})
          </Typography.Title>
          <Typography.Text strong type="danger">
            Cảm biến: {record.sensorId}
          </Typography.Text>
        </Col>
      </Row>
      <Tabs activeKey={values.cid ? "edit" : "cameras"}>
        <Tabs.TabPane
          tab="Danh sách máy quay"
          key={"cameras"}
          disabled={values.cid}
        >
          <Row>
            <Col span={24}>
              <Select
                allowClear
                showSearch
                value={values.cid}
                options={cameras}
                style={{ width: "100%" }}
                onSelect={onSelect}
                placeholder={"Thêm máy quay"}
                filterOption
                optionFilterProp="label"
                loading={loading}
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={24}>
              <Table dataSource={data} columns={columns} rowKey="id" bordered/>
            </Col>
          </Row>
        </Tabs.TabPane>
        <Tabs.TabPane tab="Cài đặt vị trí" key={"edit"} disabled={!values.cid}>
          {values.cid ? (
            <LiveCam
              cid={values.cid}
              snap={false}
              sid={record.id}
              cb={() => setValues(defaulValues)}
            />
          ) : (
            <Empty />
          )}
        </Tabs.TabPane>
      </Tabs>
    </Modal>
  );
}
