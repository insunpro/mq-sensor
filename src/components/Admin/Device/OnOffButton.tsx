import { Switch } from "antd";
import to from "await-to-js";
import React, { useState } from "react";
import useBaseHook from "@src/hooks/BaseHook";

interface OnOffButtonProps {
  defaultChecked?: boolean;
  disabled?: boolean;
  onChange: (checked: boolean, event: Event) => Promise<boolean>;
}

export default function OnOffButton({ defaultChecked=false, disabled = false, onChange }: OnOffButtonProps) {
  const [loading, setLoading] = useState(false);
  const [checked, setChecked] = useState(defaultChecked);
  const { t, notify } = useBaseHook();
  
  const handleOnChange = async (checked: boolean, event: Event) => {
    setLoading(true);
    let result = await onChange(checked, event)
    result ? setChecked(checked) : setChecked(!checked)
    setLoading(false);
  }
  return (
    <Switch
      // defaultChecked={defaultChecked}
      checked={checked}
      checkedChildren={"Bật"}
      unCheckedChildren={"Tắt"}
      loading={loading}
      disabled={disabled}
      onChange={handleOnChange}
    />
  );
}
