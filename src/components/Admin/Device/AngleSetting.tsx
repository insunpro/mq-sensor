import cameraService from "@root/src/services/cameraService";
import sensorService from "@root/src/services/sensorService";
import {
  Button,
  Col,
  Divider,
  Row,
  Slider,
  Space,
  Spin,
  Typography,
} from "antd";
import to from "await-to-js";
import _ from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import SensorAngle from "../Camera/SensorAngle";

interface AngleSetting {
  id: number;
  cid: number;
  callBack?: () => any;
}

export default function AngleSetting({ id, cid, callBack }: AngleSetting) {
  const [setting, setSetting] = useState(null);
  const [camera, setCamera] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async () => {
      let [err, result] = await to(
        sensorService().withAuth().getCamera({ id, cid })
      );
      if (err) return console.error("AngleSetting.useEffect.err", err);
      let vFrom, vTo, hFrom, hTo, z;
      if (result.setting) {
        vFrom = 90 - result.setting.v - (result.info.verticalAngle / 2);
        vTo = 90 - result.setting.v + (result.info.verticalAngle / 2);
        hFrom = result.setting.h - (result.info.horizontalAngle / 2);
        hTo = modulus360(result.setting.h + (result.info.horizontalAngle / 2));
        z = result.setting.z;
      } else {
        vFrom = 0;
        vTo = result.info.verticalAngle || 60;
        hFrom = 0;
        hTo = result.info.horizontalAngle || 120;
        z = result.info.zoom || 1
      }
      setSetting({
        v: {
          from: vFrom,
          to: vTo,
        },
        h: {
          from: hFrom,
          to: hTo,
        },
        z: z,
      });
      return setCamera(result);
    })();
  }, [cid]);

  const onChange = useCallback(
    (prop, value) => {
      let updatedAngleRange = { ...setting };
      _.set(updatedAngleRange, prop, value);
      setSetting(updatedAngleRange);
    },
    [setting, setSetting]
  );

  const onVChange = useCallback(
    ({ from, to }) => {
      onChange("v", { from, to });
    },
    [onChange]
  );

  const onHChange = useCallback(
    ({ from, to }) => {
      onChange("h", { from, to });
    },
    [onChange]
  );

  const onZChange = useCallback(
    (value) => {
      onChange("z", value);
    },
    [onChange]
  );

  const modulus360 = (angle) => (angle + 360) % 360;

  const onSave = async () => {
    setLoading(true);
    let values = { ...setting };
    let z = values.z;
    let h = modulus360(setting.h.from + modulus360(setting.h.to - setting.h.from) /2);
    let v = 90 - modulus360(setting.v.to + setting.v.from) / 2;

    let [err, res] = await to(sensorService().withAuth().updateCamera({id, cid, h, v, z}))
    setLoading(false);
    if (err) return console.error("AngleSetting.onSave.err", err);
    return callBack ? callBack() : true
  }

  return !camera ? (
    <div>
      <Spin />
    </div>
  ) : (
    <div>
      <Row>
        <Col span={24} style={{ textAlign: "center" }}>
          <Typography.Title level={5}>Thông tin máy quay</Typography.Title>
          <div>Tên: {camera.name}</div>
          <div>
            Thông số kỹ thuật: {camera.info.horizontalAngle}° -{" "}
            {camera.info.verticalAngle}° - {camera.info.zoom}%
          </div>
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <Typography.Text strong>Góc quay nằm ngang</Typography.Text>
          <div>
            Vị trí cảm biến: {modulus360(setting.h.from + modulus360(setting.h.to - setting.h.from) /2)}°
          </div>
          <div>Góc nhìn: {modulus360(setting.h.to - setting.h.from)}°</div>
          <div>Từ: {setting.h.from}°</div>
          <div>Đến: {setting.h.to}°</div>
        </Col>
        <Col span={12} style={{ textAlign: "center" }}>
          <SensorAngle
            radius={60}
            value={setting.h}
            onChange={onHChange}
            handlerRadius={10}
            handlerRangeRadiusOffset={0}
            min={0}
            max={360}
            minOffset={0}
            maxOffset={360}
            rangeAdjust={false}
          />
        </Col>
      </Row>
      <Divider style={{ width: "90%" }} />
      <Row>
        <Col span={12}>
          <Typography.Text strong>Góc quay thẳng đứng</Typography.Text>
          <div>
            Vị trí cảm biến:{" "}
            {90 - modulus360(setting.v.to + setting.v.from) / 2}°
          </div>
          <div>Góc nhìn: {modulus360(setting.v.to - setting.v.from)}°</div>
          <div>Từ: {setting.v.from}°</div>
          <div>Đến: {setting.v.to}°</div>
        </Col>
        <Col span={12} style={{ textAlign: "center", marginBottom: 30 }}>
          <SensorAngle
            radius={100}
            value={setting.v}
            onChange={onVChange}
            handlerRadius={10}
            handlerRangeRadiusOffset={10}
            min={0}
            max={105}
            minOffset={0}
            maxOffset={360}
            isQuarterCircle
            rangeAdjust={false}
          />
        </Col>
      </Row>
      <Divider style={{ width: "90%" }} />
      <Row>
        <Col span={12}>
          <Typography.Text strong>Mức độ phóng đại (Zoom)</Typography.Text>
          <div>Tỷ lệ: {setting.z}%</div>
        </Col>
        <Col span={12}>
          <Slider
            defaultValue={setting.z}
            marks={{
              1: "1%",
              25: "25%",
              50: "50%",
              75: "75%",
              100: "100%",
              128: "128%",
            }}
            max={128}
            min={1}
            onChange={onZChange}
            tooltipVisible={false}
          />
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24} style={{ textAlign: "center" }}>
          <Space>
            <Button
              type="default"
              onClick={() => callBack ? callBack() : null}
              loading={loading}
            >
              Quay lại
            </Button>
            <Button
              type="primary"
              onClick={() => onSave()}
              loading={loading}
            >
              Xác nhận
            </Button>
          </Space>
        </Col>
      </Row>
    </div>
  );
}
