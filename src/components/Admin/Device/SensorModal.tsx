import { Form, Input, Modal, Typography } from "antd";
import React, { useEffect, useState } from "react";
import useBaseHook from "@src/hooks/BaseHook";
import sensorService from "@root/src/services/sensorService";
import to from "await-to-js";

interface SensorModalProps {
  record: any;
  visible: boolean;
  onClose: () => void;
  device: any
}

export default function SensorModal({
  record,
  visible,
  onClose,
  device
}: SensorModalProps) {
  const { t, notify, redirect } = useBaseHook();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);

  const onFinish = async (values) => {
    setLoading(true);
    let [error, result] = await to(
      sensorService()
        .withAuth()
        .edit({ ...values, id: record.id })
    );
    if (error) {
      setLoading(false)
      return notify(t(`errors:${error.code}`), error, "error");
    }
    onClose()
    setLoading(false)
    return notify(t("messages:message.recordUpdated"));
  };

  useEffect(() => {
    setData(record)
    form.setFieldsValue(record)
  }, [record])

  return (
    <Modal
      visible={visible}
      title={"Sửa thông tin cảm biến"}
      cancelButtonProps={{ hidden: true }}
      onCancel={() => {
        onClose();
      }}
      destroyOnClose={true}
      onOk={async () => {
        form.submit();
      }}
      okButtonProps={{loading: loading}}
    >
      <Typography.Text>Mã thiết bị: {device.serialNumber}</Typography.Text>
      <br/>
      <Typography.Text>Mã cảm biến: {data?.sensorId}</Typography.Text>
      <br/>
      <br/>
      <Form
        name="edit"
        onFinish={onFinish}
        preserve={false}
        form={form}
      >
        <Form.Item name="description">
          <Input.TextArea placeholder={t("common:description")} />
        </Form.Item>
      </Form>
    </Modal>
  );
}
