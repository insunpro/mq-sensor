import {
  Badge,
  Button,
  Progress,
  Space,
  Table,
  Tooltip,
  Modal,
  Row,
  Col,
  Switch,
} from "antd";
import moment from "moment";
import React, { useCallback, useEffect, useState } from "react";
import useBaseHook from "@src/hooks/BaseHook";
import EditBoxLineIcon from "remixicon-react/EditBoxLineIcon";
import CameraLineIcon from "remixicon-react/CameraLineIcon";
import DeleteBin2LineIcon from "remixicon-react/DeleteBin2LineIcon";
import SensorModal from "./SensorModal";
import to from "await-to-js";
import sensorService from "@root/src/services/sensorService";
import _ from "lodash";
import OnOffButton from "./OnOffButton";
import AngleModal from "./AngleModal";

export default function SensorTable({
  record,
  onChange,
  forceReload,
}: {
  record: Device;
  onChange: any;
  forceReload: boolean;
}) {
  const { t, notify, redirect } = useBaseHook();
  const [visible, setVisible] = useState(false);
  const [visible2, setVisible2] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalData, setModalData] = useState(null);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData()
  }, [forceReload])

  const fetchData = async () => {
    setLoading(true);
    const [err, res] = await to(sensorService().withAuth().getByDevice({deviceId: record.id}));
    setLoading(false);
    if (err) return console.error("SensorTable.fetchData.err", err);
    return setData(res)
  }

  const onChangeData = () => {
    fetchData();
    onChange();
  }

  const eColumns: any[] = [
    {
      title: "Mã cảm biến",
      key: "eSensorId",
      dataIndex: "sensorId",
    },
    {
      title: "Dung lượng pin",
      key: "eBattery",
      dataIndex: "battery",
      align: "center",
      render: (text, eRecord) => {
        let color;
        if (eRecord.battery < 20) {
          color = "#f5222d";
        } else if (eRecord.battery <= 50) {
          color = "#fa8c16";
        } else {
          color = "#52c41a";
        }
        return (
          <Progress
            percent={eRecord.battery}
            steps={10}
            size="small"
            strokeColor={color}
          />
        );
      },
    },
    {
      title: t("common:description"),
      dataIndex: "description",
      key: "eDescription",
    },
    {
      title: t("common:status"),
      dataIndex: "status",
      key: "eStatus",
      align: "center",
      render: (text: string, record) => {
        let status = record.status === 1 ? "online" : "offline";
        return (
          <Tooltip
            title={record.status === 1 ? "Đang hoạt động" : "Dừng hoạt động"}
          >
            <span className={`status status-${status} status-medium`}></span>
          </Tooltip>
        );
      },
    },
    {
      title: "Cảnh báo",
      key: "modes->alert",
      align: "center",
      render: (text: string, record) => {
        return (
          <Tooltip title={"Chế độ cảnh báo"}>
            <OnOffButton
              defaultChecked={record.modes?.alert === 1}
              onChange={async (checked, event): Promise<boolean> =>
                await onChangeMode(record, "alert", checked)
              }
            />
          </Tooltip>
        );
      },
    },
    {
      title: t("common:updated_at"),
      key: "eUpdated_at",
      dataIndex: "updated_at",
      align: "center",
      render: (text: string, record: any) =>
        text ? moment(text).format("HH:mm:ss, DD/MM/YYYY") : "-",
    },
    {
      title: "Tác vụ",
      key: "eTask",
      align: "center",
      render: (text: string, record) => {
        return (
          <Space>
            <Tooltip title={"Cài đặt vị trí"}>
              <Button
                type="default"
                icon={<CameraLineIcon />}
                onClick={() => handleModal("camera", record)}
              ></Button>
            </Tooltip>
            <Tooltip title={"Sửa thông tin"}>
              <Button
                type="default"
                icon={<EditBoxLineIcon />}
                onClick={() => handleModal("edit", record)}
              ></Button>
            </Tooltip>
            <Tooltip title={"Xóa cảm biến"}>
              <Button
                type="default"
                danger
                icon={<DeleteBin2LineIcon />}
                onClick={() => handleDelete(record)}
                loading={loading}
              ></Button>
            </Tooltip>
          </Space>
        );
      },
    },
  ];

  const handleModal = useCallback(
    (modal, data) => {
      setModalData(data);
      modal == "edit" ? setVisible(true) : setVisible2(true);
  }, [])

  const handleDelete = async (data) =>
    Modal.confirm({
      title: t("buttons:deleteItem"),
      content: t("messages:message.deleteConfirm"),
      okButtonProps: { danger: true },
      onOk: async () => {
        setLoading(true);
        let [error, rs] = await to(
          sensorService().withAuth().destroy({ id: data.id })
        );
        setLoading(false);
        if (error) return notify(t(`errors:${error.code}`), "", "error");
        notify(t("messages:message.recordDeleted"));
        onChangeData();
      },
    });

  const handleDeleteAll = async (data) =>
    Modal.confirm({
      title: t("buttons:deleteItem"),
      content: t("messages:message.deleteAllConfirm"),
      okButtonProps: { danger: true },
      onOk: async () => {
        setLoading(true);
        let [error, rs] = await to(
          sensorService().withAuth().deleteAll({ device_id: data.id })
        );
        setLoading(false);
        if (error) return notify(t(`errors:${error.code}`), "", "error");
        notify(t("messages:message.recordDeleted"));
        onChangeData();
      },
    });

  const onChangeMode = async (record, mode, value) => {
    let [err, res] = await to(
      sensorService().withAuth().changeMode({ id: record.id, mode, value })
    );
    if (err) {
      notify(t(`errors:${err.code}`), "", "error");
      return false;
    }
    notify(t("messages:message.recordUpdated"));
    return true;
  };

  return (
    <Row gutter={[16, 16]} className="sensor-table">
      {!_.isEmpty(data) && (
        <Col span={24} style={{ textAlign: "right" }}>
          <Button
            type="dashed"
            danger
            style={{ marginRight: 40 }}
            onClick={() => handleDeleteAll(record)}
            loading={loading}
          >
            Xóa tất cả cảm biến
          </Button>
        </Col>
      )}
      <Col span={24}>
        <Table
          columns={eColumns}
          dataSource={data}
          size="small"
          pagination={{ pageSize: 5 }}
          rowKey="sensorId"
          bordered={true}
          loading={loading}
        />
        {modalData && <SensorModal
          visible={visible}
          record={modalData}
          onClose={() => {
            setVisible(false);
            onChangeData();
          }}
          device={record}
        />}
        {modalData && <AngleModal
          visible={visible2}
          record={modalData}
          onClose={() => {
            setVisible2(false);
            onChangeData();
          }}
          device={record}
        />}
      </Col>
    </Row>
  );
}
