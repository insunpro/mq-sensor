import { alertLevels } from "@root/config/constant";
import useBaseHooks from "@root/src/hooks/BaseHook";
import messageService from "@root/src/services/messageService";
import { Form, Input, Modal, Radio } from "antd";
import to from "await-to-js";
import React, { useEffect, useState } from "react";

const MessageModal = ({ visible = false, mid = null, onClose, onChange }) => {
  const { t, notify } = useBaseHooks();
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  useEffect(() => {
    fetchData();
  }, [mid]);

  const fetchData = async () => {
    setLoading(true);
    let [err, res] = await to(messageService().withAuth().detail({ id: mid }));
    setLoading(false);
    if (err) return console.error("MessageModal.fetchData.err", err);
    form.setFieldsValue(res);
  };

  const onFinish = async (values) => {
    setLoading(true);
    let [error, result] = await to(
      messageService()
        .withAuth()
        .edit({ ...values, id: mid })
    );
    if (error) {
      setLoading(false);
      return notify(t(`errors:${error.code}`), error, "error");
    }
    onChange()
    onClose();
    setLoading(false);
    return notify(t("messages:message.recordUpdated"));
  };

  return (
    <Modal
      visible={visible}
      title={"Chỉnh sửa tin nhắn"}
      cancelButtonProps={{ hidden: true }}
      onCancel={() => {
        onClose();
      }}
      destroyOnClose={true}
      onOk={async () => {
        form.submit();
      }}
      okButtonProps={{ loading: loading }}
    >
      <Form onFinish={onFinish} layout="vertical" form={form} name="edit">
        <Form.Item label="Tiêu đề cảnh báo" name="name">
          <Input disabled/>
        </Form.Item>
        <Form.Item label="Nội dung cảnh báo" name={["value", "content"]}>
          <Input.TextArea disabled rows={4}/>
        </Form.Item>
        <Form.Item label="Mức độ cảnh báo" name="alertLevel">
          <Radio.Group
            options={Object.keys(alertLevels).map((key) => {
              return {
                label: t("pages:settings.alertLevels." + key),
                value: alertLevels[key],
              };
            })}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default MessageModal;
