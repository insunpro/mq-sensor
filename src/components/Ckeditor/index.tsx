import NHFinder from "@ngochipx/nhfinder-ant3/reactjs";
import React, { useEffect, useState, useRef } from "react";

let CKEditor = null;
let InlineEditor = null;
let ckEditor = null;
const CKeditor1 = () => {
  let refNHFinder = useRef();
  const getContent = () => {};
  const [reload, setReload] = useState(false);
  useEffect(() => {
    CKEditor = require("@ckeditor/ckeditor5-react").CKEditor;
    InlineEditor = require("@ckeditor/ckeditor5-build-inline");
    setReload(true);
  }, []);
  console.log("ckEditor ", ckEditor);
  return (
    <React.Fragment>
      {CKEditor && InlineEditor && (
        <CKEditor
          // @ts-ignore
          editor={InlineEditor}
          data={"<b>Click here to insert content ...</b>"}
          onInit={(editor) => {
            ckEditor = editor;
          }}
          config={{
            imageManagement: {
              component: refNHFinder,
              options: {
                apiUrl: "http://localhost:3333/api/v1/nhfinder",
                apiKey: "123456", //should match the backend ApiKey.
              },
            },
            allowedContent: "*",
            // ...config
          }}
        />
      )}
      <NHFinder ref={(ref) => (refNHFinder = ref)} />
    </React.Fragment>
  );
};
export default CKeditor1;
