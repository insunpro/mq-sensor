import React, { useState, useEffect } from "react";
import {
  Layout,
  Drawer,
  BackTop,
  Row,
  Col,
  Typography,
  Spin,
  Breadcrumb,
  Menu,
} from "antd";
import useBaseHooks from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";

import Head from "next/head";
import dynamic from "next/dynamic";
import getConfig from "next/config";
import useSWR from "swr";
import Error403 from "@src/components/Errors/403";
const Sidebar = dynamic(() => import("./admin/Sidebar"), { ssr: false });
const Header = dynamic(() => import("./admin/Header"), { ssr: false });
const BreadCrumb = dynamic(() => import("@src/components/BreadCrumb"), {
  ssr: false,
});
const { Content, Footer } = Layout;

import { getRouteData } from "@src/helpers/routes";
import auth from "../helpers/auth";
import roles from "@root/config/roles";

const THEME = "light";
const { publicRuntimeConfig } = getConfig();

const Admin = (props: any) => {
  const { router, t, setStore, redirect } = useBaseHooks();
  const { checkPermission } = usePermissionHook();
  const [collapsed, setCollapsed] = useState(true);
  const [isMobile, setIsMobile] = useState(false);
  const [loading, setLoading] = useState(true);

  const onCollapseChange = (value: boolean, track = "null") => {
    // console.log(">>>>>>>>> onCollapseChange", value, track)
    setCollapsed(value);
  };

  const updateSize = () => {
    const mobile = window.innerWidth < 992;
    setIsMobile(mobile);
    setStore("isMobile", mobile);
  };

  // const checkRole = () => {
  //   let user = auth().user;
  //   if (user.role != roles.admin) {
  //     return redirect("/login");
  //   }

  //   setLoading(false);
  // };

  useEffect(() => {
    window.addEventListener("resize", updateSize);
    updateSize();
    // checkRole()
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  if (!checkPermission(props.permissions || {})) {
    return <Error403 />;
  }

  const getRouteName = async () => {
    const routePath = router.pathname;
    const routeData: any = await getRouteData();
    for (let routeName in routeData) {
      let routeElement = routeData[routeName];
      if (!routeElement.action) continue;
      if (routeElement.action.substr(5) === routePath) return routeName;
    }
  };

  const { data: routeName } = useSWR("getRouteName", () => getRouteName());

  return (
    <>
      <Head>
        <title>{props.title || publicRuntimeConfig.SITE_NAME || ""}</title>
        <meta
          property="og:title"
          content={props.title || publicRuntimeConfig.TITLE || ""}
        />
        <meta
          property="og:description"
          content={props.description || publicRuntimeConfig.DESCRIPTION || ""}
        />
        <link
          rel="shortcut icon"
          type="image/png"
          href={publicRuntimeConfig.FAVICON}
        />
        <meta property="og:image" content={publicRuntimeConfig.LOGO} />
        <link rel="apple-touch-icon" href={publicRuntimeConfig.LOGO_IOS}></link>
      </Head>
      <div id="admin">
        <Layout style={{ minHeight: "100vh" }}>
          {isMobile ? (
            <Drawer
              maskClosable
              closable={false}
              destroyOnClose={true}
              onClose={() => onCollapseChange(false, "drawer")}
              visible={collapsed}
              placement="left"
              bodyStyle={{
                padding: 0,
                height: "100vh",
              }}
            >
              <Sidebar
                className="slider"
                collapsed={false}
                onCollapseChange={onCollapseChange}
                theme={THEME}
                isMobile={isMobile}
              />
            </Drawer>
          ) : (
            <Sidebar
              className="slider"
              collapsed={collapsed}
              onCollapseChange={onCollapseChange}
              theme={THEME}
              isMobile={isMobile}
            />
          )}

          <Layout className="site-layout">
            <Header collapsed={collapsed} onCollapseChange={onCollapseChange} />
            <Content style={{ margin: "0 16px" }}>
              <Row>
                <Col span={24}>
                  <div style={{ float: "right", marginTop: 16 }}>
                    <BreadCrumb />
                  </div>
                </Col>
              </Row>
              {props.children}
            </Content>
            {/* <Footer style={{ textAlign: "center" }}>
                MQ Solutions ICT ©2021
              </Footer> */}
          </Layout>
        </Layout>
      </div>
    </>
  );
};

export default Admin;
