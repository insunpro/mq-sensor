import React, { useState, useEffect } from "react";
import Avatar from "react-avatar";
import { Menu, Layout, Button, Row, Col } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  SettingOutlined,
  UserSwitchOutlined,
} from "@ant-design/icons";
import to from "await-to-js";
import useBaseHook from "@src/hooks/BaseHook";
import auth from "@src/helpers/auth";
import ChangePassword from "@src/components/GeneralComponents/ChangePassword";
import { confirmDialog } from "@src/helpers/dialogs";
import authService from "@src/services/authService";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

const { Header } = Layout;
const { SubMenu } = Menu;
const AdminHeader = (props: any) => {
  const { t, notify, redirect, getData, router } = useBaseHook();
  const [showChangePassword, setShowChangePassword] = useState(false);

  const renderRightContent = () => {
    const username = getData(auth(), "user.username", "User");
    const name = getData(auth(), "user.name", "User");
    return (
      <>
        <Menu key="user" mode="horizontal">
          <SubMenu
            title={
              <>
                <span className="crop-name">
                  {" "}
                  {t("common:hi", { username: name })}{" "}
                </span>
                <Avatar
                  style={{ marginLeft: 8 }}
                  size="32"
                  name={name}
                  round={true}
                />
              </>
            }
            key="user-menu"
          >
            <Menu.Item key="change-password">
              <Button
                onClick={() => {
                  setShowChangePassword(true);
                }}
                type="link"
              >
                {t("buttons:changePassword")} <SettingOutlined />
              </Button>
            </Menu.Item>
            <Menu.Item key="signout">
              <Button
                type="link"
                onClick={() => {
                  confirmDialog({
                    onOk: () => {
                      authService().withAuth().logout({ username });
                      redirect("frontend.login");
                    },
                    title: t("buttons:signout"),
                    content: t("messages:message.signoutConfirm"),
                    okText: "Đồng ý",
                    cancelText: "Hủy",
                  });
                }}
              >
                {t("buttons:signout")} <UserSwitchOutlined />
              </Button>
            </Menu.Item>
          </SubMenu>
        </Menu>
      </>
    );
  };

  const onChangePassword = async (data: any): Promise<void> => {
    setShowChangePassword(false);
    let password = data.password;
    let [error, result]: [any, any] = await to(
      authService().withAuth().changePassword({ password })
    );
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordUpdated"));
    return result;
  };

  const renderPasswordDialog = () => {
    return (
      <ChangePassword
        onChangePassword={onChangePassword}
        visible={showChangePassword}
        onCancel={() => {
          setShowChangePassword(false);
        }}
      />
    );
  };

  const { collapsed, onCollapseChange } = props;
  const menuIconProps = {
    className: "trigger",
    onClick: () => onCollapseChange(!collapsed, "menu.icon"),
  };

  let headerClass = "header";
  if (collapsed) headerClass += " collapsed";

  return (
    <React.Fragment>
      <Header className="admin-header" style={{padding: 0}}>
        <Row gutter={[0,0]}>
          <Col span={24}>
            <div style={{display: "inline-block", marginLeft:25}}>
            {collapsed ? (
                <MenuUnfoldOutlined style={{fontSize: 18}} {...menuIconProps} />
              ) : (
                <MenuFoldOutlined style={{fontSize: 18}} {...menuIconProps} />
              )}
            </div>
            <div style={{ display: "inline-block", float:"right" }}>
              {renderRightContent()}
            </div>
          </Col>
        </Row>
      </Header>
      {renderPasswordDialog()}
    </React.Fragment>
  );
};

export default AdminHeader;
