import React from "react";
import { Layout, Row, Col, Divider } from "antd";
import Menu from "./Menu";

const { Sider } = Layout;
import Profile from "./Profile";

const sideBar = (props: any) => {
  const { collapsed, onCollapseChange, isMobile, theme } = props;
  return (
    <Sider
      width={256}
      collapsedWidth={isMobile ? 0 : undefined}
      trigger={null}
      breakpoint="md"
      theme={theme}
      collapsible
      collapsed={collapsed}
      // onBreakpoint={(broken) => onCollapseChange(broken, "broken")}
    >
      <div style={{ textAlign: "center", marginTop: 10,marginBottom: 50  }}>
        {collapsed ? (
          <img src="/logo/favicon.ico" height={45}></img>
        ) : (
          <img src="/logo/logo.png" height={45}></img>
        )}
      </div>
      <Menu
        theme={theme}
        onCollapseChange={onCollapseChange}
        collapsed={collapsed}
        isMobile={isMobile}
      />
    </Sider>
  );
};

export default sideBar;
