import {
  HomeOutlined, UserOutlined, TeamOutlined, AndroidOutlined,
  FieldNumberOutlined, CloudUploadOutlined, FieldBinaryOutlined,
  UnorderedListOutlined, CheckSquareOutlined, AuditOutlined, SettingOutlined
} from "@ant-design/icons";

import DashboardFillIcon from 'remixicon-react/DashboardFillIcon'

import UserSearchFillIcon from 'remixicon-react/UserSearchFillIcon'
import UserSearchLineIcon from 'remixicon-react/UserSearchLineIcon'

import Building2FillIcon from 'remixicon-react/Building2FillIcon'
import Building2LineIcon from 'remixicon-react/Building2LineIcon'

import PieChartFillIcon from 'remixicon-react/PieChartFillIcon'
import BarChartGroupedLineIcon from 'remixicon-react/BarChartGroupedLineIcon'
import BarChartLineIcon from 'remixicon-react/BarChartLineIcon'


import CameraLineIcon from 'remixicon-react/CameraLineIcon'
import Image2FillIcon from 'remixicon-react/Image2FillIcon'

import TeamFillIcon from 'remixicon-react/TeamFillIcon'
import FileHistoryFillIcon from 'remixicon-react/FileHistoryFillIcon'

import SignalTowerFillIcon from 'remixicon-react/SignalTowerFillIcon'
import SettingsFillIcon from 'remixicon-react/SettingsFillIcon'

import ToolsFillIcon from 'remixicon-react/ToolsFillIcon'
import PinDistanceFillIcon from 'remixicon-react/PinDistanceFillIcon'


interface RouteParams {}

interface SideMenu {
  routeName: string
  icon?: any
  routeParams: RouteParams
  type?: string
  children?: SideMenu[]
  permissions?: any
}

const adminSidebar: SideMenu[] = [
  // {
  //   routeName: "frontend.admin.dashboard.index",
  //   icon: <DashboardFillIcon />,
  //   routeParams: {},
  // },
  {
    routeName: "frontend.admin.devices.index",
    icon: <SignalTowerFillIcon />,
    routeParams: {},
  },
  {
    routeName: "frontend.admin.cameras.index",
    icon: <CameraLineIcon />,
    routeParams: {},
  },
  {
    routeName: "frontend.admin.logs.index",
    icon: <FileHistoryFillIcon />,
    routeParams: {},
  },
  {
    routeName: "frontend.admin.snapshots.index",
    icon: <Image2FillIcon />,
    routeParams: {},
  },
  {
    routeName: "frontend.admin.groups.index",
    icon: <PinDistanceFillIcon />,
    routeParams: {},
  },
  {
    routeName: "frontend.admin.users.index",
    icon: <TeamFillIcon />,
    routeParams: {},
  },
  {
    routeName: "frontend.admin.settings.index",
    icon: <ToolsFillIcon />,
    routeParams: {},
  },
];

export default adminSidebar;
