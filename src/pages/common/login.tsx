import React, { useEffect, useState } from "react";
import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Login"), { ssr: false });
import { LeftOutlined } from "@ant-design/icons";
import authService from "@src/services/authService";
import to from "await-to-js";
import useBaseHook from "@src/hooks/BaseHook";
import auth from "@src/helpers/auth";
import LoginComponent from "@src/components/GeneralComponents/Login";
import roles from "@root/config/roles";
import { Card, Col, Row } from "antd";

const Login = () => {
  const { t, notify, redirect } = useBaseHook();
  const [loading, setLoading] = useState(false);
  let user = auth().user;

  // useEffect(() => {
  //   if (!user) return;
  //   if (user.role == roles.admin) {
  //     redirect("frontend.admin.dashboard.index");
  //   }
  //   if (user.role == roles.user) {
  //     redirect("frontend.user.dashboard.index");
  //   }
  // }, []);

  const onFinish = async (values: any) => {
    setLoading(true);
    let [error, result]: any[] = await to(authService().login(values));
    setLoading(false);
    if (error)
      return notify(
        t("messages:message.loginFailed"),
        t(`errors:${error.code}`),
        "error"
      );
    console.log(result);
    auth().setAuth(result);
    const { user } = result;
    notify(t("messages:message.loginSuccess"));
    redirect("frontend.admin.devices.index");
    return result;
  };

  return (
    <>
      <Row>
        <Col
          xs={{ span: 22, offset: 1 }}
          sm={{ span: 18, offset: 3 }}
          md={{ span: 14, offset: 5 }}
          lg={{ span: 12, offset: 6 }}
          xl={{ span: 8, offset: 8 }}
        >
          <Card style={{marginTop: 120}}>
            <LoginComponent
              onSubmit={onFinish}
              loading={loading}
              link="frontend.admin.dashboard"
              icon={<LeftOutlined />}
              text="home"
            />
          </Card>
        </Col>
      </Row>
    </>
  );
};

Login.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:login.title")}
      description={t("pages:login.description")}
      {...props}
    />
  );
};

export default Login;
