import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import { GridTable } from "@src/components/Table";
import FilterDatePicker from "@src/components/Table/SearchComponents/DatePicker";
import { Button, Col, Row, Space, Typography } from "antd";
import userService from "@src/services/userService";
import _ from "lodash";
import moment from "moment";
import to from "await-to-js";
import auth from "@src/helpers/auth";
import React, { useState, useRef } from "react";
import { confirmDialog } from "@src/helpers/dialogs";
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  PlusCircleOutlined,
  DeleteOutlined,
  LoginOutlined,
  EditOutlined,
} from "@ant-design/icons";

const Index = () => {
  const { t, notify, redirect } = useBaseHook();
  const tableRef = useRef(null);
  const { checkPermission } = usePermissionHook();
  const [selectedIds, setSelectedIds] = useState([]);
  const [hiddenDeleteBtn, setHiddenDeleteBtn] = useState(true);
  const createPer = checkPermission({
    users: "C",
  });
  const updatePer = checkPermission({
    users: "U",
  });
  const deletePer = checkPermission({
    users: "D",
  });

  const columns = [
    {
      title: t("pages:users.table.name"),
      dataIndex: "name",
      key: "users.name",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) => {
        return true ? (
          <a
            onClick={() =>
              redirect("frontend.admin.users.edit", { id: record.id })
            }
          >
            <span className="show-on-hover">
              {record.name}
              <EditOutlined className="show-on-hover-item" />
            </span>
          </a>
        ) : (
          record.name
        );
      },
    },
    {
      title: t("pages:users.table.username"),
      dataIndex: "username",
      key: "username",
      sorter: true,
      filterable: true,
    },
    {
      title: t("pages:users.table.phone"),
      dataIndex: "phone",
      key: "phone",
      sorter: true,
      filterable: true,
    },
    {
      title: t("pages:users.table.email"),
      dataIndex: "email",
      key: "email",
      sorter: true,
      filterable: true,
    },
    {
      title: t("pages:users.table.role"),
      dataIndex: "role.name",
      key: "role.name",
      sorter: true,
      filterable: true,
      render: (text: string, record) =>
        record.role ? record.role.name : record.role_id,
    },
    {
      title: t("pages:users.table.createdAt"),
      dataIndex: "created_at",
      key: "users.created_at",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) =>
        text ? moment(text).format("HH:mm:ss, DD/MM/YYYY") : "",
      renderFilter: ({ column, confirm, ref }: FilterParam) => (
        <FilterDatePicker column={column} confirm={confirm} ref={ref} />
      ),
    },
  ];

  const onChangeSelection = (data: any) => {
    if (data.length > 0) setHiddenDeleteBtn(false);
    else setHiddenDeleteBtn(true);
    setSelectedIds(data);
  };

  const fetchData = async (values: any) => {
    if (!values.sorting.length) {
      values.sorting = [{ field: "users.id", direction: "desc" }];
    }
    let [error, users]: [any, User[]] = await to(
      userService().withAuth().index(values)
    );
    if (error) {
      const { code, message } = error;
      notify(t(`errors:${code}`), t(message), "error");
      return {};
    }
    return users;
  };

  const onDelete = async () => {
    let [error, result]: any[] = await to(
      userService().withAuth().delete({ ids: selectedIds })
    );
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordDeleted"));
    if (tableRef.current !== null) {
      tableRef.current.reload();
    }
    setSelectedIds([]);
    setHiddenDeleteBtn(true);
    return result;
  };

  const rowSelection = {
    getCheckboxProps: (record) => ({
      disabled: record.id == auth().user.id,
      id: record.id,
    }),
  };

  return (
    <div className="content">
      <Row>
        <Col md={24} lg={12}>
          <Typography.Title level={5}>
            {t("pages:users.index.title")}
          </Typography.Title>
          <Typography.Text>
            {t("pages:users.index.description")}
          </Typography.Text>
        </Col>
        <Col md={24} lg={12} style={{ textAlign: "right" }}>
          <Space>
            <Button
              type="primary"
              danger
              hidden={hiddenDeleteBtn}
              onClick={() => {
                confirmDialog({
                  title: t("buttons:deleteItem"),
                  content: t("messages:message.deleteConfirm"),
                  onOk: () => onDelete(),
                });
              }}
            >
              <DeleteOutlined />
              {t("buttons:delete")}
            </Button>
            <Button
              onClick={() => redirect("frontend.admin.users.create")}
              type="primary"
            >
              <PlusCircleOutlined />
              {t("buttons:create")}
            </Button>
          </Space>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <GridTable
            ref={tableRef}
            columns={columns}
            fetchData={fetchData}
            rowSelection={{
              selectedRowKeys: selectedIds,
              onChange: (data: any[]) => onChangeSelection(data),
              ...rowSelection,
            }}
            addIndexCol={false}
          />
        </Col>
      </Row>
    </div>
  );
};

Index.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:users.index.title")}
      description={t("pages:users.index.description")}
      {...props}
    />
  );
};

// Index.permissions = {
//   "users": "R"
// };

export default Index;
