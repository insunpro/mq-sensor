import React from 'react'
import { Form, Input, Row, Col, Select } from 'antd';
import useBaseHook from '@src/hooks/BaseHook'
import validatorHook from '@src/hooks/ValidatorHook'
import { LockOutlined } from '@ant-design/icons';
import roleService from '@root/src/services/roleService';
import useSWR from 'swr';

const { Option } = Select

export default function UserForm({ form, isEdit }: { form: any, isEdit: boolean }) {
  const { t, getData } = useBaseHook();
  const { validatorRePassword, CustomRegex } = validatorHook();
  const { data } = useSWR('roles.select2', () => roleService().withAuth().select2({}));
  const roles = data || []

  return <Row gutter={[24, 0]}>
    <Col md={24}>
      <Form.Item
        label={t("pages:users.form.username")}
        name="username"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:users.form.username') }) },
          { whitespace: true, message: t('messages:form.required', { name: t('pages:users.form.username') }) },
          { min: 6, message: t('messages:form.minLength', { name: t('pages:users.form.username'), length: 6 }) },
          { max: 100, message: t('messages:form.maxLength', { name: t('pages:users.form.username'), length: 100 }) },
          CustomRegex({
            length: 6,
            reGex: '^[0-9A-z._](\\w|\\.|_){5,100}$',
            message: t('messages:form.username')
          })
        ]}
      >
        <Input
          placeholder={t("pages:users.form.username")}
          readOnly={isEdit}
        />
      </Form.Item>
    </Col>

    {!isEdit ? (
      <>
        <Col md={12}>
          <Form.Item
            label={t("pages:users.form.password")}
            name="password"
            rules={[
              { required: true, message: t('messages:form.required', { name: t('pages:users.form.password') }) },
              { min: 6, message: t('messages:form.minLength', { name: t('pages:users.form.password'), length: 6 }) },
              { max: 100, message: t('messages:form.maxLength', { name: t('pages:users.form.password'), length: 100 }) },
              // CustomRegex({
              //   length: 6,
              //   reGex: '^[0-9A-Za-z]\\w{5,100}$',
              //   message: t('messages:form.password')
              // })
            ]}
          >
            <Input.Password
              placeholder={t('pages:users.form.password')}
              prefix={<LockOutlined />}
              autoComplete="off"
            />
          </Form.Item>
        </Col>
        <Col md={12}>
          <Form.Item
            label={t("pages:users.form.rePassword")}
            name="rePassword"
            rules={[
              { required: true, message: t('messages:form.required', { name: t('pages:users.form.rePassword') }) },
              validatorRePassword({
                key: 'password',
                message: t('messages:form.rePassword'),
                getFieldValue: form.getFieldValue
              })
            ]}
          >
            <Input.Password
              placeholder={t('pages:users.form.rePassword')}
              prefix={<LockOutlined />}
              autoComplete="off"
            />
          </Form.Item>
        </Col>
      </>
    ) : null}

    <Col md={24}>
      <Form.Item
        label={t("pages:users.form.role")}
        name="role_id"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:users.form.role') }) },
        ]}
      >
        <Select placeholder={t("pages:users.form.role")}>
          {roles.map((role: any) => (
            <Option value={role.value} key={role.value}>{role.label}</Option>
          ))}
        </Select>
      </Form.Item>
    </Col>
    <Col md={24}>
      <Form.Item
        label={t("pages:users.table.email")}
        name="email"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:users.form.email') }) },
          { type: 'email', message: t('messages:form.email') },
          { max: 255, message: t('messages:form.maxLength', { name: t('pages:users.table.email'), length: 255 }) }
        ]}
      >
        <Input
          placeholder={t('pages:users.table.email')}
          type="email"
        />
      </Form.Item>
    </Col>
    <Col md={12}>
      <Form.Item
        label={t("pages:users.form.name")}
        name="name"
        rules={[
          // { required: true, message: t('messages:form.required', { name: t('pages:users.form.name') }) },
          { max: 255, message: t('messages:form.maxLength', { name: t('pages:users.form.name'), length: 255 }) }
        ]}
      >
        <Input
          placeholder={t('pages:users.form.name')}
        />
      </Form.Item>
    </Col>
    <Col md={12}>
      <Form.Item
        label={t("pages:users.form.phone")}
        name="phone"
        rules={[
          // { required: true, message: t('messages:form.required', { name: t('pages:users.form.phone') }) },
          { max: 12, message: t('messages:form.maxLength', { name: t('pages:users.form.phone'), length: 12 }) }
        ]}
      >
        <Input
          placeholder={t('pages:users.form.phone')}
        />
      </Form.Item>
    </Col>
  </Row>
}

