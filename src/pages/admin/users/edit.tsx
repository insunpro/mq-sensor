import React, { useState, useEffect } from "react";
import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import { Button, Form, Col, Row, Spin, Space } from "antd";
import userService from "@src/services/userService";
import { confirmDialog } from "@src/helpers/dialogs";
import to from "await-to-js";
import useBaseHook from "@src/hooks/BaseHook";
import { LeftCircleFilled, SaveFilled, DeleteFilled } from "@ant-design/icons";
import usePermissionHook from "@src/hooks/PermissionHook";
import UserForm from "./form";
import auth from "@src/helpers/auth";

export default function EditUser({ record }: { record: User }) {
  const { t, notify, redirect, router } = useBaseHook();
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  //submit form
  const onFinish = async (values: any): Promise<void> => {
    setLoading(true);
    let [error, result]: any[] = await to(
      userService()
        .withAuth()
        .edit({
          id: record.id,
          ...values,
        })
    );
    setLoading(false);
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordUpdated"));
    redirect("frontend.admin.users.index");
    return result;
  };

  const onDelete = async (): Promise<void> => {
    let [error, result]: any[] = await to(
      userService().withAuth().destroy({ id: record.id })
    );
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordDeleted"));
    redirect("frontend.admin.users.index");
    return result;
  };

  if (!record)
    return (
      <div className="content">
        <Spin />
      </div>
    );
  return (
    <>
      <div className="content">
        <Form
          form={form}
          layout="vertical"
          name="edit"
          initialValues={{
            name: record.name,
            phone: record.phone,
            username: record.username,
            email: record.email,
            role_id: record.role_id,
          }}
          onFinish={onFinish}
          scrollToFirstError
        >
          <Row>
            <Col md={{ span: 16, offset: 4 }}>
              <UserForm form={form} isEdit={true} />
              <Form.Item
                wrapperCol={{ span: 24 }}
                style={{ textAlign: "center" }}
              >
                <Space>
                  <Button onClick={() => router.back()}>
                    <LeftCircleFilled /> {t("buttons:back")}
                  </Button>
                  <Button type="primary" htmlType="submit" loading={loading}>
                    <SaveFilled /> {t("buttons:submit")}
                  </Button>
                  <Button
                    type="primary"
                    danger
                    onClick={() => {
                      confirmDialog({
                        title: t("buttons:deleteItem"),
                        content: t("messages:message.deleteConfirm"),
                        onOk: () => onDelete(),
                      });
                    }}
                  >
                    <DeleteFilled /> {t("buttons:deleteItem")}
                  </Button>
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    </>
  );
}

EditUser.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:users.edit.title")}
      description={t("pages:users.edit.description")}
      {...props}
    />
  );
};

EditUser.getInitialProps = async (ctx: any) => {
  let [err, record] = await to(
    userService().withAuth(ctx).detail({ id: ctx.query.id })
  );
  if (err) {
    console.log(err);
  }
  return {
    record,
  };
};
