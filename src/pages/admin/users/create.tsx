import React, { useState } from "react";
import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import { Button, Form, Col, Row, Space } from "antd";
import userService from "@src/services/userService";
import to from "await-to-js";
import useBaseHook from "@src/hooks/BaseHook";
import { LeftCircleFilled, SaveFilled } from "@ant-design/icons";
import UserForm from "./form";

export default function CreateUser() {
  const { t, notify, redirect, router } = useBaseHook();
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();
  //submit form
  const onFinish = async (values: any): Promise<void> => {
    setLoading(true);
    let { rePassword, ...otherValues } = values;
    let [error, result]: any[] = await to(
      userService().withAuth().create(otherValues)
    );
    setLoading(false);
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordCreated"));
    // redirect("frontend.admin.users.index")
    router.back();
    return result;
  };

  return (
    <div className="content">
      <Form
        form={form}
        name="create"
        layout="vertical"
        onFinish={onFinish}
        scrollToFirstError
      >
        <Row>
          <Col md={{ span: 16, offset: 4 }}>
            <UserForm form={form} isEdit={false} />
            <Form.Item
              wrapperCol={{ span: 24 }}
              style={{ textAlign: "center" }}
            >
              <Space>
                <Button onClick={() => router.back()}>
                  <LeftCircleFilled /> {t("buttons:back")}
                </Button>
                <Button type="primary" htmlType="submit" loading={loading}>
                  <SaveFilled /> {t("buttons:submit")}
                </Button>
              </Space>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

CreateUser.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:users.create.title")}
      description={t("pages:users.create.description")}
      {...props}
    />
  );
};
