import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  Row,
  Col,
  Typography,
  Dropdown,
  Menu,
  Button,
  Space,
  Image,
  Card,
  Checkbox,
} from "antd";
import React, { useEffect, useRef, useState } from "react";
import { DownOutlined, DeleteOutlined } from "@ant-design/icons";
import moment from "moment";
import { FilterDatePicker, GridTable } from "@root/src/components/Table";
import snapshotService from "@root/src/services/snapshotService";
import to from "await-to-js";
import { confirmDialog } from "@root/src/helpers/dialogs";

const SnapshotIndex = () => {
  const { t, notify, redirect } = useBaseHook();
  const tableRef = useRef(null);
  const [selectedIds, setSelectedIds] = useState([]);
  const [hiddenDeleteBtn, setHiddenDeleteBtn] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData({pageSize: 12, page: 0});
  }, []);

  const fetchData = async (values: any) => {
    let [error, result] = await to(snapshotService().withAuth().index(values));
    if (error) {
      const { code, message } = error;
      notify(t(`errors:${code}`), t(message), "error");
      return {};
    }
    setData(result.data);
    // return result;
  };

  const onChangeSelection = (data: any) => {
    if (data.length > 0) setHiddenDeleteBtn(false);
    else setHiddenDeleteBtn(true);
    setSelectedIds(data);
  };

  const onDelete = async () => {
    let [error, result]: any[] = await to(
      snapshotService().withAuth().delete({ ids: selectedIds })
    );
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordDeleted"));
    if (tableRef.current !== null) {
      tableRef.current.reload();
    }
    setSelectedIds([]);
    setHiddenDeleteBtn(true);
    return result;
  };

  const rowSelection = {
    getCheckboxProps: (record) => ({
      // disabled: record.id == auth().user.id,
      id: record.id,
    }),
  };

  return (
    <div className="content">
      <Row>
        <Col md={24} lg={12}>
          <Typography.Title level={5}>
            {t("pages:snapshots.index.title")}
          </Typography.Title>
          <Typography.Text>
            {t("pages:snapshots.index.description")}
          </Typography.Text>
        </Col>
        <Col md={24} lg={12} style={{ textAlign: "right" }}>
          <Space>
            <Button
              type="primary"
              danger
              hidden={hiddenDeleteBtn}
              onClick={() => {
                confirmDialog({
                  title: t("buttons:deleteItem"),
                  content: t("messages:message.deleteConfirm"),
                  onOk: () => onDelete(),
                });
              }}
            >
              <DeleteOutlined />
              {t("buttons:delete")}
            </Button>
          </Space>
        </Col>
      </Row>
      <br />
      <Row gutter={[8, 8]}>
        <Image.PreviewGroup>
        {data.map((img, index) => {
          return (
            <Col span={6} key={img.id+""+index}>
              <Card
                cover={
                  <Image
                    src={"/snapshot/" + img.src.replace(/^.*[\\\/]/, "")}
                    placeholder={<Image
                      preview={false}
                      src="/logo/logo.png"
                    />}
                  />
                }
                style={{height: "100%"}}
              >
                {/* <Checkbox value={img.id}></Checkbox> */}
                <span>{moment(img.created_at).format("HH:mm:ss DD/MM/YYYY")}</span>
              </Card>
            </Col>
          );
        })}
        </Image.PreviewGroup>
      </Row>
    </div>
  );
};

SnapshotIndex.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:snapshots.index.title")}
      description={t("pages:snapshots.index.description")}
      {...props}
    />
  );
};

export default SnapshotIndex;
