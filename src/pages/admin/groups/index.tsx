import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import { GridTable } from "@src/components/Table";
import FilterDatePicker from "@src/components/Table/SearchComponents/DatePicker";
import { Button, Col, Row, Space, Typography } from "antd";
import groupService from "@src/services/groupService";
import _ from "lodash";
import moment from "moment";
import to from "await-to-js";
import auth from "@src/helpers/auth";
import React, { useState, useRef } from "react";
import { confirmDialog } from "@src/helpers/dialogs";
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  PlusCircleOutlined,
  DeleteOutlined,
  LoginOutlined,
  EditOutlined,
} from "@ant-design/icons";

const Index = () => {
  const { t, notify, redirect } = useBaseHook();
  const tableRef = useRef(null);
  const [selectedIds, setSelectedIds] = useState([]);
  const [hiddenDeleteBtn, setHiddenDeleteBtn] = useState(true);

  const columns = [
    {
      title: t("pages:groups.table.name"),
      dataIndex: "name",
      key: "groups.name",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) => {
        return true ? (
          <a
            onClick={() =>
              redirect("frontend.admin.groups.edit", { id: record.id })
            }
          >
            <span className="show-on-hover">
              {record.name}
              <EditOutlined className="show-on-hover-item" />
            </span>
          </a>
        ) : (
          record.name
        );
      },
    },
    {
      title: t("pages:groups.table.numberOfDevices"),
      dataIndex: "numberOfDevices",
      key: "numberOfDevices",
      sorter: true,
      filterable: true,
      align: "right"
    },
    {
      title: t("common:province"),
      dataIndex: "province_id",
      key: "province.name",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) => record.province ? record.province.name : record.province_id
    },
    {
      title: t("common:district"),
      dataIndex: "district_id",
      key: "district.name",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) => record.district ? record.district.name : record.district_id
    },
    {
      title: t("common:ward"),
      dataIndex: "ward_id",
      key: "ward.name",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) => record.ward ? record.ward.name : record.ward_id
    },
    {
      title: t("pages:groups.table.address"),
      dataIndex: "address",
      key: "address",
      sorter: true,
      filterable: true,
    },
    {
      title: t("pages:groups.table.description"),
      dataIndex: "description",
      key: "description",
      sorter: true,
      filterable: true,
    },
    {
      title: t("common:created_at"),
      dataIndex: "created_at",
      key: "groups.created_at",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) =>
        text ? moment(text).format("HH:mm:ss, DD/MM/YYYY") : "",
      renderFilter: ({ column, confirm, ref }: FilterParam) => (
        <FilterDatePicker column={column} confirm={confirm} ref={ref} />
      ),
    },
  ];

  const onChangeSelection = (data: any) => {
    if (data.length > 0) setHiddenDeleteBtn(false);
    else setHiddenDeleteBtn(true);
    setSelectedIds(data);
  };

  const fetchData = async (values: any) => {
       let [error, result]: [any, User[]] = await to(
      groupService().withAuth().index(values)
    );
    if (error) {
      const { code, message } = error;
      notify(t(`errors:${code}`), t(message), "error");
      return {};
    }
    return result;
  };

  const onDelete = async () => {
    let [error, result]: any[] = await to(
      groupService().withAuth().delete({ ids: selectedIds })
    );
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordDeleted"));
    if (tableRef.current !== null) {
      tableRef.current.reload();
    }
    setSelectedIds([]);
    setHiddenDeleteBtn(true);
    return result;
  };

  const rowSelection = {
    getCheckboxProps: (record: Group) => ({
      disabled: record.numberOfDevices > 0,
      id: record.id,
    }),
  };

  return (
    <>
      <div className="content">
        <Row>
          <Col md={24} lg={12}>
            <Typography.Title level={5}>
              {t("pages:groups.index.title")}
            </Typography.Title>
            <Typography.Text>
              {t("pages:groups.index.description")}
            </Typography.Text>
          </Col>
          <Col md={24} lg={12} style={{ textAlign: "right" }}>
            <Space>
              <Button
                type="primary"
                danger
                hidden={hiddenDeleteBtn}
                onClick={() => {
                  confirmDialog({
                    title: t("buttons:deleteItem"),
                    content: t("messages:message.deleteConfirm"),
                    onOk: () => onDelete(),
                  });
                }}
              >
                <DeleteOutlined />
                {t("buttons:delete")}
              </Button>
              <Button
                onClick={() => redirect("frontend.admin.groups.create")}
                type="primary"
              >
                <PlusCircleOutlined />
                {t("buttons:create")}
              </Button>
            </Space>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={24}>
            <GridTable
              ref={tableRef}
              columns={columns}
              fetchData={fetchData}
              rowSelection={{
                selectedRowKeys: selectedIds,
                onChange: (data: any[]) => onChangeSelection(data),
                ...rowSelection,
              }}
              addIndexCol={false}
            />
          </Col>
        </Row>
      </div>
    </>
  );
};

Index.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:groups.index.title")}
      description={t("pages:groups.index.description")}
      {...props}
    />
  );
};


export default Index;
