import React, { useEffect, useState } from 'react'
import { Form, Input, Row, Col, Select } from 'antd';
import useBaseHook from '@src/hooks/BaseHook'
import validatorHook from '@src/hooks/ValidatorHook'
import { LockOutlined } from '@ant-design/icons';
import useSWR from 'swr';
import areaService from '@root/src/services/areaService';
import to from 'await-to-js';

const { Option } = Select

export default function GroupForm({ form, isEdit }: { form: any, isEdit: boolean }) {
  const { t, getData } = useBaseHook();
  const { validatorRePassword, CustomRegex } = validatorHook();
  const { data } = useSWR('provinces.select2', () => areaService().withAuth().province2({}));
  const provinces = data || [];
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);

  useEffect(() => {
    let currentProvince = form.getFieldValue('province_id');
    let currentDistrict = form.getFieldValue('district_id');
    if (currentProvince) onChangeProvince(currentProvince)
    if (currentDistrict) onChangeDistrict(currentDistrict)
  }, [])

  const onChangeProvince = async (value) => {
    if (!value) return setDistricts([]);
    let [err, res] = await to(areaService().withAuth().district2({province_id: value}));
    if (res) {
      return setDistricts(res)
    }
    return setDistricts([])
  }

  const onChangeDistrict = async (value) => {
    if (!value) return setWards([]);
    let [err, res] = await to(areaService().withAuth().ward2({district_id: value}));
    if (res) {
      return setWards(res)
    }
    return setWards([])
  }

  return <Row gutter={[24, 0]}>
    <Col md={24}>
      <Form.Item
        label={t("pages:groups.form.name")}
        name="name"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:groups.form.name') }) },
          // { whitespace: true, message: t('messages:form.required', { name: t('pages:groups.form.name') }) },
          { max: 100, message: t('messages:form.maxLength', { name: t('pages:groups.form.name'), length: 100 }) },
        ]}
      >
        <Input
          placeholder={t("pages:groups.form.name")}
        />
      </Form.Item>
    </Col>

    <Col md={8}>
      <Form.Item
        label={t("common:province")}
        name="province_id"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('common:province') }) },
        ]}
      >
        <Select placeholder={t("common:province")} options={provinces} allowClear showSearch filterOption optionFilterProp="label" onChange={onChangeProvince}/>
      </Form.Item>
    </Col>

    <Col md={8}>
      <Form.Item
        label={t("common:district")}
        // name="district_id"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('common:district') }) },
        ]}
        shouldUpdate={(prevValues, curValues) => prevValues.province_id !== curValues.province_id}
      >
        {
          () => {
            let province_id = form.getFieldValue('province_id')
            return (
              <Form.Item name="district_id" rules={[
                { required: true, message: t('messages:form.required', { name: t('common:district') }) },
              ]}>
                <Select placeholder={t("common:district")} options={districts}  allowClear showSearch filterOption optionFilterProp="label" disabled={province_id ? false : true} onChange={onChangeDistrict}/>
              </Form.Item>
            )
          }
        }
      </Form.Item>
    </Col>

    <Col md={8}>
      <Form.Item
        label={t("common:ward")}
        rules={[
          { required: true, message: t('messages:form.required', { name: t('common:ward') }) },
        ]}
        shouldUpdate={(prevValues, curValues) => prevValues.district_id !== curValues.district_id}
      >
        {
          () => {
            let district_id = form.getFieldValue('district_id')
            return (
              <Form.Item name="ward_id" rules={[
                { required: true, message: t('messages:form.required', { name: t('common:ward') }) },
              ]}>
                <Select placeholder={t("common:ward")} options={wards}  allowClear showSearch filterOption optionFilterProp="label" disabled={district_id ? false : true} onChange={onChangeDistrict}/>
              </Form.Item>
            )
          }
        }
      </Form.Item>
    </Col>

    <Col md={24}>
      <Form.Item
        label={t("pages:groups.form.address")}
        name="address"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:groups.form.address') }) },
          { max: 255, message: t('messages:form.maxLength', { name: t('pages:groups.form.address'), length: 255 }) }
        ]}
      >
        <Input
          placeholder={t('pages:groups.form.address')}
        />
      </Form.Item>
    </Col>
    <Col md={24}>
      <Form.Item
        label={t("pages:groups.form.description")}
        name="description"
        rules={[
          { max: 255, message: t('messages:form.maxLength', { name: t('pages:groups.form.description'), length: 255 }) }
        ]}
      >
        <Input.TextArea
          placeholder={t('pages:groups.form.description')}
          rows={3}
        />
      </Form.Item>
    </Col>
    
  </Row>
}

