import React from "react";
import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import useBaseHook from "@src/hooks/BaseHook";
import { Empty, Space, Tabs, Typography } from "antd";
import SettingZalo from "@root/src/components/Admin/Setting/Zalo";
import to from "await-to-js";
import settingService from "@root/src/services/settingService";
import Chat3LineIcon from "remixicon-react/Chat3LineIcon";
import TelegramLineIcon from "remixicon-react/TelegramLineIcon";
import SkypeLineIcon from "remixicon-react/SkypeLineIcon";
import ToolsLineIcon from "remixicon-react/ToolsLineIcon";
import SettingMain from "@root/src/components/Admin/Setting/Main";
import MailLineIcon from "remixicon-react/MailLineIcon";
import DiscussLineIcon from "remixicon-react/DiscussLineIcon";
import SettingMessage from "@root/src/components/Admin/Setting/Message";
import SettingEmail from "@root/src/components/Admin/Setting/Email";
import SettingTelegram from "@root/src/components/Admin/Setting/Telegram";

const { TabPane } = Tabs;

export default function SettingIndex({ zalo }: { zalo: Setting }) {
  const { t, notify, redirect } = useBaseHook();

  return (
    <div className="content">
      <Tabs defaultActiveKey="main">
        <TabPane
          tab={
            <>
              <Space>
                <ToolsLineIcon />
                <Typography.Title level={5}>
                  Cài đặt chung
                </Typography.Title>
              </Space>
            </>
          }
          key="main"
        >
          <SettingMain />
        </TabPane>
        <TabPane
          tab={
            <>
              <Space>
                <DiscussLineIcon />
                <Typography.Title level={5}>
                  Tin nhắn cảnh báo
                </Typography.Title>
              </Space>
            </>
          }
          key="message"
          disabled={false}
        >
          <SettingMessage />
        </TabPane>
        <TabPane
          tab={
            <>
              <Space>
                <MailLineIcon />
                <Typography.Title level={5}>
                  Email
                </Typography.Title>
              </Space>
            </>
          }
          key="email"
          disabled={false}
        >
          <SettingEmail />
        </TabPane>
        <TabPane
          tab={
            <>
              <Space>
                <Chat3LineIcon />
                <Typography.Title level={5}>
                  Zalo
                </Typography.Title>
              </Space>
            </>
          }
          key="zalo"
          disabled={false}
        >
          <SettingZalo />
        </TabPane>
        <TabPane
          tab={
            <>
              <Space>
                <TelegramLineIcon />
                <Typography.Title level={5}>
                  Telegram
                </Typography.Title>
              </Space>
            </>
          }
          key="telegram"
          disabled={false}
        >
          <SettingTelegram />
        </TabPane>
        <TabPane
          tab={
            <>
              <Space>
                <SkypeLineIcon />
                <Typography.Title level={5}>
                  Skype
                </Typography.Title>
              </Space>
            </>
          }
          key="skype"
          disabled={false}
        >
          <Empty />
        </TabPane>
      </Tabs>
    </div>
  );
}

SettingIndex.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:settings.index.title")}
      description={t("pages:settings.index.description")}
      {...props}
    />
  );
};

SettingIndex.getInitialProps = async (ctx: any) => {
  let [err, zalo] = await to(settingService().withAuth(ctx).getZalo({}));
  if (err) {
    console.log("SettingIndex.getInitialProps", err);
  }
  return {
    zalo: zalo || {},
  };
};
