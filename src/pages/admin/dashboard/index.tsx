import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import { Row, Col, Typography, Dropdown, Menu, Button, Space } from "antd";
import React, { useEffect, useRef, useState } from "react";
import { DownOutlined, DeleteOutlined } from "@ant-design/icons";
import moment from "moment";
import { FilterDatePicker, GridTable } from "@root/src/components/Table";
import deviceService from "@root/src/services/deviceService";
import to from "await-to-js";
import { confirmDialog } from "@root/src/helpers/dialogs";

const Dashboard = () => {
  const { t, notify, redirect } = useBaseHook();
  const tableRef = useRef(null);
  const [selectedIds, setSelectedIds] = useState([]);
  const [hiddenDeleteBtn, setHiddenDeleteBtn] = useState(true);

  const fetchData = async (values: any) => {
    let [error, result]: [any, User[]] = await to(
      deviceService().withAuth().indexLog(values)
    );
    if (error) {
      const { code, message } = error;
      notify(t(`errors:${code}`), t(message), "error");
      return {};
    }
    return result;
  };

  const columns = [
    {
      title: t("pages:devices.log.device"),
      dataIndex: "device_id",
      key: "device.serialNumber",
      sorter: true,
      filterable: true,
      render: (text: string, record) =>
        record.device ? record.device.serialNumber : record.device_id,
    },
    {
      title: t("pages:devices.log.topic"),
      dataIndex: "topic",
      key: "topic",
      sorter: true,
      filterable: true,
    },
    {
      title: t("pages:devices.log.payload"),
      dataIndex: "payload",
      key: "payload",
      sorter: true,
      filterable: true,
    },
    {
      title: t("common:created_at"),
      dataIndex: "created_at",
      key: "created_at",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) =>
        text ? moment(text).format("HH:mm:ss, DD/MM/YYYY") : "",
      renderFilter: ({ column, confirm, ref }: FilterParam) => (
        <FilterDatePicker column={column} confirm={confirm} ref={ref} />
      ),
    },
  ];

  const onChangeSelection = (data: any) => {
    if (data.length > 0) setHiddenDeleteBtn(false);
    else setHiddenDeleteBtn(true);
    setSelectedIds(data);
  };

  const onDelete = async () => {
    let [error, result]: any[] = await to(
      deviceService().withAuth().delete({ ids: selectedIds })
    );
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordDeleted"));
    if (tableRef.current !== null) {
      tableRef.current.reload();
    }
    setSelectedIds([]);
    setHiddenDeleteBtn(true);
    return result;
  };

  const rowSelection = {
    getCheckboxProps: (record) => ({
      // disabled: record.id == auth().user.id,
      id: record.id,
    }),
  };
  
  return (
    <div className="content">
      <Row>
        <Col span={24} style={{ textAlign: "right" }}>
          <Space>
            <Button
              type="primary"
              danger
              hidden={hiddenDeleteBtn}
              onClick={() => {
                confirmDialog({
                  title: t("buttons:deleteItem"),
                  content: t("messages:message.deleteConfirm"),
                  onOk: () => onDelete(),
                });
              }}
            >
              <DeleteOutlined />
              {t("buttons:delete")}
            </Button>
          </Space>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <GridTable
            ref={tableRef}
            columns={columns}
            fetchData={fetchData}
            rowSelection={{
              selectedRowKeys: selectedIds,
              onChange: (data: any[]) => onChangeSelection(data),
              ...rowSelection,
            }}
            addIndexCol={false}
          />
        </Col>
      </Row>
    </div>
  );
};

Dashboard.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:dashboard.index.title")}
      description={t("pages:dashboard.index.description")}
      {...props}
    />
  );
};

export default Dashboard;
