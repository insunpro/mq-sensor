import React from 'react'
import { Form, Input, Row, Col, Select } from 'antd';
import useBaseHook from '@src/hooks/BaseHook'
import validatorHook from '@src/hooks/ValidatorHook'
import { LockOutlined } from '@ant-design/icons';
import useSWR from 'swr';
import groupService from '@root/src/services/groupService';

const { Option } = Select

export default function GroupForm({ form, isEdit }: { form: any, isEdit: boolean }) {
  const { t, getData } = useBaseHook();
  const { validatorRePassword, CustomRegex } = validatorHook();
  const { data } = useSWR('groups.select2', () => groupService().withAuth().select2({}));
  const groups = data || []

  return <Row gutter={[24, 0]}>
    <Col md={24}>
      <Form.Item
        label={t("pages:devices.form.serialNumber")}
        name="serialNumber"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:devices.form.serialNumber') }) },
          // { whitespace: true, message: t('messages:form.required', { name: t('pages:devices.form.serialNumber') }) },
          { max: 100, message: t('messages:form.maxLength', { name: t('pages:devices.form.serialNumber'), length: 100 }) },
        ]}
      >
        <Input
          placeholder={t("pages:devices.form.serialNumber")}
          readOnly={isEdit ? true: false}
        />
      </Form.Item>
    </Col>

    <Col md={24}>
      <Form.Item
        label={t("pages:devices.form.name")}
        name="name"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:devices.form.name') }) },
          // { whitespace: true, message: t('messages:form.required', { name: t('pages:devices.form.name') }) },
          { max: 100, message: t('messages:form.maxLength', { name: t('pages:devices.form.name'), length: 100 }) },
        ]}
      >
        <Input
          placeholder={t("pages:devices.form.name")}
        />
      </Form.Item>
    </Col>

    {/* <Col md={24}>
      <Form.Item
        label={t("pages:users.form.role")}
        name="role"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:users.form.role') }) },
        ]}
      >
        <Select placeholder={t("pages:users.form.role")} allowClear showSearch>
          {Object.keys(roles).map((key: any) => (
            <Option value={roles[key]} key={key}>{key}</Option>
          ))}
        </Select>
      </Form.Item>
    </Col> */}
    <Col md={24}>
      <Form.Item
        label={t("pages:devices.form.lng")}
        name="lng"
        rules={[
          // { required: true, message: t('messages:form.required', { name: t('pages:devices.form.lng') }) },
          { max: 255, message: t('messages:form.maxLength', { name: t('pages:devices.form.lng'), length: 255 }) }
        ]}
      >
        <Input
          placeholder={t('pages:devices.form.lng')}
        />
      </Form.Item>
    </Col>
    <Col md={24}>
      <Form.Item
        label={t("pages:devices.form.lat")}
        name="lat"
        rules={[
          // { required: true, message: t('messages:form.required', { name: t('pages:devices.form.lat') }) },
          { max: 255, message: t('messages:form.maxLength', { name: t('pages:devices.form.lat'), length: 255 }) }
        ]}
      >
        <Input
          placeholder={t('pages:devices.form.lat')}
        />
      </Form.Item>
    </Col>
    <Col md={24}>
      <Form.Item
        label={t("pages:devices.form.group_id")}
        name="group_id"
        rules={[
          // { required: true, message: t('messages:form.required', { name: t('pages:devices.form.group_id') }) },
        ]}
      >
        <Select placeholder={t("pages:devices.form.group_id")}>
          {groups.map((group: any) => (
            <Option value={group.value} key={group.value}>{group.label}</Option>
          ))}
        </Select>
      </Form.Item>
    </Col>
  </Row>
}

