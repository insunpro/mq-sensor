import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import { GridTable } from "@src/components/Table";
import FilterDatePicker from "@src/components/Table/SearchComponents/DatePicker";
import {
  Badge,
  Button,
  Col,
  Progress,
  Row,
  Space,
  Switch,
  Table,
  Tooltip,
  Typography,
} from "antd";
import deviceService from "@src/services/deviceService";
import _ from "lodash";
import moment from "moment";
import to from "await-to-js";
import auth from "@src/helpers/auth";
import React, { useState, useRef } from "react";
import { confirmDialog } from "@src/helpers/dialogs";
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  PlusCircleOutlined,
  DeleteOutlined,
  LoginOutlined,
  EditOutlined,
  RetweetOutlined,
} from "@ant-design/icons";
import SensorTable from "@root/src/components/Admin/Device/SensorTable";
import OnOffButton from "@root/src/components/Admin/Device/OnOffButton";

const Index = () => {
  const { t, notify, redirect } = useBaseHook();
  const tableRef = useRef(null);
  const [selectedIds, setSelectedIds] = useState([]);
  const [hiddenDeleteBtn, setHiddenDeleteBtn] = useState(true);
  const [forceReload, setForceReload] = useState(false);
  // const [loading, setLoading] = useState(false);

  const columns = [
    {
      title: t("pages:devices.table.serialNumber"),
      dataIndex: "serialNumber",
      key: "devices.serialNumber",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) => {
        return true ? (
          <a
            onClick={() =>
              redirect("frontend.admin.devices.edit", { id: record.id })
            }
          >
            <span className="show-on-hover">
              {record.serialNumber}
              <EditOutlined className="show-on-hover-item" />
            </span>
          </a>
        ) : (
          record.serialNumber
        );
      },
    },
    {
      title: t("pages:devices.table.name"),
      dataIndex: "name",
      key: "name",
      sorter: true,
      filterable: true,
    },
    {
      title: t("pages:devices.table.group_id"),
      dataIndex: "group_id",
      key: "group_id",
      sorter: true,
      filterable: true,
      render: (text: string, record) =>
        record.group ? record.group.name : record.group_id,
    },
    {
      title: t("pages:devices.table.lng"),
      dataIndex: "lng",
      key: "lng",
      sorter: true,
      filterable: true,
    },
    {
      title: t("pages:devices.table.lat"),
      dataIndex: "lat",
      key: "lat",
      sorter: true,
      filterable: true,
    },
    {
      title: t("common:status"),
      dataIndex: "status",
      key: "status",
      sorter: true,
      filterable: true,
      align: "center",
      render: (text: string, record) => {
        let status = record.status === 1 ? "processing" : "offline";
        return (
          <Tooltip
            title={record.status === 1 ? "Đang hoạt động" : "Dừng hoạt động"}
          >
            {/* <Badge status={record.status === 1 ? "processing" : "default"} /> */}
            <span className={`status status-${status} status-big`}></span>
          </Tooltip>
        );
      },
    },
    {
      title: "Hòa mạng",
      key: "modes->config",
      align: "center",
      render: (text: string, record) => {
        return (
          <Tooltip title={"Chế độ hòa mạng"}>
            <OnOffButton
              defaultChecked={record.modes?.config === 1}
              onChange={async (checked, event): Promise<boolean> =>
                await onChangeMode(record, "config", checked)
              }
              disabled={record.status !== 1}
            />
          </Tooltip>
        );
      },
    },
    // {
    //   title: t("common:created_at"),
    //   dataIndex: "created_at",
    //   key: "devices.created_at",
    //   sorter: true,
    //   filterable: true,
    //   render: (text: string, record: any) =>
    //     text ? moment(text).format("HH:mm:ss, DD/MM/YYYY") : "",
    //   renderFilter: ({ column, confirm, ref }: FilterParam) => (
    //     <FilterDatePicker column={column} confirm={confirm} ref={ref} />
    //   ),
    // },
    {
      title: t("common:updated_at"),
      dataIndex: "updated_at",
      key: "devices.updated_at",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) =>
        text ? moment(text).format("HH:mm:ss, DD/MM/YYYY") : "",
      renderFilter: ({ column, confirm, ref }: FilterParam) => (
        <FilterDatePicker column={column} confirm={confirm} ref={ref} />
      ),
    },
  ];

  const onChangeSelection = (data: any) => {
    if (data.length > 0) setHiddenDeleteBtn(false);
    else setHiddenDeleteBtn(true);
    setSelectedIds(data);
  };

  const fetchData = async (values: any) => {
    let [error, result]: [any, User[]] = await to(
      deviceService().withAuth().index(values)
    );
    if (error) {
      const { code, message } = error;
      notify(t(`errors:${code}`), t(message), "error");
      return {};
    }
    return result;
  };

  const onDelete = async () => {
    let [error, result]: any[] = await to(
      deviceService().withAuth().delete({ ids: selectedIds })
    );
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordDeleted"));
    if (tableRef.current !== null) {
      tableRef.current.reload();
    }
    setSelectedIds([]);
    setHiddenDeleteBtn(true);
    return result;
  };

  const rowSelection = {
    getCheckboxProps: (record) => ({
      disabled: record.id == auth().user.id,
      id: record.id,
    }),
  };

  const expandedRowRender = (record, index) => {
    return (
      <SensorTable
        key={record.id + index}
        record={record}
        onChange={() => tableRef.current?.reload()}
        forceReload={forceReload}
      />
    );
  };

  const onChangeMode = async (record, mode, value) => {
    // setLoading(true);
    let [err, result] = await to(
      deviceService()
        .withAuth()
        .changeMode({ id: record.id, mode, value: value ? 1 : 0 })
    );
    if (err) {
      notify(t(`errors:${err.code}`), "", "error");
      tableRef.current?.reload();
      return false;
    }
    notify(t("messages:message.recordUpdated"));
    tableRef.current?.reload();
    return true;
  };

  return (
    <div className="content">
      <Row>
        <Col md={24} lg={12}>
          <Typography.Title level={5}>
            {t("pages:devices.index.title")}
          </Typography.Title>
          <Typography.Text>
            {t("pages:devices.index.description")}
          </Typography.Text>
        </Col>
        <Col md={24} lg={12} style={{ textAlign: "right" }}>
          <Space>
            <Button
              type="primary"
              danger
              hidden={hiddenDeleteBtn}
              onClick={() => {
                confirmDialog({
                  title: t("buttons:deleteItem"),
                  content: t("messages:message.deleteConfirm"),
                  onOk: () => onDelete(),
                });
              }}
            >
              <DeleteOutlined />
              {t("common:delete")}
            </Button>
            <Button
              onClick={() => {
                tableRef.current?.reload();
                setForceReload(!forceReload);
              }}
              type="default"
            >
              <RetweetOutlined />
              {t("common:refresh")}
            </Button>
          </Space>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <GridTable
            ref={tableRef}
            columns={columns}
            fetchData={fetchData}
            rowSelection={{
              selectedRowKeys: selectedIds,
              onChange: (data: any[]) => onChangeSelection(data),
              ...rowSelection,
            }}
            addIndexCol={false}
            expandable={{ expandedRowRender }}
          />
        </Col>
      </Row>
    </div>
  );
};

Index.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:devices.index.title")}
      description={t("pages:devices.index.description")}
      {...props}
    />
  );
};

export default Index;
