import React, { useState } from "react";
import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import {
  Button,
  Form,
  Col,
  Row,
  Space,
  Spin,
  Empty,
  Table,
  Typography,
  Tooltip,
} from "antd";
import cameraService from "@src/services/cameraService";
import to from "await-to-js";
import useBaseHook from "@src/hooks/BaseHook";
import { LeftOutlined, SearchOutlined } from "@ant-design/icons";
import ConnectModal from "@root/src/components/Admin/Camera/ConnectModal";
import LinksFillIcon from "remixicon-react/LinksFillIcon";

const Search = () => {
  const { t, notify, router } = useBaseHook();
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [modalData, setModalData] = useState(null);
  const [data, setData] = useState([]);

  const fetchData = async () => {
    setLoading(true);
    let [err, res] = await to(cameraService().withAuth().onvifSearch({}));
    setLoading(false);
    if (err) return console.error("Search.fetchData.err", err);
    setData(res);
  };

  const columns: any = [
    {
      key: "host",
      title: "Địa chỉ",
      dataIndex: "host",
      render: (text: string, record: any) => text || "-",
    },
    {
      key: "name",
      title: "Tên thiết bị",
      dataIndex: "name",
      render: (text: string, record: any) => text || "-",
    },
    {
      key: "hardware",
      title: "Loại thiết bị",
      dataIndex: "hardware",
      render: (text: string, record: any) => text || "-",
    },
    {
      key: "task",
      align: "center",
      render: (text: string, record: any) => {
        return (
          <Tooltip
            title={
              record.existed ? "Thiết bị đã kết nối" : "Kết nối với thiết bị"
            }
          >
            <Button
              type="primary"
              icon={<LinksFillIcon />}
              disabled={record.existed}
              onClick={() => {
                setModalData(record);
                setVisible(true);
              }}
            ></Button>
          </Tooltip>
        );
      },
    },
  ];

  return (
    <>
      <div className="content">
        <Row>
          <Col md={24} lg={12}>
            <Typography.Title level={5}>
              {t("pages:cameras.search.title")}
            </Typography.Title>
            <Typography.Text>
              {t("pages:cameras.search.description")}
            </Typography.Text>
          </Col>
          <Col md={24} lg={12} style={{ textAlign: "right" }}>
            <Space>
              <Button
                onClick={() => fetchData()}
                type="primary"
                loading={loading}
              >
                <SearchOutlined />
                Tìm kiếm
              </Button>
              <Button onClick={() => router.back()}>
                <LeftOutlined />
                {t("common:back")}
              </Button>
            </Space>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={24}>
            <Table
              columns={columns}
              dataSource={data}
              loading={loading}
              rowKey="urn"
              bordered
            />
            {modalData ? (
              <ConnectModal
                visible={visible}
                data={modalData}
                onClose={() => setVisible(false)}
                onSuccess={fetchData}
              />
            ) : (
              ""
            )}
          </Col>
        </Row>
      </div>
    </>
  );
};

Search.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:cameras.search.title")}
      description={t("pages:cameras.search.description")}
      {...props}
    />
  );
};

// Search.permissions = {
//   "admins": "C"
// }

export default Search;
