import dynamic from "next/dynamic";
const Layout = dynamic(() => import("@src/layouts/Admin"), { ssr: false });
import { GridTable } from "@src/components/Table";
import FilterDatePicker from "@src/components/Table/SearchComponents/DatePicker";
import { Button, Col, Row, Space, Tooltip, Typography } from "antd";
import cameraService from "@src/services/cameraService";
import _ from "lodash";
import moment from "moment";
import to from "await-to-js";
import auth from "@src/helpers/auth";
import React, { useState, useRef } from "react";
import { confirmDialog } from "@src/helpers/dialogs";
import useBaseHook from "@src/hooks/BaseHook";
import usePermissionHook from "@src/hooks/PermissionHook";
import {
  PlusOutlined,
  DeleteOutlined,
  SearchOutlined,
  EditOutlined,
  CheckCircleOutlined,
  CloseCircleOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import ApiModal from "@root/src/components/Admin/Camera/ApiModal";
import LiveLineIcon from "remixicon-react/LiveLineIcon";
import CameraLineIcon from "remixicon-react/CameraLineIcon";
import CameraSwitchLineIcon from "remixicon-react/CameraSwitchLineIcon";
import StreamModal from "@root/src/components/Admin/Camera/StreamModal";
import SnapshotModal from "@root/src/components/Admin/Camera/SnapshotModal";

const Index = () => {
  const { t, notify, redirect } = useBaseHook();
  const tableRef = useRef(null);
  const [selectedIds, setSelectedIds] = useState([]);
  const [hiddenDeleteBtn, setHiddenDeleteBtn] = useState(true);
  const [visible, setVisible] = useState(false);
  const [streamVisible, setStreamVisible] = useState(false);
  const [cid, setCid] = useState(null);

  const columns = [
    {
      title: t("pages:cameras.table.name"),
      dataIndex: "name",
      key: "name",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) => {
        return true ? (
          <a
            onClick={() =>
              redirect("frontend.admin.cameras.edit", { id: record.id })
            }
          >
            <span className="show-on-hover">
              {record.name + " "}
              <EditOutlined className="show-on-hover-item" />
            </span>
          </a>
        ) : (
          record.name
        );
      },
    },
    {
      title: t("pages:cameras.table.numberOfSensors"),
      dataIndex: "numberOfSensors",
      key: "numberOfSensors",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) => text || 0,
    },
    {
      title: t("pages:cameras.table.info"),
      dataIndex: "info",
      key: "info",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) => {
        // let v = _.get(record.info, "verticalAngle", "-");
        // let h = _.get(record.info, "horizontalAngle", "-");
        // let z = _.get(record.info, "zoom", "-");
        let resolutions = [];
        if (record.profiles && record.profiles.length) {
          record.profiles.map(profile => {
            let w = _.get(profile, 'video.encoder.resolution.width');
            let h = _.get(profile, 'video.encoder.resolution.height');
            resolutions.push(w+"x"+h)
          })
        }

        return resolutions.join(", ")
      },
    },
    {
      title: t("common:updated_at"),
      dataIndex: "updated_at",
      key: "updated_at",
      sorter: true,
      filterable: true,
      render: (text: string, record: any) =>
        text ? moment(text).format("HH:mm:ss, DD/MM/YYYY") : "-",
      renderFilter: ({ column, confirm, ref }: FilterParam) => (
        <FilterDatePicker column={column} confirm={confirm} ref={ref} />
      ),
    },
    // {
    //   title: t("pages:cameras.table.api"),
    //   dataIndex: "api",
    //   key: "api",
    //   align: "center",
    //   render: (text: string, record: any) => {
    //     return (
    //       <Button
    //         type="default"
    //         icon={<SettingOutlined />}
    //         onClick={() => {
    //           setCid(record.id);
    //           setVisible(true);
    //         }}
    //       />
    //     );
    //   },
    // },
    {
      title: t("common:task"),
      align: "center",
      render: (text: string, record: any) => {
        return (
          <Space>
            {/* <Tooltip title="Streaming">
              <Button
                icon={<LiveLineIcon />}
                onClick={() => {
                  setCid(record.id);
                  setStreamVisible(true);
                }}
              ></Button>
            </Tooltip> */}
            <Tooltip title="Trực tiếp">
              <Button
                icon={<CameraSwitchLineIcon />}
                onClick={() => {
                  setCid(record.id);
                  setVisible(true);
                }}
              ></Button>
            </Tooltip>
          </Space>
        );
      },
    },
  ];

  const onChangeSelection = (data: any) => {
    if (data.length > 0) setHiddenDeleteBtn(false);
    else setHiddenDeleteBtn(true);
    setSelectedIds(data);
  };

  const fetchData = async (values: any) => {
    let [error, result]: [any, User[]] = await to(
      cameraService().withAuth().index(values)
    );
    if (error) {
      const { code, message } = error;
      notify(t(`errors:${code}`), t(message), "error");
      return {};
    }
    return result;
  };

  const onDelete = async () => {
    let [error, result]: any[] = await to(
      cameraService().withAuth().delete({ ids: selectedIds })
    );
    if (error) return notify(t(`errors:${error.code}`), "", "error");
    notify(t("messages:message.recordDeleted"));
    if (tableRef.current !== null) {
      tableRef.current.reload();
    }
    setSelectedIds([]);
    setHiddenDeleteBtn(true);
    return result;
  };

  const rowSelection = {
    getCheckboxProps: (record: Group) => ({
      disabled: record.numberOfDevices > 0,
      id: record.id,
    }),
  };

  return (
    <>
      <div className="content">
        <Row>
          <Col md={24} lg={12}>
            <Typography.Title level={5}>
              {t("pages:cameras.index.title")}
            </Typography.Title>
            <Typography.Text>
              {t("pages:cameras.index.description")}
            </Typography.Text>
          </Col>
          <Col md={24} lg={12} style={{ textAlign: "right" }}>
            <Space>
              <Button
                type="primary"
                danger
                hidden={hiddenDeleteBtn}
                onClick={() => {
                  confirmDialog({
                    title: t("buttons:deleteItem"),
                    content: t("messages:message.deleteConfirm"),
                    onOk: () => onDelete(),
                  });
                }}
              >
                <DeleteOutlined />
                {t("buttons:delete")}
              </Button>
              <Button
                onClick={() => redirect("frontend.admin.cameras.create")}
                type="primary"
              >
                <PlusOutlined />
                {t("common:create")}
              </Button>
              <Button
                onClick={() => redirect("frontend.admin.cameras.search")}
                type="primary"
              >
                <SearchOutlined />
                Tìm kiếm
              </Button>
            </Space>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={24}>
            <GridTable
              ref={tableRef}
              columns={columns}
              fetchData={fetchData}
              rowSelection={{
                selectedRowKeys: selectedIds,
                onChange: (data: any[]) => onChangeSelection(data),
                ...rowSelection,
              }}
              addIndexCol={false}
            />
          </Col>
        </Row>
        {cid && (
          <StreamModal
            visible={visible}
            cid={cid}
            onClose={() => setVisible(false)}
          />
        )}
        {/* {cid && (
          <SnapshotModal
            visible={visible}
            cid={cid}
            onClose={() => setVisible(false)}
          />
        )} */}
      </div>
    </>
  );
};

Index.Layout = (props) => {
  const { t } = useBaseHook();
  return (
    <Layout
      title={t("pages:cameras.index.title")}
      description={t("pages:cameras.index.description")}
      {...props}
    />
  );
};

export default Index;
