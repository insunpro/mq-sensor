import React, { useEffect, useState } from 'react'
import { Form, Input, Row, Col, Select, InputNumber, Checkbox } from 'antd';
import useBaseHook from '@src/hooks/BaseHook'
import validatorHook from '@src/hooks/ValidatorHook'
import { LockOutlined } from '@ant-design/icons';
import useSWR from 'swr';
import areaService from '@root/src/services/areaService';
import to from 'await-to-js';

const { Option } = Select

export default function CameraForm({ form, isEdit }: { form: any, isEdit: boolean }) {
  const { t, getData } = useBaseHook();

  useEffect(() => {
  }, [])

  const changeOption = (value) => {
    console.log(value)
  }


  return <Row gutter={[24, 0]}>
    <Col md={24}>
      <Form.Item
        label={t("pages:cameras.table.name")}
        name="name"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:cameras.table.name') }) },
          // { whitespace: true, message: t('messages:form.required', { name: t('pages:cameras.table.name') }) },
          { max: 100, message: t('messages:form.maxLength', { name: t('pages:cameras.table.name'), length: 100 }) },
        ]}
      >
        <Input
          placeholder={t("pages:cameras.table.name")}
        />
      </Form.Item>
    </Col>

    <Col md={24}>
      <Form.Item 
        label={t("pages:cameras.table.host")}
      name="host" rules={[
        { required: true, message: t('messages:form.required', { name: t('pages:cameras.table.host') }) },
        { max: 64, message: t('messages:form.maxLength', { name: t('pages:cameras.table.host'), length: 64 }) }
      ]}>
        <Input
          placeholder={t('pages:cameras.table.host')}
        />
      </Form.Item>
    </Col>

    {/* <Col md={12}>
      <Form.Item 
        label={t("pages:cameras.table.port")}
      name="port" rules={[
        { required: true, message: t('messages:form.required', { name: t('pages:cameras.table.port') }) },
      ]}>
        <InputNumber
          placeholder={"0 - 99999"}
          min={0}
          max={99999}
          style={{width: "100%"}}
        />
      </Form.Item>
    </Col> */}

    <Col md={12}>
      <Form.Item 
        label={t("pages:cameras.table.username")}
        name="username" 
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:cameras.table.username') }) },
          { max: 64, message: t('messages:form.maxLength', { name: t('pages:cameras.table.username'), length: 64 }) }
        ]}
      >
        <Input
          placeholder={t('pages:cameras.table.username')}
        />
      </Form.Item>
    </Col>

    <Col md={12}>
      <Form.Item
        label={t("pages:cameras.table.password")}
        name="password"
        rules={[
          { required: true, message: t('messages:form.required', { name: t('pages:cameras.table.password') }) },
          { max: 64, message: t('messages:form.maxLength', { name: t('pages:cameras.table.password'), length: 64 }) }
        ]}
      >
        <Input
          placeholder={t('pages:cameras.table.password')}
        />
      </Form.Item>
    </Col>
    <Col md={24}>
      <Form.Item
        label={t("pages:cameras.table.options")}
        name={["info", "options"]}
      >
        <Checkbox.Group
          onChange={(changeOption)}
          style={{width: "100%"}}
        >
          <Row>
            <Col span={8}>
              <Checkbox value="rtsp">Ptz</Checkbox>
            </Col>
            <Col span={8}>
              <Checkbox value="ptz">Rtsp</Checkbox>
            </Col>
            <Col span={8}>
              <Checkbox value="snapshot">Snapshot</Checkbox>
            </Col>
          </Row>
        </Checkbox.Group>
      </Form.Item>
    </Col>
    <Col span={8}>
      <Form.Item
        label={t("pages:cameras.table.horizontalAngle")}
        name={["info", "horizontalAngle"]}
      >
        <InputNumber style={{width: "100%"}} min={1} max={360} placeholder="1° - 360°"/>
      </Form.Item>
    </Col>
    <Col span={8}>
      <Form.Item
        label={t("pages:cameras.table.verticalAngle")}
        name={["info", "verticalAngle"]}
      >
        <InputNumber style={{width: "100%"}} min={1} max={105} placeholder="1° - 105°"/>
      </Form.Item>
    </Col>
    <Col span={8}>
      <Form.Item
        label={t("pages:cameras.table.zoom")}
        name={["info", "zoom"]}
      >
        <InputNumber style={{width: "100%"}} min={1} max={128} placeholder="1% - 128%"/>
      </Form.Item>
    </Col>
  </Row>
}

