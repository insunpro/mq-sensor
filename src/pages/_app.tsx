import React from 'react'
import App from 'next/app'
import I18n from '@core/I18n'
import wrapper from '@src/components/Redux'
// import '@src/less/base/custom-ant-theme.less';
// import '@src/less/base/vars.less';
import '@src/less/themes/default.less'
// import '@src/less/login.less'
// import '@src/less/admin.less'
// import '@src/less/home.less'
// import '@src/less/homeIndex.less'
// import '@src/less/user.less'

// import '@ngochipx/nhfinder/reactjs/dist/main.css'

import Router from 'next/router';
import NProgress from 'nprogress'; //nprogress module
import 'nprogress/nprogress.css'; //styles of nprogress
import viVn from "antd/lib/locale/vi_VN";
import { ConfigProvider } from "antd";
//Binding events.
Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done(true));
Router.events.on("routeChangeError", () => NProgress.done());

const DefaultComponent = (props: any) => {
  return <>{props.children}</>;
};
class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    let Layout = Component["Layout"] ? Component["Layout"] : DefaultComponent;
    const permissions = Component["permissions"] || {};
    return (
      <Layout permissions={permissions}>
        <ConfigProvider locale={viVn}>
          <Component {...pageProps} />
        </ConfigProvider>
      </Layout>
    );
  }
}

export default wrapper.withRedux(I18n.appWithTranslation(MyApp));
