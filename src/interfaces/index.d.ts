interface User {
  id: number;
  username: string;
  phone: string;
  name: string;
  email: string;
  permissions?: any;
  role_id: number;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}

interface Group {
  id: number;
  name: string;
  description: string;
  address: string;
  province_id: number;
  district_id: number;
  ward_id: number;
  numberOfDevices: number;
}

interface Device {
  id: number;
  serialNumber: string;
  name: string;
  lng: string;
  lat: string;
  group_id: number;
  sensors?: Sensor[];
}

interface Sensor {
  id: number;
  sensorId: number;
  battery: number;
  status: number;
  lng: string;
  lat: string;
  description: string;
  updated_at: Date;
}

interface Setting {
  key: string;
  value: any;
}

interface Camera {
  id: number;
  name: string;
  host: string;
  port: number;
  username: string;
  password: string;
  info: any;
  updated_at: Date;
}