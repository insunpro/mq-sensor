import Base from "./baseService";

class SettingService extends Base {
  getMain = async (filter: any) => {
    return this.request({
      url: "/api/v1/settings/main",
      method: "GET",
      data: filter,
    });
  };

  editMain = async (filter: any) => {
    return this.request({
      url: "/api/v1/settings/main",
      method: "PUT",
      data: filter,
    });
  };

  getEmail = async (filter: any) => {
    return this.request({
      url: "/api/v1/settings/email",
      method: "GET",
      data: filter,
    });
  };

  editEmail = async (filter: any) => {
    return this.request({
      url: "/api/v1/settings/email",
      method: "PUT",
      data: filter,
    });
  };

  getZalo = async (filter: any) => {
    return this.request({
      url: "/api/v1/settings/zalo",
      method: "GET",
      data: filter,
    });
  };

  editZalo = async (filter: any) => {
    return this.request({
      url: "/api/v1/settings/zalo",
      method: "PUT",
      data: filter,
    });
  };

  getTelegram = async (filter: any) => {
    return this.request({
      url: "/api/v1/settings/telegram",
      method: "GET",
      data: filter,
    });
  };

  editTelegram = async (filter: any) => {
    return this.request({
      url: "/api/v1/settings/telegram",
      method: "PUT",
      data: filter,
    });
  };
}

export default function SettingServiceFn() {
  return new SettingService();
}
