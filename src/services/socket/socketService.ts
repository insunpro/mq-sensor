import { io } from "socket.io-client";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

export default class SocketService {
  cid: number;
  host: string;
  io;
  connected = false;

  constructor(cid: number) {
    this.cid = cid;
    this.host = publicRuntimeConfig.API_HOST;
    this.io = io(publicRuntimeConfig.API_HOST, {
      withCredentials: false,
    });
    this.init(this.io);
  }

  init(socket) {
    socket.on("connect", () => {
      this.connected=true;
      console.log("socket connected: ", this.cid);
    });
    socket.on("disconnect", (reason) => {
      this.connected=false;
      console.log("socket disconnected: ", this.cid, reason);
    });
    
  }

  emit(event, data) {
      this.io.emit(event, data)
  }

  getStream(cb) {
    this.io.emit("getStream", { cid: this.cid })
    this.io.on("cameraStream", (data) => {
      cb(data)
    })
  }

  ptzGotoHome() {
    this.io.emit("ptzGotoHome", { cid: this.cid })
  }

  ptzMove(direction, speed) {
    this.io.emit("ptzMove", { cid: this.cid, direction, speed })
  }

  ptzStop() {
    this.io.emit("ptzStop", { cid: this.cid })
  }

  close() {
    this.io.disconnect();
  }
}
