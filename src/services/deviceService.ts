import Base from "./baseService";

class DeviceService extends Base {
  index = async (filter: any) => {
    return this.request({
      url: "/api/v1/devices",
      method: "GET",
      data: filter,
    });
  };

  select2 = async (filter: any) => {
    return this.request({
      url: "/api/v1/devices/select2",
      method: "GET",
      data: filter,
    });
  };

  create = async (data: any) => {
    return this.request({
      url: "/api/v1/devices",
      method: "POST",
      data: data,
    });
  };

  detail = async (data: any) => {
    return this.request({
      url: "/api/v1/devices/:id",
      method: "GET",
      data: data,
    });
  };

  edit = async (data: any) => {
    return this.request({
      url: "/api/v1/devices/:id",
      method: "PUT",
      data: data,
    });
  };

  delete = async (data: any) => {
    return this.request({
      url: "/api/v1/devices",
      method: "DELETE",
      data: data,
    });
  };

  destroy = async (data: any) => {
    return this.request({
      url: "/api/v1/devices/:id",
      method: "DELETE",
      data: data,
    });
  };

  indexLog = async (filter: any) => {
    return this.request({
      url: "/api/v1/device-logs",
      method: "GET",
      data: filter,
    });
  };

  detailLog = async (data: any) => {
    return this.request({
      url: "/api/v1/device-logs/:id",
      method: "GET",
      data: data,
    });
  };

  deleteLog = async (data: any) => {
    return this.request({
      url: "/api/v1/device-logs",
      method: "DELETE",
      data: data,
    });
  };

  changeMode = async (data: any) => {
    return this.request({
      url: "/api/v1/devices/mode",
      method: "PUT",
      data: data,
    });
  };
}

export default function DeviceServiceFn() {
  return new DeviceService();
}
