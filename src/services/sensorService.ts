import Base from "./baseService";

class SensorService extends Base {
  index = async (filter: any) => {
    return this.request({
      url: "/api/v1/sensors",
      method: "GET",
      data: filter,
    });
  };

  getByDevice = async (filter: any) => {
    return this.request({
      url: "/api/v1/sensors/byDevice",
      method: "GET",
      data: filter,
    });
  };

  select2 = async (filter: any) => {
    return this.request({
      url: "/api/v1/sensors/select2",
      method: "GET",
      data: filter,
    });
  };

  select2camera = async (filter: any) => {
    return this.request({
      url: "/api/v1/sensors/:id/select2camera",
      method: "GET",
      data: filter,
    });
  };

  create = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors",
      method: "POST",
      data: data,
    });
  };

  detail = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors/:id",
      method: "GET",
      data: data,
    });
  };

  edit = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors/:id",
      method: "PUT",
      data: data,
    });
  };

  delete = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors",
      method: "DELETE",
      data: data,
    });
  };

  destroy = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors/:id",
      method: "DELETE",
      data: data,
    });
  };

  deleteAll = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors/all",
      method: "DELETE",
      data: data,
    });
  };

  changeMode = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors/mode",
      method: "PUT",
      data: data,
    });
  };

  getCamera = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors/:id/camera/:cid",
      method: "GET",
      data: data,
    });
  };

  indexCamera = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors/:id/camera",
      method: "GET",
      data: data,
    });
  };

  updateCamera = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors/:id/camera",
      method: "POST",
      data: data,
    });
  };

  removeCamera = async (data: any) => {
    return this.request({
      url: "/api/v1/sensors/:id/camera",
      method: "DELETE",
      data: data,
    });
  };
}

export default function SensorServiceFn() {
  return new SensorService();
}
