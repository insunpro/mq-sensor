import Base from "./baseService";

class SnapshotService extends Base {
  index = async (filter: any) => {
    return this.request({
      url: "/api/v1/snapshots",
      method: "GET",
      data: filter,
    });
  };

  detail = async (data: any) => {
    return this.request({
      url: "/api/v1/snapshots/:id",
      method: "GET",
      data: data,
    });
  };

  delete = async (data: any) => {
    return this.request({
      url: "/api/v1/snapshots",
      method: "DELETE",
      data: data,
    });
  };

  destroy = async (data: any) => {
    return this.request({
      url: "/api/v1/snapshots/:id",
      method: "DELETE",
      data: data,
    });
  };
}

export default function SnapshotServiceFn() {
  return new SnapshotService();
}
