import Base from "./baseService";

class CameraService extends Base {
  index = async (filter: any) => {
    return this.request({
      url: "/api/v1/cameras",
      method: "GET",
      data: filter,
    });
  };

  select2 = async (filter: any) => {
    return this.request({
      url: "/api/v1/cameras/select2",
      method: "GET",
      data: filter,
    });
  };

  create = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras",
      method: "POST",
      data: data,
    });
  };

  sync = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/sync",
      method: "POST",
      data: data,
    });
  };

  detail = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/:id",
      method: "GET",
      data: data,
    });
  };

  edit = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/:id",
      method: "PUT",
      data: data,
    });
  };

  delete = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras",
      method: "DELETE",
      data: data,
    });
  };

  destroy = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/:id",
      method: "DELETE",
      data: data,
    });
  };

  lastSnap = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/:id/lastSnap",
      method: "GET",
      data: data,
    });
  };

  snapshot = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/:id/snapshot",
      method: "GET",
      data: data,
    });
  };

  onvifSearch = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/search",
      method: "GET",
      data: data,
    });
  };

  onvifConnect = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/connect",
      method: "POST",
      data: data,
    });
  };

  onvifSave = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/save",
      method: "POST",
      data: data,
    });
  };

  savePreset = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/preset",
      method: "POST",
      data: data,
    });
  };

  removePreset = async (data: any) => {
    return this.request({
      url: "/api/v1/cameras/preset",
      method: "DELETE",
      data: data,
    });
  };
}

export default function CameraServiceFn() {
  return new CameraService();
}
