import Base from "./baseService";

class AreaService extends Base {
  province2 = async (filter: any) => {
    return this.request({
      url: "/api/v1/areas/province/2",
      method: "GET",
      data: filter,
    });
  };

  province = async (filter: any) => {
    return this.request({
      url: "/api/v1/areas/province",
      method: "GET",
      data: filter,
    });
  };

  district2 = async (filter: any) => {
    return this.request({
      url: "/api/v1/areas/district/2",
      method: "GET",
      data: filter,
    });
  };

  district = async (filter: any) => {
    return this.request({
      url: "/api/v1/areas/district",
      method: "GET",
      data: filter,
    });
  };

  ward2 = async (filter: any) => {
    return this.request({
      url: "/api/v1/areas/ward/2",
      method: "GET",
      data: filter,
    });
  };

  ward = async (filter: any) => {
    return this.request({
      url: "/api/v1/areas/ward",
      method: "GET",
      data: filter,
    });
  };
}

export default function AreaServiceFn() {
  return new AreaService();
}
